This directory contains working examples and programs.

Author is clearly noted at the top of the file if not a submodule; "VenomousWyvern241" for myself.

Directories:

* `rules_and_services` contains system rules and service files
* `scripts` contains python and bash scripts 
* `src` contains source files for either experimentation or examples
  * files with 'example' in their name are programs created to show usage of something
* `tests` contains tests for files within `src` directory
* `third_party` contains code and submodules authored by others

For individual files I have noted as the first file comment:
  * description
  * compilation command
  * run command
  * original location / author
  * (optionaly) notes
