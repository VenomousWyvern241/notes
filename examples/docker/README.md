# Official Images

https://hub.docker.com/_/ubuntu?tab#tags

# Building Docker

## Locally

1. Install Docker.
  * `$ sudo apt-get install docker.io`
2. Create Dockerfile.
3. Attempt to build locally: `$ docker image build - < Dockerfile`

## gitlab-ci

0. (optional) Create new branch.
1. Make changes.
2. Commit changes (or if applicable merge).

New latest is generated.

# Errors

## (gstd) error: XDG_RUNTIME_DIR not set in the environment.

[source](https://askubuntu.com/a/1108489)

Fix:

```
$ sudo xhost +
access control disabled, clients can connect from any host
$ echo $DISPLAY
:0
$ docker run -it --env DISPLAY=unix:0 --privileged  --volume /tmp/.X11-unix:/tmp/.X11-unix <repository:tag|id> /bin/bash
```

## Permission denied trying to connect to Docker deamon socket.

```
$ docker image build - < Dockerfile
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.24/build?buildargs#%7B%7D&cachefrom#%5B%5D&cgroupparent#&cpuperiod#0&cpuquota#0&cpusetcpus#&cpusetmems#&cpushares#0&dockerfile#Dockerfile&labels#%7B%7D&memory#0&memswap#0&networkmode#default&rm#1&shmsize#0&target#&ulimits#null&version#1: dial unix /var/run/docker.sock: connect: permission denied
```

Fix:
```
$ sudo chmod 666 /var/run/docker.sock
```

## You don't have enough free space in /var/cache/apt/archives.

1. Stop docker service.
   ```
   $ systemctl stop docker
   ```
2. Edit `/lib/systemd/system/docker.service` to tell docker to use directory X instead of default /var/lib/docker.
   ```
   $ sudo vi /lib/systemd/system/docker.service
   -- ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
   ++ ExecStart=/usr/bin/dockerd --data-root /mnt/var_lib_docker -H fd:// --containerd=/run/containerd/containerd.sock
   ```
2. Do daemon-reload as we changed docker.service file.
   ```
   $ systemctl daemon-reload
   ```
3. `rsync` existing docker data to new location.
   ```
   $ mkdir /mnt/var_lib_docker
   $ rsync -aqxP /var/lib/docker/ /mnt/var_lib_docker/
   ```
4. Restart docker service.
   ```
   $ sysctl docker start
   ```

## Unable to init server running GUI.

[example in job](https://gitlab.com/VenomousWyvern241/notes/-/jobs/2334024952)

```
$ cd examples/third_party/gtk-test
$ cargo test -- --test-threads#1
...
   Compiling gtk-test v0.15.0 (/builds/VenomousWyvern241/notes/examples/third_party/gtk-test)
    Finished test [unoptimized + debuginfo] target(s) in 6m 30s
     Running unittests (target/debug/deps/gtk_test-3373b3d9decef8e2)
running 0 tests
test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
     Running tests/basic.rs (target/debug/deps/basic-b09611646e29afeb)
Unable to init server: Could not connect: Connection refused
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: BoolError { message: "Failed to initialize GTK", filename: "/root/.cargo/git/checkouts/gtk3-rs-a818089b55180ee1/ffbbf55/gtk/src/rt.rs", function: "gtk::rt", line: 134 }', tests/basic.rs:11:17
note: run with `RUST_BACKTRACE#1` environment variable to display a backtrace
error: test failed, to rerun pass '--test basic'
```

Fix: run the gui in a virtual display environment

1. Install `xvfb` on docker image.
2. In yml/script call as: `xvfb-run <application call.....>`

# Running Docker

## Locally

1. List the available images: `$ docker images`
2. Run the desired image
  a. via name: `$ docker run -it <repository>:<tag> <shell>`
    * where <shell> in my case is `/bin/bash`
  b. via id: `$ docker run -it <ID> <shell>`

## Mount Local Directory

Add the following to the `docker run` command:

```
--mount type=bind,source=</full/path/to/desired_dir>,target=/mnt
```

That will mount the "desired_dir" to `/mnt` within the [Linux] Docker image. [source](https://docs.docker.com/storage/bind-mounts/#start-a-container-with-a-bind-mount)
