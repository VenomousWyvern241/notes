Last login: Tue Jun 25 17:24:21 UTC 2024 on pts/0
[rhel@control ~]$ cd ansible-files/
[rhel@control ansible-files]$ mkdir roles

// HG - Once the roles directory is created, use ansible-galaxy to build a role labeled apache

[rhel@control ansible-files]$ ansible-galaxy init --offline roles/apache
- Role roles/apache was created successfully

// HG - View role directories and their content.

[rhel@control ansible-files]$ tree roles
roles
└── apache
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml

9 directories, 8 files

// HG - run the newly created ansible playbook.

[rhel@control ansible-files]$ ansible-navigator run deploy_apache.yml

PLAY [Setup Apache Web Servers] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [node1]
ok: [node2]

TASK [apache : Install Apache web server] **************************************
changed: [node1]
changed: [node2]

TASK [apache : Ensure Apache is running and enabled] ***************************
changed: [node1]
changed: [node2]

TASK [apache : Install firewalld] **********************************************
changed: [node1]
changed: [node2]

TASK [apache : Ensure firewalld is running] ************************************
changed: [node1]
changed: [node2]

TASK [apache : Allow HTTP traffic on web servers] ******************************
changed: [node1]
changed: [node2]

TASK [apache : Deploy custom index.html] ***************************************
changed: [node1]
changed: [node2]

RUNNING HANDLER [apache : Reload Firewall] *************************************
changed: [node1]
changed: [node2]

PLAY RECAP *********************************************************************
node1                      : ok=8    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node2                      : ok=8    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

// HG - verify results.

[rhel@control ansible-files]$ ssh node1 "systemctl status httpd"
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-06-25 17:43:26 UTC; 40s ago
       Docs: man:httpd.service(8)
   Main PID: 78910 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 11102)
     Memory: 35.2M
        CPU: 53ms
     CGroup: /system.slice/httpd.service
             ├─78910 /usr/sbin/httpd -DFOREGROUND
             ├─78911 /usr/sbin/httpd -DFOREGROUND
             ├─78912 /usr/sbin/httpd -DFOREGROUND
             ├─78913 /usr/sbin/httpd -DFOREGROUND
             └─78914 /usr/sbin/httpd -DFOREGROUND

Jun 25 17:43:26 node1 systemd[1]: Starting The Apache HTTP Server...
Jun 25 17:43:26 node1 httpd[78910]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 127.0.0.1. Set the 'ServerName' directive globally to suppress this message
Jun 25 17:43:26 node1 systemd[1]: Started The Apache HTTP Server.
Jun 25 17:43:26 node1 httpd[78910]: Server configured, listening on: port 80
[rhel@control ansible-files]$ curl http://node1
<html>
<head>
<title>Welcome to node1</title>
</head>
<body>
 <h1>Hello from node1</h1>
</body>
</html>[rhel@control ansible-fssh node2 "systemctl status httpd" httpd"
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2024-06-25 17:43:27 UTC; 49s ago
       Docs: man:httpd.service(8)
   Main PID: 77296 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 11103)
     Memory: 30.8M
        CPU: 100ms
     CGroup: /system.slice/httpd.service
             ├─77296 /usr/sbin/httpd -DFOREGROUND
             ├─77297 /usr/sbin/httpd -DFOREGROUND
             ├─77298 /usr/sbin/httpd -DFOREGROUND
             ├─77299 /usr/sbin/httpd -DFOREGROUND
             └─77300 /usr/sbin/httpd -DFOREGROUND

Jun 25 17:43:27 node2 systemd[1]: Starting The Apache HTTP Server...
Jun 25 17:43:27 node2 httpd[77296]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 127.0.0.1. Set the 'ServerName' directive globally to suppress this message
Jun 25 17:43:27 node2 systemd[1]: Started The Apache HTTP Server.
Jun 25 17:43:27 node2 httpd[77296]: Server configured, listening on: port 80
[rhel@control ansible-files]$ curl http://node2
<html>
<head>
<title>Welcome to node2</title>
</head>
<body>
 <h1>Hello from node2</h1>
</body>
</html>[rhel@control ansible-files]$ 
