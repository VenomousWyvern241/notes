# Compiler Directives
# distutils: language=c++
# cython: language_level=3

#
# Derived From:
#  * https://github.com/thewtex/cython-cmake-example/tree/53b358fa8a718f4d7b4bffb425e7177d39f62e3f
#  * https://github.com/dev-cafe/cmake-cookbook/tree/749e008036469f4af955224a43d5fa9d85a92f72/chapter-09/recipe-03
#
# @VenomousWyvern241 2022/Dec/22
#

from libcpp.string cimport string

# Cython will internally handle this ambiguity.
import numpy as np   # NumPy-Python functions (np.ndarray, np.asarray)
cimport numpy as np  # NumPy C API (https://numpy.org/doc/stable/reference/c-api/index.html)

# If using numpy from C or Cython, it must be initialized or there will be segfaults.
np.import_array()

#
# Describe the C++ interface.
#
cdef extern from "binding_experiment.hpp":
    # Declare the Mode enum.
    # cdef enum Mode 'Mode': means "use Mode in Python and 'Mode' in Cpp" (https://stackoverflow.com/a/38059038)
    cdef enum Mode 'Mode':
        NONE 'Mode::NONE'       # Invalid: first entry defaults to 0, explicitly setting for clarity.
        NORMAL 'Mode::NORMAL'   # Normal Use: Fibonacci Sequence related.
        TEST 'Mode::TEST'       # Test Use: To confirm that various pieces work.

    # Delcare the class.
    cdef cppclass Fibonacci:
        # Declare methods.
        Fibonacci() except +
        int test_return_int(int)
        string test_return_string(string)
        void test_print()
        int * calculate_seq(int)
        void calculate_seq_void(int, int*)
        int get_nth_digit(int)
        void print_n_digits(int)
        void set_mode(Mode)
        void get_mode(Mode)
        void print_mode(Mode)

#
# Build the library wrapper.
#
cdef class pyFibonacci:

    # Module accessors for enum vals.
    #  Necessary workaround due to the fact that Mode seems to be local to this file.
    #  According to `print(dir(fib))` "Mode" is not part of pyFibonacci (without these workarounds).
    def mode_normal(self):
        return NORMAL
    def mode_none(self):
        return NONE
    def mode_test(self):
        return TEST

    # CPP instance that is being wrapped.
    cdef Fibonacci *thisptr

    def __init__(self):
        self.thisptr = new Fibonacci()

    def test_return_int(self, n):
        return self.thisptr.test_return_int(n)

    def test_return_string(self, s):
        return self.thisptr.test_return_string(s.encode('utf-8'))

    def test_print(self):
        self.thisptr.test_print()

    def calculate_seq(self, n):
        ## Modified From: https://stackoverflow.com/a/59666408
        # This is done via a Typed MemoryView, essentially telling Cython to look at that
        #  memory with a predefined format (int array in this case).
        #  - (https://cython.readthedocs.io/en/stable/src/userguide/memoryviews.html)
        #  - [::1] means that the elements will be contiguous (one element apart in memory)
        cdef int[::1] memview_arr = <int[:n]> self.thisptr.calculate_seq(n)
        # Convert the MemoryView into a NumPy array for easier use later.
        return np.asarray(memview_arr)

    # Reference for ndarray dtype differences: https://stackoverflow.com/a/46416257
    def calculate_seq_void(self, num, np.ndarray[int, ndim=1] arr):
        # This is done via a typed numpy array - very similar to Typed Memoryview.
        self.thisptr.calculate_seq_void(num, &arr[0])
        return arr

    def get_nth_digit(self, n):
        return self.thisptr.get_nth_digit(n)

    def print_n_digits(self, n):
        self.thisptr.print_n_digits(n)

    # Cython/Python see enums as integers so because we are cython ver <3.0
    #  need to do a manual conversion such as this.
    def set_mode(self, mode):
        if mode == NORMAL:
            self.thisptr.set_mode(NORMAL)
        elif mode == TEST:
            self.thisptr.set_mode(TEST)
        else:
            self.thisptr.set_mode(NONE)

    # Cython/Python see enums as integers so because we are cython ver <3.0
    #  need to do a manual conversion such as this.
    def print_mode(self, mode):

        if mode == NORMAL:
            self.thisptr.print_mode(NORMAL)
        elif mode == TEST:
            self.thisptr.print_mode(TEST)
        else:
            self.thisptr.print_mode(NONE)
