# Compiler Directives
# distutils: language=c
# cython: language_level=3

#
# Derived From: @VenomousWyvern CPP Cython Experiment 
#
# @VenomousWyvern241 2023/Jan/20
#

# Cython will internally handle this ambiguity.
import numpy as np   # NumPy-Python functions (np.ndarray, np.asarray)
cimport numpy as np  # NumPy C API (https://numpy.org/doc/stable/reference/c-api/index.html)

# If using numpy from C or Cython, it must be initialized or there will be segfaults.
np.import_array()

#
# Describe the C interface.
#
cdef extern from "binding_experiment.h":
    # Declare methods.
    int test_return_int(int)
    char* test_return_string(char *)
    void test_print()
    int * calculate_seq(int)
    void calculate_seq_void(int, int*)
    int get_nth_digit(int)
    void print_n_digits(int)

#
# Build the library wrapper.
#
def py_test_return_int(n):
    return test_return_int(n)

def py_test_return_string(s):
    return test_return_string(s.encode('utf-8'))

def py_test_print():
    test_print()

def py_calculate_seq(n):
    ## Modified From: https://stackoverflow.com/a/59666408
    # This is done via a Typed MemoryView, essentially telling Cython to look at that
    #  memory with a predefined format (int array in this case).
    #  - (https://cython.readthedocs.io/en/stable/src/userguide/memoryviews.html)
    #  - [::1] means that the elements will be contiguous (one element apart in memory)
    cdef int[::1] memview_arr = <int[:n]> calculate_seq(n)
    # Convert the MemoryView into a NumPy array for easier use later.
    return np.asarray(memview_arr)

# Reference for ndarray dtype differences: https://stackoverflow.com/a/46416257
def py_calculate_seq_void(num, np.ndarray[int, ndim=1] arr):
    # This is done via a typed numpy array - very similar to Typed Memoryview.
    calculate_seq_void(num, &arr[0])
    return arr

def py_get_nth_digit(n):
    return get_nth_digit(n)

def py_print_n_digits(n):
    print_n_digits(n)
