/**
 * @file
 *
 * @brief Part of a set of example programs to investigate python bindings for C vs CPP.
 * Programs have the same use case and function names for comparisons sake.
 *
 * Compile:
 *   `$ gcc -Wall -g -c <file_name>.<ext> -o <file_name>.o`
 *   `$ cmake --build . -t <file_name>`
 * View Exported Functions: `nm -D libbinding_exp_c.so`
 * * symbols listed here: https://linux.die.net/man/1/nm
 *
 * @VenomousWyvern241 2022/November/18
 */

#include "binding_experiment.h"

#include <stdio.h>
#include <stdlib.h>

int test_return_int(int n)
{
  return n;
}

char* test_return_string(char* str)
{
  return str;
}

void test_print()
{
  printf("Testing printing capability.\n");
}

int* calculate_seq(int n)
{
  if (n <= 0)
    return NULL;

  int* ret = calloc((unsigned int)n, sizeof(int));

  /*"ret" is zero initialized, so setting the first element is unnecessary. */
  if (n >= 2)
    ret[1] = 1;
  if (n >= 3)
  {
    for (int i = 2; i < n; i++)
      ret[i] = ret[i - 1] + ret[i - 2];
  }

  return ret;
}

void calculate_seq_void(int n, int* res)
{
  if (n >= 1)
    res[0] = 0;
  if (n >= 2)
    res[1] = 1;
  if (n >= 3)
  {
    for (int i = 2; i < n; i++)
      res[i] = res[i - 1] + res[i - 2];
  }
}

int get_nth_digit(int n)
{
  if (n <= 1)
    return n;

  return get_nth_digit(n - 1) + get_nth_digit(n - 2);
}

void print_n_digits(int n)
{
  int* fib = calculate_seq(n);

  if (fib == NULL)
    printf("Oops no Fibonacci sequence avaialble.");
  else
  {
    for (int i = 0; i < n; i++)
      printf("%d ", (int)fib[i]);
  }

  printf("\n");

  free(fib);
}
