// Example rust program prints hello world.
//
// Compile (optional): `$ cargo build --example hello_world`
// Run: `$ cargo run --example hello_world`
//
// @VenomousWyvern241 2022/April/14

fn main() {
  println!("Hello, world!");
}
