// Example gtk+rust program that creates a window with a button in it, that when clicked exits the program.
// Part of a comparison implementation, see: gtk_3-24_helloworld_test.c gtkmm_3-24_helloworld.cpp gtk_3-24_helloworld.rs gtk_3-24_helloworld_test.rs
//
// Compile (optional): `$ cargo build --example gtk_3-24_helloworld`
// Compile & Run: `$ cargo run --example gtk_3-24_helloworld`
//
// @Venomouswyvern241 2022/April/18
//
// Influence from: 
//   * https://github.com/gtk-rs/gtk3-rs/blob/master/examples/basics/main.rs
//   * https://gitlab.gnome.org/GNOME/gtk/-/blob/gtk-3-24/examples/hello-world.c

extern crate gtk;

use gtk::prelude::*;

fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);

    window.set_title("Windows");
    window.set_border_width(10);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(200, 200);

    let window_clone = window.clone();
    let button = gtk::Button::with_label("Hello World!");

    // https://users.rust-lang.org/t/gtk-rs-bc-wont-let-me-close-window/32060
    button.connect_clicked(move |_|  window_clone.close());

    window.add(&button);

    window.show_all();
}

fn main() {
    let application =
        gtk::Application::new(Some("org.gtk.example"), Default::default());

    application.connect_activate(build_ui);

    application.run();
}
