/*
 * Example program test output of various commands with varying parameters.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2025/January/27
 */

#include <sys/stat.h>

#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <cstring>

void print_data(struct stat* stbuf)
{
  // Courtesty of: https://man7.org/linux/man-pages/man2/stat.2.html

  printf(" File type:       ");
  switch (stbuf->st_mode & S_IFMT)
  {
    case S_IFBLK:
      printf("block device\n");
      break;
    case S_IFCHR:
      printf("character device\n");
      break;
    case S_IFDIR:
      printf("directory\n");
      break;
    case S_IFIFO:
      printf("FIFO/pipe\n");
      break;
    case S_IFLNK:
      printf("symlink\n");
      break;
    case S_IFREG:
      printf("regular file\n");
      break;
    case S_IFSOCK:
      printf("socket\n");
      break;
    default:
      printf("unknown?\n");
      break;
  }

  printf(" I-node number:   %ju\n", (uintmax_t)stbuf->st_ino);
  printf(" Mode:            %jo (octal)\n", (uintmax_t)stbuf->st_mode);
  printf(" Link count:      %ju\n", (uintmax_t)stbuf->st_nlink);
  printf(" Ownership:       UID=%ju   GID=%ju\n", (uintmax_t)stbuf->st_uid,
         (uintmax_t)stbuf->st_gid);
}

/**
 * Test STAT().
 */
void showcase_stat()
{
  struct stat stbuf;
  const char* bad_path_file = "file_not_found.txt";
  const char* bad_path_dir = "/dir_not_found";
  const char* good_path_file = "./stat.cpp";
  const char* good_path_dir = "/tmp";

  int ret = 0;

  memset(&stbuf, 0, sizeof(struct stat));
  ret = stat(bad_path_file, &stbuf);
  printf("\n[%s with \'%s\'] returned %d\n   specifically (%d) %s\n", __func__, bad_path_file, ret,
         errno, strerror(errno));

  memset(&stbuf, 0, sizeof(struct stat));
  ret = stat(bad_path_dir, &stbuf);
  printf("\n[%s with \'%s\'] returned %d\n   specifically (%d) %s\n", __func__, bad_path_dir, ret,
         errno, strerror(errno));

  memset(&stbuf, 0, sizeof(struct stat));
  ret = stat(good_path_file, &stbuf);
  printf("\n[%s with \'%s\'] returned %d \n", __func__, good_path_file, ret);
  print_data(&stbuf);

  memset(&stbuf, 0, sizeof(struct stat));
  ret = stat(good_path_dir, &stbuf);
  printf("\n[%s with \'%s\'] returned %d \n", __func__, good_path_dir, ret);
  print_data(&stbuf);
}

/**
 * Showcase.
 */
int main()
{
  showcase_stat();

  return 0;
}
