/*
 * Example program to show use of spdlog.
 *
 * Compile Library:
 *   `$ g++ -c <file_name>.<ext> -I ../third_party/spdlog/include/`
 *   `$ cmake --build . -t <file_name>`
 *
 * @VenomousWyvern241 2022/May/16
 */

#include "spdlog_example.h"

#include <ctime>
#include <iostream>

#include "spdlog/sinks/basic_file_sink.h"

using fmt::format;
using std::string;

SpdLogEx::SpdLogEx() : log_fn_("example.log"), log_path_("./")
{
  try
  {
    // Initialize the logger.
    logger_ = spdlog::basic_logger_mt("example_logger", getLog());
    // Turn off by default.
    logger_->set_level(spdlog::level::off);
  }
  catch (const spdlog::spdlog_ex& ex)
  {
    std::cout << "Log initialization failed: " << ex.what() << std::endl;
    std::cout << "Error: debugging was NOT enabled." << std::endl;
  }
}

SpdLogEx::~SpdLogEx()
{
  // Politely exit the logger.
  spdlog::shutdown();
}

void SpdLogEx::disableDebug()
{
  logger_->set_level(spdlog::level::off);
}

void SpdLogEx::enableDebug()
{
  // Enable logging by setting any level other than off.
  logger_->set_level(spdlog::level::debug);
  // Flush the logger (to the log file) anytime an message is added that has a level equal to or
  // greater than the one specified below.
  logger_->flush_on(spdlog::level::debug);

  collectBaseInformation();
}

bool SpdLogEx::isDebugOn() const
{
  // If the log level is set to off then debug is disabled.
  if (logger_->level() == spdlog::level::off)
    return false;

  return true;
}

string SpdLogEx::getLog() const
{
  return log_path_ + log_fn_;
}

void SpdLogEx::setLog(const string& path, const string& fn)
{
#ifdef _WIN32
  const char path_separator = '\\';
#else
  const char path_separator = '/';
#endif

  bool debug_was_on = isDebugOn();

  // If the new name is actually provided update; elsewise leave as default.
  if (!fn.empty())
  {
    logger_->info("Updating log file name from \"{}\" to \"{}\".", log_fn_, fn);
    log_fn_ = fn;
  }
  else
  {
    logger_->info("Leaving log file name as default: \"{}\".", log_fn_);
  }

  // If the new path is actually provided update, elsewise leave as default.
  if (!path.empty())
  {
    logger_->info("Updating log file path from \"{}\" to \"{}\".", log_path_, path);
    log_path_ = path;
  }
  else
  {
    logger_->info("Leaving log file path as default: \"{}\".", log_path_);
  }

  // If necessary add a path_separator character <dir>/<fn>.
  if (log_path_.back() != path_separator && fn.front() != path_separator)
    log_path_.push_back(path_separator);

  // Update logger: shutdown the existing and create a new one, truncation is to remove duplication.
  spdlog::shutdown();
  logger_ = spdlog::basic_logger_mt("example_logger", getLog(), true);

  // Return to prior debug status.
  if (debug_was_on)
    enableDebug();

  logger_->info("Updated the log file.");
}

void SpdLogEx::collectBaseInformation()
{
  time_t now = time(0);
  char* dt = ctime(&now);

  logger_->info(" -----------------------------------------------------------");
  logger_->info("{}", dt);
  logger_->info("");
}

void SpdLogEx::doSomething()
{
  logger_->info("This is nothing interesting.");
}

void SpdLogEx::doSomethingBad()
{
  logger_->error("ERROR: providing an error as if something bad happened.");
}

void SpdLogEx::doSomethingElse()
{
  logger_->debug("This is supposed to do something else.");
}
