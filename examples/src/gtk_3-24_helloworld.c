// Example gtk program that creates a window with a button in it, when clicked exits the program.
// Part of a comparison implementation, see:
//   gtk_3-24_helloworld_test.c
//   gtkmm_3-24_helloworld.cpp
//   gtk_3-24_helloworld.rs
//   gtk_3-24_helloworld_test.rs
//
// Compile:
//   * `$ gcc -o <file_name> <file_name>.<ext> `pkg-config --libs gtk+3.0 --cflags gtk+3.0``
//   * `$ cmake --build . -t <file_name>`
// Run: `$ ./gtk_3-24_helloworld`
//
// Courtesy of:
// - https://gitlab.gnome.org/GNOME/gtk/-/blob/gtk-3-24/examples/hello-world.c (slightly modified)

#include <gtk/gtk.h>

static void activate(GtkApplication *app, gpointer user_data)
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *button_box;

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW(window), "Window");
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 200);

  button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add(GTK_CONTAINER(window), button_box);

  button = gtk_button_new_with_label("Hello World");
  g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), window);
  gtk_container_add(GTK_CONTAINER(button_box), button);

  gtk_widget_show_all(window);
}

int main(int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
