/* clang-format off */
// Example gtkmm program that creates a window with a button in it, when clicked exits the program. 
// Part of a comparison implementation, see: 
//   gtk_3-24_helloworld_test.c
//   gtkmm_3-24_helloworld.cpp 
//   gtk_3-24_helloworld.rs 
//   gtk_3-24_helloworld_test.rs
//
// Compile:
//   * `$ g++ <file_name>.<ext> -o <file_name> `pkg-config gtkmm-3.0 --cflags --libs``
//   * `$ cmake --build . -t <file_name>`
//  Run: `$ ./gtkmm_3-24_helloworld`
//
// @VenomousWyvern241 2022/April/19
// Based on:
// - https://gitlab.gnome.org/GNOME/gtkmm-documentation/-/tree/gtkmm-3-24/examples/book/buttons/button
/* clang-format off */

#include <gtkmm/application.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>

class Button : public Gtk::Window
{
 public:
  Button();
  virtual ~Button();

 protected:
  // Signal handlers:
  void on_button_clicked();

  // Child widgets:
  Gtk::Button m_button;
};

Button::Button()
{
  set_title("Window");
  set_size_request(200, 200);
  set_border_width(10);

  m_button.add_label("Hello World!");

  m_button.signal_clicked().connect(sigc::mem_fun(*this, &Button::on_button_clicked));

  add(m_button);

  show_all_children();
}

Button::~Button()
{
}

void Button::on_button_clicked()
{
  this->hide();
}

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  Button buttons;

  // Shows the window and returns when it is closed.
  return app->run(buttons);
}
