/**
 * Parse command line arguments via 'getopts'.
 *
 * Compile:
 *   `$ gcc <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./arg_parse_example_getopt [options]`
 *
 * @VenomousWyvern241
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static int unit_options[] = {1,3,5,7};
static int channel_options[] = {0,1};

/**
 * Check if array contains a specific value.
 *
 * @param array The integer array.
 * @param value The value to search.
 * 
 * @return True on successfule location, Flase otherwise.
 */
bool check_array(int *array, int value)
{
  for(int i = 0; i < (sizeof(array)/sizeof(*array)); i++)
  {
    if(array[i] == value)
      return true;
  }
  return false;
}

/**
 * Print array of integers.
 * 
 * @param array The integer array to print.
 */
void print_array(int *array)
{
  for(int i = 0; i < (sizeof(array)/sizeof(*array)); i++)
    printf("%d, ", array[i]);
}


/**
 * Print the usage documentation.
 *
 * @param progname The name of the program being executed.
 * @param hint A hint as to why the usage is being displayed.
 */
void usage(const char* progname, const char* hint)
{
  printf("\n");
  if (hint && *hint)
    printf("%s\n", hint);
  printf("Usage: `%s [options]\n", progname);
  printf("Options:\n");
  printf("-u <unit>; Unit number of device. Valid Options: ");
  print_array(unit_options);
  printf("\n");
  printf("-c <channel>; Channel number to use. Valid Options: ");
  print_array(channel_options);
  printf("\n");
  printf("-o <outfile>; Set the output file.\n");
  printf("-d <debug_file>; Set the debug file.\n");
  printf("-i; Invert the output.\n");
  printf("-v; Verbose output.\n");
  printf("-r; Reset settings.\n");
  printf("-h; Display usage help.\n");

  exit(1);
}

/**
 * Example of parsign command line arguments via 'getopts.
 */
int main(int argc, char **argv)
{
  int unit = -1;
  int channel = -1;
  int c;
  char *out_file = NULL;
  char *debug_file = NULL;
  bool invert = false;
  bool verbose = false;
  bool reset = false;
  bool help = false;

  const char* progname = argv[0];

  // Process options.
  while ((c = getopt (argc, argv, ":u:c:o:d:iwvr")) != -1)
  { 
    switch (c)
    {
      case 'u':
        if (check_array(unit_options, atoi(optarg)))
        {
          printf("Setting 'unit' to '%s'.\n", optarg);
          unit = atoi(optarg);
        }
        else
          usage(progname, "Error unit selection is invalid.");
        break;
      case 'c':
        if (check_array(channel_options, atoi(optarg)))
        {
          printf("Setting 'channel' to '%s'.\n", optarg);
          channel = atoi(optarg);
        }
        else 
          usage(progname, "Error channel selection is invalid.");
        break;
      case 'o':
        out_file = optarg;
        printf("Setting 'out_file' to '%s'.\n", optarg);
        break;
      case 'd':
        debug_file = optarg;
        printf("Setting 'debug_file' to '%s'.\n", optarg);
        break;
     case ':': 
        usage(progname, "Error one of the options specified requires a value!"); 
        break; 
     case 'i':
        printf("Setting 'invert'!\n");
        invert = true;
        break;
     case 'v':
        printf("Setting 'verbose'!\n");
        verbose = true;
        break;
     case 'r':
        printf("Setting 'reset'!\n");
        reset = true;
        break;
     case 'h':
     case '?':
     default:
        usage(progname, "");
    }
  }

  // Check mandatory parameters:
  if (unit == -1)
    usage(progname, "Error: unit specification is mandatory.");
  if (channel == -1)
    usage(progname, "Error: channel specification is mandatory.");

  printf("%s", ((invert) ? " ... inverting ...\n" : ""));
  printf("unit: %d\n channel: %d\n", unit, channel);

  if(out_file)
    printf("output file: %s\n", out_file);

  if(debug_file)
    printf("debug file: %s%s\n", debug_file, ((verbose) ? " (verbose)" : "" ));

  if (help)
    usage(progname, "Help was requested?");

  if (reset)
    printf("Resetting settings now!");
}
