#ifndef IMITATION_REGISTER_EXAMPLE_CONSTANTS_H_
#define IMITATION_REGISTER_EXAMPLE_CONSTANTS_H_

#define REG_ID 0x0
/* Module ID PreRelease Number. */
#define REG_ID_RC_Sft 0
#define REG_ID_RC_Msk (0xFF << REG_ID_RC_Sft)
/* Module ID Patch Number. */
#define REG_ID_PAT_Sft 8
#define REG_ID_PAT_Msk (0xFF << REG_ID_PAT_Sft)
/* Module ID Minor Number. */
#define REG_ID_MIN_Sft 16
#define REG_ID_MIN_Msk (0xFF << REG_ID_MIN_Sft)
/* Module ID Major Number. */
#define REG_ID_MAJ_Sft 24
#define REG_ID_MAJ_Msk (0xFFU << REG_ID_MAJ_Sft)

#define REG_CONTROL 0x1
/* Option A: */
#define REG_CONTROL_OPT_A_Msk (1 << 0)
/* Option B: */
#define REG_CONTROL_OPT_B_Msk (1 << 1)
/* Option C: setup the device. */
#define REG_CONTROL_SETUP_DEVICE_Msk (1 << 2)
/* Flag 1: */
#define REG_CONTROL_FLAG_1_Msk (1 << 4)
/* Flag 2: */
#define REG_CONTROL_FLAG_2_Msk (1 << 5)
/* Is the device configured; yes (0), no(1). */
#define REG_CONTROL_UNCFGD_Msk (1 << 6)

#define REG_RESET 0x2
/* RST_ALL: all pins necessary for reset at both a software level and a hardware level. */
#define REG_RESET_RST_ALL_Msk (0x1F)
/* Reset just the fifo. */
#define REG_RESET_FIFO_RST_Msk (1 << 0)
/* Hard reset. */
#define REG_RESET_HARD_RST_Msk (1 << 1)
/* Reset just the clock. */
#define REG_RESET_CLK_Msk (1 << 2)
/* Reset just the data inversion (engaged set to 1). */
#define REG_RESET_INVERT_Msk (1 << 3)
/* Reset just the test mode (engaged set to 1).*/
#define REG_RESET_TEST_MD_Msk (1 << 4)
/* Reset just the busy bit. (engaged set to 1).*/
#define REG_RESET_BUSY_Msk (1 << 5)

#endif
