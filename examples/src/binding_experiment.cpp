/*
 * Part of a set of example programs to investigate python bindings for C vs CPP.
 * Programs have the same use case and function names for comparisons sake.
 *
 * Compile:
 *   `$ g++ -c <file_name>.<ext> -fPIC -o <file_name>.o`
 *   `$ cmake --build . -t <file_name>`
 *
 * @VenomousWyvern241 2022/November/18
 */

#include "binding_experiment.hpp"

#include <stdio.h>

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <type_traits>

Fibonacci::Fibonacci() : mode_(Mode::NONE)
{
}

Fibonacci::~Fibonacci()
{
}

int Fibonacci::test_return_int(int n)
{
  return n;
}

std::string Fibonacci::test_return_string(std::string str)
{
  return str;
}

void Fibonacci::test_print()
{
  printf("This is all about the Fibonacci Sequence!\n");
}

int *Fibonacci::calculate_seq(int n)
{
  int *ret;

  if (n <= 0)
    return nullptr;
  else
    ret = new int[n]();

  // "ret" is zero initialized, so setting the first element is unnecessary.
  if (n >= 2)
    ret[1] = 1;
  if (n >= 3)
  {
    for (int i = 2; i < n; i++)
      ret[i] = ret[i - 1] + ret[i - 2];
  }

  return ret;
}

void Fibonacci::calculate_seq_void(int n, int *res)
{
  if (n >= 1)
    res[0] = 0;
  if (n >= 2)
    res[1] = 1;
  if (n >= 3)
  {
    for (int i = 2; i < n; i++)
      res[i] = res[i - 1] + res[i - 2];
  }
}

int Fibonacci::get_nth_digit(int n)
{
  if (n <= 2)
    return n - 1;

  return get_nth_digit(n - 1) + get_nth_digit(n - 2);
}

void Fibonacci::print_n_digits(int n)
{
  int *fib = calculate_seq(n);

  if (fib == nullptr)
    printf(" Oops no Fibonacci sequence available.");
  else
  {
    for (int i = 0; i < n; i++)
      printf("%d ", fib[i]);
  }

  printf("\n");
}

void Fibonacci::set_mode(const Mode mode)
{
  if (std::find(invalid_patterns.begin(), invalid_patterns.end(), mode) != invalid_patterns.end())
  {
    printf(" ^ Invalid Mode specified, setting to: %d.\n",
           static_cast<std::underlying_type<Mode>::type>(Mode::NONE));
    mode_ = Mode::NONE;
  }
  else
  {
    printf(" ^ Mode set to: %d.\n", static_cast<int>(mode));
    mode_ = mode;
  }
}

void Fibonacci::get_mode(Mode &mode)
{
  mode = mode_;
}

void Fibonacci::print_mode(Mode mode)
{
  switch (mode)
  {
    case Mode::NORMAL:
      printf(" Running in \"normal\" mode.\n");
      break;
    case Mode::TEST:
      printf(" Running in \"test\" mode.\n");
      break;
    default:
      printf(" Mode is %d.\n", static_cast<int>(mode));
      break;
  }
}

// What follows are examples to show swig use - they do not hold a purpose in the is class.
// Used only in python testing; suppressing expected "unusedFunction" cppcheck warnings.

// cppcheck-suppress unusedFunction
std::shared_ptr<sp_ex> Fibonacci::start_something_useless()
{
  std::shared_ptr<sp_ex> ret = std::make_shared<sp_ex>();
  ret->n = 0;
  return ret;
}

// cppcheck-suppress unusedFunction
void Fibonacci::do_something_useless(const std::shared_ptr<sp_ex> &sp)
{
  sp->n = 10;
}

// cppcheck-suppress unusedFunction
void Fibonacci::print_something_useless(const std::shared_ptr<sp_ex> sp)
{
  printf("value contained in shared_ptr: %d\n", sp->n);
}

// cppcheck-suppress unusedFunction
void Fibonacci::initial_structure(structure &s)
{
  s.integer_value = 0;
  s.text_value = "initial";
  s.double_value = 1.5;
}

// cppcheck-suppress unusedFunction
void Fibonacci::print_structure(structure s)
{
  printf("Note Entry, integer_value %d, double_value, %f\n", s.integer_value, s.double_value);
  printf("  %s\n", s.text_value.c_str());
}

// What follows are C functions that call the C++ class functions; necessary for ctypes.
//  https://github.com/Auctoris/ctypes_demo
//  https://stephenscotttucker.medium.com/interfacing-python-with-c-using-ctypes-classes-and-arrays-42534d562ce7
// Used only in python testing; suppressing expected "unusedFunction" cppcheck warnings.
extern "C"
{
  // cppcheck-suppress unusedFunction
  Fibonacci *Fib_new()
  {
    return new Fibonacci();
  }
  // cppcheck-suppress unusedFunction
  void Fib_delete(Fibonacci *fib)
  {
    if (fib)
      delete fib;
  }

  // cppcheck-suppress unusedFunction
  int Fib_test_return_int(Fibonacci *fib, int n)
  {
    return fib->test_return_int(n);
  }
  // cppcheck-suppress unusedFunction
  const char *Fib_test_return_string(Fibonacci *fib, char *str)
  {
    std::string sstr = str;
    std::string ret = fib->test_return_string(sstr);
    char *c_ptr = new char[ret.length() + 1];
    strcpy(c_ptr, ret.c_str());
    return c_ptr;
  }
  // cppcheck-suppress unusedFunction
  void Fib_test_print(Fibonacci *fib)
  {
    fib->test_print();
  }
  // cppcheck-suppress unusedFunction
  int *Fib_calculate_seq(Fibonacci *fib, int n)
  {
    return fib->calculate_seq(n);
  }
  // cppcheck-suppress unusedFunction
  void Fib_calculate_seq_void(Fibonacci *fib, int n, int *arr)
  {
    fib->calculate_seq_void(n, arr);
  }
  // cppcheck-suppress unusedFunction
  int Fib_get_nth_digit(Fibonacci *fib, int n)
  {
    return fib->get_nth_digit(n);
  }
  // cppcheck-suppress unusedFunction
  void Fib_print_n_digits(Fibonacci *fib, int n)
  {
    fib->print_n_digits(n);
  }
  // cppcheck-suppress unusedFunction
  void Fib_set_mode(Fibonacci *fib, int mode)
  {
    fib->set_mode(static_cast<Mode>(mode));
  }
  // cppcheck-suppress unusedFunction
  int Fib_get_mode(Fibonacci *fib)
  {
    Mode m = Mode::UNKNOWN;
    fib->get_mode(m);
    return static_cast<int>(m);
  }
  // cppcheck-suppress unusedFunction
  void Fib_print_mode(Fibonacci *fib, int mode)
  {
    fib->print_mode(static_cast<Mode>(mode));
  }
}
