(VSC) Visual Studio Code https://code.visualstudio.com/
(MSVS) Microsoft Visual Studio https://visualstudio.microsoft.com/

The files within this directory are for VSC.

# Setup

1. Download VSC.
  * https://code.visualstudio.com/
2. Install VSC.
3. Collect the C/C++ extension.
  * https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools

## Linux 

## Windows

