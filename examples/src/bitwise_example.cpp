/*
 * Example program to showcase bitwise calculations.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run (local): `$ ./bitwise_example`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2022/December/8
 */

#include <bitset>
#include <iomanip>
#include <iostream>
using namespace std;

// Usually want to #define masking and register addresses/offsets at the top of the CPP file (within
// namespace as applicable). If the definitions were in the header file, then they would be passed
// onto anything that includes that header.

#define MASK_RIGHT 0xF
#define MASK_LEFT  (MASK_RIGHT << 4)

// Macro for masking a specific hex digit (rightmost is 0) of a value.
#define MASK_DIGIT(HEX_DIGIT) (MASK_RIGHT << (HEX_DIGIT * 4))

// Macro for masking that touches the most significant bit - i.e. the sign bit.
// - Need to use `U` to specify unsigned to avoid overflow (would happen if it was a signed int).
// - No need to specify on others (above) because they are clearly labeled as masks (implies they
// will only be used in bitwise OR/AND operations), now if they were going to be used in 
// mathematical computations then all should have `U` specified for consistency.
#define MASK_MAX   (1U << 16)
#define MASK_TOP_4 (0xFU << 12)

/**
 * Showcase bitwise calculations.
 */
int main()
{
  uint16_t a = 0x1, b = 0x8, c = 0x80, d = 0x88, e = a;

  std::cout << "------ Shifting ------" << std::endl;

  std::cout << " a = 0x" << std::setw(2) << std::setfill('0') << std::hex << a << " = "
            << std::bitset<8>(a << 0) << std::endl;
  std::cout << " b >> 3 = " << std::bitset<8>(b) << " -> " << std::bitset<8>(b >> 3) << std::endl;
  std::cout << " c >> 7 = " << std::bitset<8>(c) << " -> " << std::bitset<8>(c >> 7) << std::endl;

  std::cout << std::endl;

  std::cout << " b = 0x" << std::setw(2) << std::setfill('0') << std::hex << b << " = "
            << std::bitset<8>(b << 0) << std::endl;
  std::cout << " a << 3 = " << std::bitset<8>(a) << " -> " << std::bitset<8>(a << 3) << std::endl;
  std::cout << " c >> 4 = " << std::bitset<8>(c) << " -> " << std::bitset<8>(c >> 4) << std::endl;

  std::cout << std::endl;

  std::cout << " c = 0x" << std::hex << c << " = " << std::bitset<8>(c << 0) << std::endl;
  std::cout << " a << 7 = " << std::bitset<8>(a) << " -> " << std::bitset<8>(a << 7) << std::endl;
  std::cout << " b << 4 = " << std::bitset<8>(b) << " -> " << std::bitset<8>(b << 4) << std::endl;
  std::cout << std::endl;

  std::cout << "------ OR (Combining) ------" << std::endl;

  std::cout << " d = 0x" << std::hex << d << " = " << std::bitset<8>(d) << std::endl;
  std::cout << " b | c = " << std::bitset<8>(b) << " | " << std::bitset<8>(c) << " = "
            << std::bitset<8>(b | c) << std::endl;
  std::cout << std::endl;

  std::cout << "------ OR (Setting) ------" << std::endl;

  std::cout << " e = a = 0x" << std::hex << e << " = " << std::bitset<8>(e) << std::endl;
  e |= b;
  std::cout << " e |= b => e = (e | b) = ( " << std::bitset<8>(a) << " | " << std::bitset<8>(b)
            << " ) = " << std::bitset<8>(e) << std::endl;
  std::cout << std::endl;

  std::cout << "------ NOT + AND (Resetting) ------" << std::endl;

  std::cout << " e = (a | b) = 0x" << std::hex << e << " = " << std::bitset<8>(e) << std::endl;
  e &= ~b;
  std::cout << " e &= ~b => e = (e|~b) = ( " << std::bitset<8>(a | b) << " | " << std::bitset<8>(~b)
            << " ) = " << std::bitset<8>(e) << std::endl;

  std::cout << std::endl;

  std::cout << "------ AND (Masking) ------" << std::endl;

  std::cout << " d = 0x" << std::hex << d << " = " << std::bitset<8>(d) << std::endl;
  std::cout << " c = 0x" << std::hex << c << " = d & MASK_LEFT     = ( " << std::bitset<8>(d)
            << " & " << std::bitset<8>(MASK_LEFT) << " ) = " << std::bitset<8>(d & MASK_LEFT)
            << std::endl;
  std::cout << " c = 0x" << std::hex << c << " = d & MASK_DIGIT(1) = ( " << std::bitset<8>(d)
            << " & " << std::bitset<8>(MASK_DIGIT(1))
            << " ) = " << std::bitset<8>(d & MASK_DIGIT(1)) << std::endl;
  std::cout << " b = 0x" << std::setw(2) << std::setfill('0') << std::hex << b
            << " = d & MASK_RIGHT    = ( " << std::bitset<8>(d) << " & "
            << std::bitset<8>(MASK_RIGHT) << " ) = " << std::bitset<8>(d & MASK_RIGHT) << std::endl;
  std::cout << std::endl;

  std::cout << "------ AND (Checking Status) ------" << std::endl;

  std::cout << " c = 0x" << std::hex << c << " = " << std::bitset<8>(c) << std::endl;
  std::cout << " bit 0 of 'c' is " << ((c & a) ? "on\n" : "off\n");
  std::cout << " bit 7 of 'c' is " << ((c & b) ? "on\n" : "off\n");

  std::cout << std::endl;

  return 0;
}
