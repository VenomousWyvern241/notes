/*
 * Example program to showcase use of source_location.
 *   Also used in debugging a compiler error - see relevant CMakeLists.txt for info.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -std=c++<VER> -o <file_name>`
 *     - VER: 11 (NF), 17(F), 20, etc to check for failure
 *   `$ cmake --build . -t <file_name>`
 * Run (local): `$ ./source_location_example`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2023/August/23
 *
 * Note:
 *   Determine default c++ standard for compiler. (https://stackoverflow.com/a/46980534)
 *   $  g++ -x c++  -E -dM -< /dev/null | grep __cplusplus
 *   -> Codes at https://en.cppreference.com/w/cpp/preprocessor/replace#Predefined_macros
 *      199711 (C++98)
 *      201103 (C++11), 201402 (C++14), 201703 (C++17)
 *      202002L (C++20), 202302L (C++23)
 */

#include <cstdio>
#include <experimental/source_location>
#include <string>

#define UNUSED(expr) do { (void)(expr); } while (0)

using namespace std;

/**
 * Display an inline message.
 *
 * @param message A message to display.
 * @param location A source code location.
 */
void inline_message(const string &message, const experimental::source_location location =
                                              std::experimental::source_location::current())
{
  printf("%s:%s():ln%u\n", location.file_name(), location.function_name(), location.line());
  printf("  message: %s\n", message.c_str());
}

/**
 * Do nothing.
 */
void do_nothing()
{
  // Do nothing.
}

/**
 * Dummy function - just call function under test.
 */
void do_something()
{
  inline_message("Doing something (but not really).");
}

/**
 * Showcase source_location use.
 */
int main(int argc, char **argv)
{
  UNUSED(argc);
  UNUSED(argv);

  inline_message("Ready.... set..... gooooo!");
  do_something();
  do_nothing();
  do_something();

  return 0;
}
