/*
 * Example program to show use of `enum class` that uses bitwise operations.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2020/July/17
 */

#include <cstdio>
#include <iostream>
#include <string>

/* Contains available test status. */
enum class test : uint32_t
{
  BUSY = 0x100,
  WAIT = 0x200,
  ERR = 0x400
};

/* Operator Overloads */

inline bool operator&(uint32_t a, test b)
{
  return static_cast<bool>(a & static_cast<uint32_t>(b));
}

inline bool operator|(uint32_t a, test b)
{
  return static_cast<bool>(a | static_cast<uint32_t>(b));
}

inline uint32_t operator~(test b)
{
  return ~(static_cast<uint32_t>(b));
}

inline uint32_t& operator|=(unsigned& lhs, test rhs)
{
  lhs = lhs | static_cast<uint32_t>(rhs);
  return lhs;
}

inline uint32_t& operator&=(unsigned& lhs, test rhs)
{
  lhs = lhs & static_cast<uint32_t>(rhs);
  return lhs;
}

/**
 * Print the hexadecimal notation of the given enum.
 *
 * @param num The uint32_t representation of the enum.
 */
void printMethod(uint32_t num)
{
  std::cout << std::hex << num << std::endl;
}

/**
 * Decode the hexidecimal notation of the given enum.
 *
 * @param num The uint32_t representation of the enum.
 */
void decodeMethod(uint32_t num)
{
  printMethod(num);

  if (num & test::BUSY)
    std::cout << "test is busy" << std::endl;
  else if (num & test::WAIT)
    std::cout << "test is waiting" << std::endl;
  else if (num & test::ERR)
    std::cout << "test had an error" << std::endl;
  else
    std::cout << "test is done" << std::endl;
}

/**
 * Showcase enum class.
 */
int main()
{
  uint32_t var = 0x1;

  // Will print the value of BUSY.
  printMethod(static_cast<uint32_t>(test::BUSY));
  std::cout << std::endl;

  // Set to busy.
  var |= test::BUSY;
  decodeMethod(var);
  std::cout << std::endl;

  // Clear busy and set to wait.
  var &= ~(test::BUSY);
  var |= test::WAIT;
  decodeMethod(var);
}
