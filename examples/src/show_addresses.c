/*
 * Demo variable addresses.
 *
 * Compile:
 *   `$ gcc <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./show_addresses`
 *
 * @VenomousWyvern241
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int global;

int main()
{
  int stack;

  // Alocate heap memory.
  int *heap = malloc(sizeof(int));

  printf("global    addr %p\n", &global);
  printf("stack     addr %p\n", &stack);
  printf("heap      addr %p\n", heap);

  // Free heap memory.
  free(heap);
}
