/*
 * Example program to showcase usage of 'const'.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run (local): `$ ./const_example`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2023/February/17
 */

#include <iostream>
using namespace std;

/* Variables */

static const double arr[] = {0, 1, 2, 3, 4, 5};

/**
 * Print the contents of an array.
 * NOTE: Template allows for any size array to be passed.
 *
 * @param arr - constant array (of any size 'S') of doubles.
 */
template <std::size_t S>
void print_array(const double (&arr)[S])
{
  int lut_size = sizeof(arr) / sizeof(double);
  for (int i = 0; i < lut_size; i++)
    cout << arr[i] << " ";
  cout << endl;
}

/**
 * Showcase const usage.
 */
int main()
{
  std::cout << "------ static const array ------" << std::endl;
  print_array(arr);

  return 0;
}
