/* file: binding_experiment_cpp.i */
/* Derived from: https://www.geeksforgeeks.org/wrapping-cc-python-using-swig-set-1/ */
/* @ Venomouswyvern241 2022/11/30 */

/* Name the module. */
%module binding_experiment_cpp
%{
  /*
   * Everything in this file is being copied in the wrapper file.
   * We include the CPP header file necessary to compile the interface.
   */
  #include "binding_experiment.hpp"
  /* [As applicable] add relevant namespace for the include, to tell swig where to look. */

  /* Additional Headers */

  /* Additional Variable Declaration */

%}

/* Include modules that define macros used to generate wrappers. */
/*  - see https://www.swig.org/Doc1.3/Library.html */
%include "std_string.i"

/* array - needs macro specified. */
%include "carrays.i"
%array_class(int, intArray);

/* shared_ptr - needs name of object that is shared.*/
%include <std_shared_ptr.i>
%shared_ptr(sp_ex);

/* 
! REPLACED WITH TYPEMAPS IMPLEMENTATION IMMEDIATELY BELOW !
Courtesy of: https://stackoverflow.com/a/44584054 - "1. Use %extend to provide an overload" 
Notes:
 - Return via ref; create wrapper only overload to temporarily store the value, then return. 
   - If using this method, the overloaded function needs to be %ignored (see below). 

%extend Fibonacci {
  Mode get_mode(Mode mode)
  {
    // For simplicity if overloading in cpp keep existing parameters, but change them in some
    //   minor way (remove the reference or pointer / change type) and mark as unused.
    UNUSED(mode);
    // Create temporary storage for 'normal' call.
    Mode m = Mode::UNKNOWN;
    // Call 'normal' call.
    $self->get_mode(m);
    // Return the stored value.
    return m;
  }
}
*/

%include "typemaps.i"
/* 
Courtesy of: 
 https://riptutorial.com/swig/topic/9289/introduction-to-typemaps
 https://www.swig.org/Doc1.3/Typemaps.html#Typemaps_nn25
 https://stackoverflow.com/a/21414174/15080704
 https://stackoverflow.com/a/70524080/15080704

Notes:
 - printf statments are functional, uncommnt to display data.
 - need to be visible before the relevant %include.
 - %typemap(NAME[, OPTION=VALUE]) TYPENAME [(LOCALVARTYPE LOCALVARNAME)] { CODE }
   - NAME:
     - in;  used to convert function arguments from the target language to C.
     - out; used to convert function/method return values from C into the target language.
     - arginit; used to set the initial value of a function argument (before any conversion has occurred), not normally necessary.
     - argout; used to return values from arguments, almost always paired with an "in".
     - check; used to supply value checking code during argument conversion, applied after arguments have been converted.
     - default; used to turn an argument into a default argument.
     - freearg; used to cleanup argument data, only used when argument might have allocated resources requiring cleaning.
     - meberin; used to copy data from an already converted input into a structure member, typically used to handle array members / special cases.
     - newfree; (only with %newobject directive) used to deallocate memory used by the return result of a function.
     - throws; (only parsing a C++ method with exception or %catches) used to provide a default handler for C++ methods with declared exceptions to be thrown.
     - typecheck; used to support overloaded functions and methods, checks if argument matches a specific type. 
     - varin; (implementation specific) used to convert objects in the target language to C for the purposes of assigning to a C/C++ global variable.
     - varout; (implementation specific) used to convert a C/C++ object to an object in the target language when reading a C/C++ global variable.
   - [, OPTION=VALUE]; (OPTIONAL) extra options to influence behavior.
     - numinputs=0; take no input from the target language.
   - TYPENAME: the type that this will be applied to - qualifiers matter.
     - if variable name unspecified then it is applied to all functions with TYPENAME
   - (LOCALVARTYPE LOCALVARNAME): extra local variable to hold objects around the call.
   - CODE: implementation of conversion.
 - Special Variables (applicable typemap NAME)
   - $input (in) is the value FROM PYTHON.
   - $result (out) is the thing being returned TO PYTHON.
   - $1 (in/out) is the C/CPP variable matching the typemap.
     - (in) assign to
     - (out) read from
*/
/* Typemap for passing: Mode */
%typemap(in, numinputs=0) Mode& 
{
  //printf(" --- get_mode(Mode &mode) ---\n");
  //printf(" - IN has %d arguments.\n", $argnum);
  //printf("   \"%s1\" is (object address to be given to python i.e. save location for Mode).\n", "$");
  Mode m = Mode::UNKNOWN;
  $1 = &m;
  //printf("     initialized value of %s1: %p (%d)\n", "$", $1, static_cast<int>(*$1));
}
%typemap(argout) Mode& 
{
  //printf(" --- get_mode(Mode &mode) ---\n");
  //printf(" - ARGOUT has %d arguments.\n", $argnum);
  //printf("   \"%s1\" is (object address from python i.e. retrieved Mode).\n", "$");
  //printf("   argout: %d\n", static_cast<int>(*$1));
  $result = PyInt_FromLong(reinterpret_cast<long>(static_cast<long int>(*$1)));
  // For functions with one or more return value(s) (ex: success/fail + &A + &B) the result needs to be appended.
  // $result = SWIG_Python_AppendOutput($result, PyInt_FromLong(reinterpret_cast<long>(static_cast<long int>(*$1))));
}
%typemap(in) Mode
{
  //printf(" --- <print|set>_mode(Mode mode) ---\n");
  //printf(" - IN has %d arguments.\n", $argnum);
  //printf("   \"%sinput\" is (object holding value to be converted).\n", "$");
  //printf("   address of %sinput: %p\n", "$", $input);
  //printf("   value of %sinput: %li\n", "$", PyInt_AsLong($input));
  $1 = static_cast<Mode>(PyInt_AsLong($input));
}

/* Typemap for passing: structure */
%typemap(in, numinputs=0) structure&
{
  structure *s = new structure{0, "unknown", 0};
  $1 = s;
}
%typemap(argout) structure&
{
  $result = SWIG_Python_AppendOutput($result, SWIG_NewPointerObj($1, $1_descriptor, SWIG_POINTER_OWN));
}

/* Interfaced Functions / Variables */
/* PreOption: Ignore any python incompatible operator overloads. */
/*   https://www.swig.org/Doc4.0/SWIGPlus.html#SWIGPlus_nn28 */
/*   SWIG ignores operator friend declarations; may need to use ignore everywhere below. */
/*   %ignore [namespace::]<class>>::operator=; // Ignore = in specific class */
/*   %ignore *::operator=;                     // Ignore = in all classes */
/*   %ignore operator=;                        // Ignore = everywhere. */
/* Option 1: Explicitly list all to be interfaced. */
/* Option 2: If all functions and variables are desired just include: */
/*  (b) relevant '#include ...' headers within (a); must be listed BEFORE (a) */
/*  (a) the module specific header file(s) (listed within the '%module' block above) */
/*   ^^^ (b) fixes: "swig/python detected a memory leak of type ___, no destructor found." */
%include "binding_experiment.hpp"
