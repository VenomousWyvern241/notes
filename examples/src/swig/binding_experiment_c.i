/* file: binding_experiment_c.i */
/* Derived from: https://www.geeksforgeeks.org/wrapping-cc-python-using-swig-set-1/ */
/* @ Venomouswyvern241 2022/12/01 */

/* Name the module. */
%module binding_experiment_c
%{
  /*
   * Everything in this file is being copied in the wrapper file.
   * We include the CPP header file necessary to compile the interface.
   */
  #include "binding_experiment.h"
  /* [As applicable] add relevant namespace for the include, to tell swig where to look. */

  /* Additional Headers */

  /* Additional Variable Declaration */

%}

/* Include modules that define macros used to generate wrappers. */
/*  - see https://www.swig.org/Doc1.3/Library.html */

/* array - needs macro specified. */
%include "carrays.i"
%array_class(int, intArray);

/* Interfaced Functions / Variables */
/* Option 1: Explicitly list all to be interfaced. */
/* Option 2: If all functions and variables is desired just include the header. */
%include "binding_experiment.h"
