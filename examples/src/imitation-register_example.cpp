/*
 * Example for how to access/use registers - since I do not have actual hardware this is FAKE.
 *
 * @VenomousWyver241 2023/April/20
 */

#include "imitation-register_example.hpp"

#include <iostream>

#include "imitation-register_example_constants.hpp"

RegisterExample::RegisterExample()
{
}

RegisterExample::~RegisterExample()
{
}

void RegisterExample::readFromRegister(uint32_t addr, uint32_t &data)
{
  UNUSED(data);
  printf("Reading from 0x%x.\n", addr);
}

void RegisterExample::writeToRegister(uint32_t addr, uint32_t data)
{
  printf("Writing %u to 0x%x.\n", data, addr);
}

void RegisterExample::setup()
{
  uint32_t data = 0;

  readFromRegister(REG_CONTROL, data);

  if (data & REG_CONTROL_UNCFGD_Msk)
  {
    // Write the reset bits.
    writeToRegister(REG_RESET, (REG_RESET_INVERT_Msk | REG_RESET_TEST_MD_Msk));

    // Update to denote the device is configured.
    data &= (~REG_CONTROL_UNCFGD_Msk);
    writeToRegister(REG_CONTROL, data);
  }
}
void RegisterExample::performAction()
{
  uint32_t data = 0;

  readFromRegister(REG_CONTROL, data);

  // Handle option a: setting appropriate flags.
  if (data & REG_CONTROL_OPT_A_Msk)
    data |= REG_CONTROL_FLAG_1_Msk;
  else
    data |= REG_CONTROL_FLAG_2_Msk;

  writeToRegister(REG_CONTROL, data);

  // Handle option b: bumping the patch.
  if (data & REG_CONTROL_OPT_B_Msk)
  {
    uint32_t tmp_data = 0;
    
    // Read the ID register.
    readFromRegister(REG_ID, tmp_data);

    // Bump the ID register's patch number.
    uint32_t patch = (tmp_data & REG_ID_PAT_Msk) + (1 << REG_ID_PAT_Sft);
    tmp_data &= ~(REG_ID_PAT_Msk);
    tmp_data |= patch;

    // Write the new value.
    writeToRegister(REG_ID, tmp_data);
  }

  // Setup if needed.
  if (data & REG_CONTROL_SETUP_DEVICE_Msk)
    setup();
}
