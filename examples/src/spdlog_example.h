/*
 * Example program to show use of spdlog.
 *
 * @VenomousWyvern241 2022/May/16
 */

#ifndef SPDLOG_EXAMPLE_H
#define SPDLOG_EXAMPLE_H

#include <spdlog/spdlog.h>

/**
 * Manages all interaction with example.
 */

class SpdLogEx
{
 public:
  /**
   * Constructor.
   */
  SpdLogEx();

  /**
   * Destructor.
   */
  ~SpdLogEx();

  /**
   * Disables debugging.
   */
  void disableDebug();

  /**
   * Enables debugging.
   */
  void enableDebug();

  /**
   * Check if debugging is enabled.
   *
   * @return True if so, False otherwise.
   */
  bool isDebugOn() const;

  /**
   * Returns the full path to the logging file.
   *
   * @return The full path of the logging file.
   */
  std::string getLog() const;

  /**
   * Sets the logging files path and name.
   *
   * @param path - new location the logging file will be saved under.
   * @param fn - the new name for the logging file.
   */
  void setLog(const std::string &path, const std::string &fn);

  /**
   * Collect some basic data to differentiate this logging messge set from any previous entries.
   */
  void collectBaseInformation();

  /**
   * Stub function to pretend something happened.
   */
  void doSomething();

  /**
   * Stub function to pretend something bad happened.
   */
  void doSomethingBad();

  /**
   * Stub function to pretend something debugging related happened.
   */
  void doSomethingElse();

 private:
  // Name of the log file.
  std::string log_fn_;
  // Path to the log file.
  std::string log_path_;
  // Spdlogger.
  std::shared_ptr<spdlog::logger> logger_;
};

#endif  // SPDLOG_EXAMPLE_H
