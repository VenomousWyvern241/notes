/*
 * PSUEDO example to show use of SPI - specifically the 4 wire variant.
 *
 * ! For purposes of note taking / layout only, not compilable. !
 *
 * Chip Select [CS]: usually active low, output from master to indicate that data is being sent.
 * Master In ChipAnode Out [MISO]: data output from subnode.
 * Master Out ChipAnode In [MOSI]: data output from master.
 * Serial Clock [SCLK]: generated and output from master.
 *
 * Three wire variant combines the input (MISO) and output (MOSI) into one line.
 *
 * @VenomousWyvern241 2022/March/17
 *
 * Resources:
 * - (1) https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html
 * - (2) https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi/all
 * - (3) https://learn.adafruit.com/circuitpython-basics-i2c-and-spi/spi-devices
 * - (4) https://en.wikipedia.org/wiki/Serial_Peripheral_Interface#Mode_numbers
 */
#include <pair>
#include <tuple>

/*
 * Interface for interacting with SPI.
 */
class Spi
{
 public:
  // Modes: mode, clock(polarity, phase[, edge]) (source: 4)
  enum ClockMode
  {
    CLK_UNKNOWN,
    CLK_M_0,
    CLK_M_1,
    CLK_M_2,
    CLK_M_3
  };
  // - Microchip PIC / ARM-Based Microcontrollers
  const std::map<enum ClockMode, std::tuple<uint8_t, int8_t, uint8_t>> PAClockModes = {
      {CLK_UNKNOWN, {-1, -1, -1}}, {CLK_M_0, {0, 0, 1}}, {CLK_M_1, {0, 1, 0}},
      {CLK_M_2, {1, 0, 1}},        {CLK_M_3, {1, 1. 0}},
  };
  // - Other Microcontrollers
  const std::map<enum ClockMode, std::pair<uint8_t, uint8_t>> ClockModes = {
      {CLK_UNKNOWN, std::make_pair{-1, -1}}, {CLK_M_0, std::make_pair(0, 0)},
      {CLK_M_1, std::make_pair(0, 1)},       {CLK_M_2, std::make_pair(1, 0)},
      {CLK_M_3, std::make_pair(1, 1)},
  };

  explicit Spi(uint32_t base_addr);
  ~Spi();

  /***
   * SPI Specific Functions - will be in any SPI implmementation.
   ***/

  /*
   * Read from a register on an SPI peripheral.
   *
   * @param addr - address to read from.
   * @param data - value at provided address.
   *
   * @return true on success.
   */
  bool read(uint32_t addr, uint32_t &data);

  /*
   * Write to a register on an SPI peripheral.
   *
   * @param addr - address to write to.
   * @param data - data to write.
   *
   * @return true on success.
   */
  bool write(uint32_t addr, uint32_t data);

  /*
   * Get the base address.
   *
   * @param base_addr - the current base address.
   *
   * @return true on success.
   */
  bool getBaseAddr(uint32_t &base_addr);

  /*
   * Get the clock frequency.
   *
   * @param freq - the current frequency in hertz.
   *
   * @return true on success.
   */
  bool getClockFrequency(double &freq);

  /*
   * Set the clock frequency.
   *
   * @param freq - new clock frequency in hertz.
   *
   * @return true on success.
   */
  bool setClockFrequency(double freq);

  /*
   * Get the clock mode: phase + polarity.
   *
   * @param mode - the current mode.
   *
   * @return true on success.
   */
  bool getClockMode(ClockMode &mode);

  /*
   * Set the clock mode: phase + polarity.
   *
   * @param mode - new mode.
   *
   * @return true on success.
   */
  bool setClockMode(ClockMode mode);

  /*
   * Check if the SPI interface is currently in use.
   *
   * @return true if busy, false if not.
   */
  bool isBusy();

  /***
   * Generic Product Implementation - will be in any specialization of the main interface.
   ***/

  /*
   * Lock use of SPI; avoids garbage data.
   *
   * @return true if locked, false if unsuccessful.
   */
  bool lock();

  /*
   * Release the SPI lock.
   *
   * @return true on successful unlock.
   */
  bool unlock();

   /*
   * Fetch the SPI module id.
   *   Note: ID register contents decided by board manufacturer (via firmware) fetched via direct
   * register access `read(...)`.
   *
   * @param mid - the SPI device's module ID.
   *
   * @return true on successful retrieval.
   */
  bool getSpiMid(uint32_t &mid);

  /*
   * Read data from a component register (using SPI Read Frame).
   *
   * @param data - data returned in the frame.
   *
   * @return true if there is data returned.
   */
  bool readFrame(uint32_t &data);

  /*
   * Write data to a component register (using SPI Write Frame).
   *
   * @param data - data to write to the frame.
   *
   * @return true on successful write.
   */
  bool writeFrame(uint32_t data);

  /*
   * Set the direction read/write for a transaction.
   *
   * @param read - true for read, false for write.
   *
   * @return true if direction is set properly.
   */
  bool setTransactionDirection(bool read);

  /*
   * Create a <read/write>Transaction frame.
   *  Note: parameter type may change depending on SPI Frame capacity/capability.
   *
   * @param read_trans - true if building for a read transaction.
   * @param addr - the component register address to read/write - specified my manufaccturer.
   * @param data - data to write (ignored for a read transaction).
   * @param frame - completed transaction frame.
   *
   * @return true.
   */
  bool createFrame(bool read, uint32_t addr, uint32_t data, uint32_t &frame)

  /*
   * Perform a read transaction for the SPI component.
   *  Set direction to read, writeFrame(address to read from), and readFrame(data).
   *
   * @param frame - frame to write (component address).
   * @param data - data from read.
   *
   * @return true if read is successful.
   */
  bool readTransaction(uint32_t frame, uint32_t &data);

  /*
   * Perform a write transaction for the SPI component.
   *  Set direction to write, writeFrame(address + data).
   *
   * @param frame - frame to write (component address + data).
   *
   * @return true if write is successful.
   */
  bool writeTransaction(uint32_t frame);

  /***
   * Component Specific Implementation - will only be in component specific code.
   ***/

  /*
   * Fetch the components module id.
   *   Note: ID register contents decided by board manufacturer (via firmware) fetched via direct
   * register access `read(...)`.
   *
   * @param mid - the component's module ID.
   *
   * @return true on successful retrieval.
   */
  bool getComponentMid(uint32_t &mid);

  /*
   * Check the component's setup.
   *   Note: control register contents decided by component manufacturer fetched via indirect
   * register access `readTransaction(...)`.
   *
   * @param ctrl - the data within the components control register.
   *
   * @return true on successful retrieval.
   */
  bool getComponentControl(uint32_t &ctrl);

 private:
  // Only Main generates the clock.
  ClockMode clock_mode_;  // Clock Mode.
  uint32_t clock_freq_;   // Clock frequency (Hz).
  uint32_t base_addr_;    // SPI Node base address.
};

/*
 * Implementation of chip "A" that has an individual SPI interface.
 *   "A" is the only peripheral on this SPI.
 */
class ChipA
{
 public:
  explicit ChipA(SpiMain &spi);
  ~ChipA();

  /*
   * A function to initiate doing stuff.
   */
  void doStuff();

 private:
  SpiMain spi_;
};

/*
 * Implementation of chip "B" that has a shared SPI interface.
 *   "B" is one of two peripherals on this SPI; the other is "C".
 */
class ChipB
{
 public:
  explicit ChipB(SpiMain &spi);
  ~ChipB();

  /*
   * A function to initiate doing something.
   */
  void doSomething();

 private:
  SpiMain spi_;
};

/*
 * Implementation of chip "C" that has a shared SPI interface.
 *   "C" is one of two peripherals on this SPI; the other is "B".
 */
class ChipC
{
 public:
  explicit ChipC(SpiMain &spi);
  ~ChipC();

  /*
   * A function to initiate doing nothing.
   */
  void doNothing();

 private:
  SpiMain spi_;
};
