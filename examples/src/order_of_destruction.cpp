/*
 * Example program to showcase destructor user/ordering.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run (local): `$ ./order_of_destruction`
 *
 * @VenomousWyvern241 2023/March/14
 * Based on:
 * - https://learn.microsoft.com/en-us/cpp/cpp/destructors-cpp?view=msvc-170#order-of-destruction
 */

#include <cstdio>

/* Cat interface. */
class Cat
{
 public:
  explicit Cat();
  ~Cat();
};

Cat::Cat()
{
  printf(" - Creating: Cat\n");
}

Cat::~Cat()
{
  printf(" - Entering DTOR: Cat\n");
}

/* Child interface. */
class Child
{
 public:
  explicit Child();
  ~Child();
};

Child::Child()
{
  printf(" - Creating: Child\n");
}

Child::~Child()
{
  printf(" - Entering DTOR: Child\n");
}

/* Dog interface. */
class Dog
{
 public:
  explicit Dog();
  ~Dog();
};

Dog::Dog()
{
  printf(" - Creating: Dog\n");
}

Dog::~Dog()
{
  printf(" - Entering DTOR: Dog\n");
}

/* Parent interface. */
class Parent
{
 public:
  explicit Parent();
  ~Parent();

 private:
  Child oldest__;
  Child youngest_;
};

Parent::Parent()
{
  printf(" - Creating: Parent\n");
}

Parent::~Parent()
{
  printf(" - Entering DTOR: Parent\n");
}

/* Dad instance of parent interface. */
class Dad : public Parent
{
 public:
  explicit Dad();
  ~Dad();
};

Dad::Dad() : Parent()
{
  printf(" - Creating: Dad\n");
}

Dad::~Dad()
{
  printf(" - Entering DTOR: Dad\n");
}

/* Mom instance of parent interface. */
class Mom : public Parent
{
 public:
  explicit Mom();
  ~Mom();
};

Mom::Mom() : Parent()
{
  printf(" - Creating: Mom\n");
}

Mom::~Mom()
{
  printf(" - Entering DTOR: Mom\n");
}

/* Family interface. */
class Family
{
 public:
  explicit Family();
  ~Family();

 private:
  Dad D_;
  Mom M_;
  Cat c_;
  Dog d_;
};

Family::Family()
{
  printf(" - Creating: Family\n");
}

Family::~Family()
{
  printf(" - Entering DTOR: Family\n");
}

/**
 * Showcasing order of destruction for included classes.
 */
int main()
{
  printf("--- ---  - Creating of new Parent* --- ---\n");
  printf("Parent *p = new Parent();\n");
  Parent *p = new Parent();
  printf("\n");
  printf("// Must \'delete\' as \"p\" was created with \'new\'.\n");
  printf("delete p;\n");
  delete p;

  printf("\n");

  printf("--- --- Declaraion of Family --- ---\n");
  printf("Family f;\n");
  Family f;
  printf("\n");
  printf(
      "// Example program ending; no need to call destructor, will be auto-called when \"f\" goes "
      "out of scope.\n");

  return 0;
}
