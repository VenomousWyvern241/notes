/*
 * Example program to showcase filling a file with single repetative character.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./<file_name>`
 * Examine Output: `$ xxd filled_file.txt | less`
 *
 * @VenomousWyvern241 2025/February/4
 */

#include <stdint.h>

#include <array>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;

/**
 * Implementation.
 */
int main()
{
  // Create/Open file.
  ofstream output_file;
  char filename[] = "filled_file.txt";
  output_file.open(filename, ofstream::binary);

  // Verify file is open.
  if (!output_file.is_open())
  {
    printf("ERROR: opening file '%s'!\n", filename);
    return 1;
  }

  // Create buffer for populating file.
  char fill_ch = 0xA5;
  const int fill_buff_len = 8 * 1024;
  array<uint32_t, fill_buff_len> fill_buff;
  memset(fill_buff.data(), fill_ch, fill_buff_len * sizeof(uint32_t));

  // Write buffer to file.
  output_file.write((char*)&fill_buff, sizeof(fill_buff));

  // Close file.
  output_file.close();

  return 0;
}
