/*
 * Example program to show use of spdlog.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 *
 * @VenomousWyvern241 2022/May/16
 */

#include "spdlog_example.h"

int main(int argc, char *argv[])
{
  SpdLogEx example;

  // Turn on debugging logs and show use.
  example.enableDebug();

  example.doSomething();
  example.doSomethingBad();
  example.doSomethingElse();

  // Change log file and continue logging.
  example.setLog("", "example2.log");

  example.doSomethingBad();
  example.doSomethingBad();
  example.doSomethingElse();

  // Change log file and disable logging.
  example.setLog("", "example3.log");
  example.disableDebug();

  example.doSomethingBad();
  example.doSomethingElse();
}
