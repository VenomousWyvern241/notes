/*
 * Example program to showcase use of a map containing a pair value.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run (local): `$ ./mapped_pair_example -c high`
 *
 * @VenomousWyvern241 2022/February/8
 */

#include <cxxopts.hpp>
#include <iostream>
#include <map>

#include "fmt/format.h"

using namespace std;
using fmt::print;

/* Caffine Rating, (Bean Roast, Brew Style) */
const std::map<std::string, std::pair<std::string, std::string>> CaffineRating = {
    {"HIGH", std::make_pair("Blonde", "Cold Brew")},
    {"MED-HIGH", std::make_pair("Light", "Espresso")},
    {"MEDIUM", std::make_pair("Medium", "French Press")},
    {"MED-LOW", std::make_pair("Dark", "Drip")},
    {"LOW", std::make_pair("?", "Instant")},
};

/**
 * Showcase map containing pair.
 */
int main(int argc, char **argv)
{
  string caffine_level = "";

  // Parse arguments.
  try
  {
    cxxopts::Options options("Mapped Pair Example - Examples of a map using a pair.");
    options.add_options()("c, caffine_level",
                          "Level of caffine desired. (high, med-<high|low>, medium, low)",
                          cxxopts::value<string>(caffine_level))("h,help", "Print help");

    auto result = options.parse(argc, argv);

    if (result.count("help"))
    {
      print("{}\n", options.help());
      return 0;
    }
  }
  catch (const cxxopts::exceptions::exception &e)
  {
    print("Error parsing options: {}\n", e.what());
    return 1;
  }

  // Transform input request into all capitals.
  transform(caffine_level.begin(), caffine_level.end(), caffine_level.begin(), ::toupper);

  // Display coffee suggestion for requested caffine level.
  auto it = CaffineRating.find(caffine_level);
  if (it != CaffineRating.end())
  {
    print("For {} caffinated coffee try:\n", caffine_level);
    print("  Bean Roast: {}\n", it->second.first);
    print("  Brewing Style: {}\n", it->second.second);
  }
  else
  {
    print("ERROR invalid input.");
  }

  return 0;
}
