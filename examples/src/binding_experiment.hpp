/*
 * Part of a set of example programs to investigate python bindings for C vs CPP.
 * Programs have the same use case and function names for comparisons sake.
 *
 * @VenomousWyvern241 2022/November/18
 */

#ifndef BINDING_EXPERIMENT_HPP
#define BINDING_EXPERIMENT_HPP

#include <list>
#include <memory>
#include <string>

#define UNUSED(expr) \
  do                 \
  {                  \
    (void)(expr);    \
  } while (0)

/***** IMPORTANT: all desired functions must be in the header. *****/

// `: int` specifies the underlying type.
enum class Mode : int
{
  NONE = 0,  // Invalid: first entry defaults to 0, explicitly setting for clarity.
  NORMAL,    // Normal Use: Fibonacci Sequence related.
  TEST,      // Test Use: To confirm that various pieces work.
  BAD,       // ! DO NOT USE ! bad mode
  INVALID,   // ! DO NOT USE !
  UNKNOWN
};

// List of patterns that are either not reccomended or not functional.
const std::list<Mode> invalid_patterns = {Mode::BAD, Mode::INVALID, Mode::UNKNOWN};

/* Struct for example use of shared_ptr.*/
struct sp_ex
{
  int n;
};

struct structure
{
  int integer_value;
  std::string text_value;
  double double_value;
};

class Fibonacci
{
 public:
  Fibonacci();
  ~Fibonacci();

  /*
   * Basic test to return a provided value.
   *
   * @param n - a random integer.
   *
   * @return the provided parameter.
   */
  int test_return_int(int n);

  /*
   * Basic test to return a provided value.
   *
   * @param str - a random string.
   *
   * @return the provided parameter.
   */
  std::string test_return_string(std::string str);

  /*
   * Basic test to print a string.
   */
  void test_print();

  /*
   * Calculate the Fibonacci sequence up to digit N.
   *
   * @param n - number of digits to calculate.
   *
   * @return Integer array containing the sequence.
   */
  int *calculate_seq(int n);

  /*
   * Calculate the Fibonacci sequence up to digit N.
   *
   * @param n - number of digits to calculate.
   * @param arr - integer array to contain the sequence.
   */
  void calculate_seq_void(int n, int *arr);

  /*
   * Retrieve the Nth digit of the Fibonacci sequence.
   *
   * @param n - Nth digit.
   *
   * @return value of Nth digit of the Fibonacci sequence.
   */
  int get_nth_digit(int n);

  /*
   * Print N digits of the Fibonacci sequence.
   *
   * @param n - number of digits to print.
   */
  void print_n_digits(int n);

  /*
   * Set the running mode.
   *
   * @param mode - new mode to run.
   */
  void set_mode(const Mode mode);

  /*
   * Get the running mode.
   *
   * @mode the current running mode.
   */
  void get_mode(Mode &mode);

  /*
   * Print the specified mode or if unspecified, the current running mode.
   *
   * @param - [optional]  mode that needs printing.
   */
  void print_mode(Mode mode);

  /* **************************************************************************************** */
  /* What follows are examples to show swig use - they do not hold a purpose in the is class. */
  /* **************************************************************************************** */

  /*
   * Create and return a shared_ptr.
   */
  std::shared_ptr<sp_ex> start_something_useless();

  /*
   * Pass a shared_ptr by reference and manipulate.
   *
   * @param sp Shared_Ptr to manipulate.
   */
  void do_something_useless(const std::shared_ptr<sp_ex> &sp);

  /*
   * Print a shared_ptr value.
   *
   * @param sp Shared_Ptr to display.
   */
  void print_something_useless(const std::shared_ptr<sp_ex> sp);

  /*
   * Pass a structure by reference and populate.
   *
   * @param s A structure to fill.
   */
  void initial_structure(structure &s);

  /*
   * Display a specified structure.
   *
   * @param s Log entry to print.
   */
  void print_structure(structure s);

 private:
  Mode mode_;

};     // end Fibonacci
#endif /* BINDING_EXPERIMENT_HPP */
