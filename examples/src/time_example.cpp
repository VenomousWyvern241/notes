/*
 * Example program to show creation of time_t from seconds.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./time_example`
 *
 * @VenomousWyvern241 2023/May/5
 *
 * Resources:
 * - (1)  https://en.cppreference.com/mwiki/index.php?title=cpp/chrono/c/strftime&oldid=144959
 * - (2) https://stackoverflow.com/a/50495821/15080704
 */

#include <chrono>
#include <iostream>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::seconds;
using std::chrono::system_clock;

/**
 * Get the current time as a time_t object.
 *
 * @return Appropriate time_t object; the number of seconds since Jan 1 1970.
 */
time_t getCurrentTime()
{
  // Parameter is ignored thus gives current.
  return time(NULL);
}

/**
 * Get specific time as a time_t object.
 *
 * @param sec The time to calculate; number of seconds fince Jan 1 1970.
 *
 * @return Appropriate time_t object; coresponding to the time specified.
 */
time_t getTimeFromSec(uint64_t sec)
{
  double d_sec = static_cast<double>(sec);
  time_t time = system_clock::to_time_t(
      system_clock::time_point(duration_cast<seconds>(duration<double>(d_sec))));
  return time;
}

/**
 * Showcase time_t calculation.
 */
int main()
{
  // More formatting options are available; see (1).
  const char format[] = "%Y/%b/%d %H:%M:%S";
  char time_str[30] = {'\0'};

  uint64_t seconds = 0x64558314;
  time_t time = getTimeFromSec(seconds);
  struct tm *info = localtime(&time);
  strftime(time_str, sizeof(time_str), format, info);

  std::cout << "Previous Time: " << time_str << std::endl;

  time = getCurrentTime();
  info = localtime(&time);
  strftime(time_str, sizeof(time_str), format, info);

  std::cout << "Current Time: " << time_str << std::endl;
}
