/*
 * Example program to show creation and use of lambda.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./lambda_example`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2020/July/16
 */

#include <iostream>
#include <string>

/* Test data points. */
struct testStruct
{
  uint32_t num[4];
  bool boolean1;
  bool boolean2;
};

/**
 * Print the contents of the provided structure.
 *
 * @param t The structure to display.
 */
void printStatus(const testStruct &t)
{
  if (t.boolean1)
    std::cout << "t.boolean1 = true" << std::endl;
  else
    std::cout << "t.boolean1 = false" << std::endl;

  if (t.boolean2)
    std::cout << "t.boolean2 = true" << std::endl;
  else
    std::cout << "t.boolean2 = false" << std::endl;

  std::cout << "t.num[0] " << std::hex << t.num[0] << std::endl;
  std::cout << "t.num[1] " << std::hex << t.num[1] << std::endl;
  std::cout << "t.num[2] " << std::hex << t.num[2] << std::endl;
  std::cout << "t.num[3] " << std::hex << t.num[3] << std::endl;
}

/**
 * Modify struct member boolean1.
 *
 * @param t The structure to interact with.
 *
 * @return Copy of structure with modified member.
 */
testStruct modBool1b(const testStruct &t_struct)
{
  testStruct ret = t_struct;
  ret.boolean1 = true;

  // Additionally modify the second element of the structures array.
  ret.num[2] |= 0x100;

  return ret;
}

/**
 * Modify struct member boolean2.
 *
 * @param t The structure to interact with.
 *
 * @return Copy of structure with modified member.
 */
testStruct modBool1a(const testStruct &t_struct)
{
  testStruct ret2 = t_struct;
  ret2.boolean2 = true;

  // Additionally modify the second element of the structures array.
  ret2.num[2] = 0x9;

  // Modify the other boolean value.
  return modBool1b(ret2);
}

/**
 * Modify struct member boolean1 in place.
 *
 * @param t The structure to interact with.
 */
void modBool2b(testStruct *t_struct)
{
  t_struct->boolean1 = true;

  // Additionally modify the second element of the structures array.
  t_struct->num[2] |= 0x100;
}

/**
 * Modify struct member boolean2 in place.
 *
 * @param t The structure to interact with.
 */
void modBool2a(testStruct *t_struct)
{
  t_struct->boolean2 = true;

  // Additionally modify the second element of the structures array.
  t_struct->num[2] = 0x9;

  // Modify the other boolean value.
  modBool2b(t_struct);
}

/**
 * Initialize the provided structure.
 *
 * @param t The structure to interact with.
 */
void setupStruct(testStruct &t)
{
  t.num[0] = 0x1;
  t.num[1] = 0x2;
  t.num[2] = 0x3;
  t.num[3] = 0x4;
  t.boolean1 = false;
  t.boolean2 = false;
}

/**
 * Showcase lambda use.
 */
int main()
{
  testStruct t_struct;
  testStruct t;
  setupStruct(t_struct);
  setupStruct(t);

  // Origional
  printStatus(t_struct);
  std::cout << std::endl;

  // Verison 1: returning a modified copy (t) of t_struct
  auto one_t = [&t_struct]() -> testStruct { return modBool1a(t_struct); };
  t = one_t();
  printStatus(t);
  std::cout << std::endl;

  // Version 2: modifying origional t_struct
  auto two_t = [&t_struct]() mutable { modBool2a(&t_struct); };
  two_t();
  printStatus(t_struct);
}
