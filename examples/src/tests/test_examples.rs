// Example gtk tests.
//
// Compile & Run: `$ cargo test <...>` [see "Run Directions" for detailed description]
//
// @VenomousWyvern241 2022/April/18
//
// References:
//  * https://doc.rust-lang.org/rust-by-example/testing.html
//  * https://doc.rust-lang.org/book/ch11-00-testing.html
//
// Run Directions
// * all (non-ignored) tests: `cargo test -- --test`
//   * runs functional_test and non_functional_test
// * ignored tests: `cargo test -- --ignored`
//   * runs all tests with `#[ignore]`
// * specific test: `cargo test <test_name>`
//   * `cargo test non_functional_test` will run only non_functional_test
// * tests containing <name_fragament>: `cargo test <name_fragment>`
//   * `cargo test functional` will run functional_test & non_functional_test

#[cfg(test)]
mod tests {
    #[test]
    fn functional_test() {
        let val = 13;
        let check = 10 + 3;
        assert_eq!(val, check)
    }

    #[test]
    #[should_panic]
    fn non_functional_test() {
        panic!("Make this test fail as expected");
    }

    #[test]
    #[ignore]
    fn ignore_test() {
        panic!("Make this test fail, unexpectedly");
    }
}

