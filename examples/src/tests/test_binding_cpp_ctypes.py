#!/usr/bin/python3

"""test_binding_cpp_ctypes.py

An example implementation of python CPP binding using ctypes.

Compile: `$ cd notes/build && cmake --build . --`
  * Library path found with find_libpath and attached at bottom.
Run: `$ python3 test_binding_cpp_ctypes.py [-i <int> -s <str> -f]`
  * Specifically: anywhere in the 'notes' dir `notes$ </full/path/to/>test_binding_cpp_ctypes.py`
Helpful Links:
  * https://realpython.com/python-bindings-overview/#python-bindings-overview
Resources:
  * https://github.com/Auctoris/ctypes_demo
  * https://stephenscotttucker.medium.com/
      interfacing-python-with-c-using-ctypes-classes-and-arrays-42534d562ce7

@VenomousWyvern241 2023/January/19
"""

import argparse
import ctypes
import ctypes.util
import pathlib
import sys

from enum import Enum


class Error(Exception):
    """Base error class."""


# Courtesy: https://stackoverflow.com/a/62097714
class Mode(Enum):
    """Class to represent cpp enum Mode."""

    NONE = 0
    NORMAL = 1
    TEST = 2

    @classmethod
    def from_param(cls, obj):
        """Translate Mode object into int value so it is usable by python."""
        if not isinstance(obj, Mode):
            raise TypeError("Not a Mode enumeration.")
        return ctypes.c_int8(obj)


def find_libpath(library_name):
    """Find the librarys full path.
    This will allow the library to be accessed regardless of where this script is called,
      so long as it is called from within the "notes" directory.

    Args:
        library_name: str - the name of the library to find.

    Raises:
        Error if path does not contain "notes".

    Returns:
        The full path to the library.
    """
    curr_path = pathlib.Path().absolute()

    # First find the top directory "notes".
    if "notes" not in str(curr_path):
        raise Error("Must be called from somewhere within the notes directory!")
    while curr_path.name != "notes":
        curr_path = curr_path.parent

    library_path = ""
    # Walk the dirs until libname is found.
    for path in curr_path.rglob(library_name):
        library_path = path

    return library_path


def execute(opts):
    """Execute some library commands.

    Args:
        opts: result of argparse.parse()
    """

    # Need to set the argument types and return types for all the used functions.
    #  Without this there can be segmentation faults due to truncation.
    cpp_lib.Fib_new.argtypes = []
    cpp_lib.Fib_new.restype = ctypes.c_void_p
    cpp_lib.Fib_test_return_int.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_test_return_int.restype = ctypes.c_int
    cpp_lib.Fib_test_return_string.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
    cpp_lib.Fib_test_print.restype = None
    cpp_lib.Fib_test_print.argtypes = [ctypes.c_void_p]
    cpp_lib.Fib_test_return_string.restype = ctypes.c_char_p
    cpp_lib.Fib_get_nth_digit.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_get_nth_digit.restype = ctypes.c_int
    cpp_lib.Fib_print_n_digits.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_print_n_digits.restype = None
    cpp_lib.Fib_calculate_seq.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_calculate_seq.restype = ctypes.POINTER(ctypes.c_int)
    cpp_lib.Fib_calculate_seq_void.argtypes = [
        ctypes.c_void_p,
        ctypes.c_int,
        ctypes.POINTER(ctypes.c_int),
    ]
    cpp_lib.Fib_calculate_seq_void.restype = None
    cpp_lib.Fib_set_mode.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_set_mode.restype = None
    cpp_lib.Fib_get_mode.argtypes = [ctypes.c_void_p]
    cpp_lib.Fib_get_mode.restype = ctypes.c_int
    cpp_lib.Fib_print_mode.argtypes = [ctypes.c_void_p, ctypes.c_int]
    cpp_lib.Fib_print_mode.restype = None

    # Create instance of Fibonacci to pass around.
    fib_instance = cpp_lib.Fib_new()

    print("==== Int & String Return Tests ====")

    answer = cpp_lib.Fib_test_return_int(fib_instance, opts.test_int)
    print(f"Test integer is: {answer}.")

    answer = cpp_lib.Fib_test_return_string(fib_instance, opts.test_str.encode("utf-8"))
    print(f"Test string is: {answer.decode('utf-8')}.")

    print("==== Printing Test ====")

    cpp_lib.Fib_test_print(fib_instance)

    print("==== Complicated Function Test ====")

    answer = cpp_lib.Fib_get_nth_digit(fib_instance, opts.test_int)
    print(f"Digit {opts.test_int} of the Fibonacci Sequence is: {answer}")

    cpp_lib.Fib_print_n_digits(fib_instance, opts.test_int)

    print("==== Int Array Test 1 (Return Array) ====")

    # Create the return variable as an int pointer.
    fib_arr = ctypes.POINTER(ctypes.c_int)
    fib_arr = cpp_lib.Fib_calculate_seq(fib_instance, opts.test_int)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {fib_arr[i]}")

    print("==== Int Array Test 2 (Pass Array) ====")

    # Create the return variable as a zeroed int array.
    arr = (ctypes.c_int * opts.test_int)()
    cpp_lib.Fib_calculate_seq_void(fib_instance, opts.test_int, arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {arr[i]}")

    print("==== Enum Class Test ====")

    cpp_lib.Fib_print_mode(fib_instance, Mode.NONE.value)
    print("Setting to normal...")
    cpp_lib.Fib_set_mode(fib_instance, Mode.NORMAL.value)
    cpp_lib.Fib_print_mode(fib_instance, Mode.NONE.value)
    print("Specifying test mode to print.")
    cpp_lib.Fib_print_mode(fib_instance, Mode.TEST.value)


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-i",
        "--test_int",
        type=int,
        default=9,
        help="Integer input used in execution.",
    )
    parser.add_argument(
        "-s",
        "--test_str",
        type=str,
        default="testing 123",
        help="Example",
    )
    parser.add_argument(
        "-d",
        "--example_flag",
        action="store_true",
        help="Example.",
    )

    opts = parser.parse_args()

    try:
        execute(opts)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    # Load the shared library into ctypes
    LIBNAME = "libbinding_experiment_lib_cpp.so"
    libpath = find_libpath(LIBNAME)

    try:
        cpp_lib = ctypes.CDLL(libpath)
    except OSError:
        print("Unable to load the system CPP library")
        sys.exit()

    print(f'Succesfully loaded the system CPP library from "{libpath}"')

    main()
