/* 
 * GoogleTest example; testing CPP binding_experiement.
 *
 * Compile:
 *   `build$ cmake --build . -t unit_cpp_binding_exp_test`
 * Execute:
 *   `build$ ./examples/src/tests/unit_cpp_binding_exp_test` (individually)
 *   - OR -
 *   build$ ctest --test-dir examples/src/tests/` (all tests in directory)
 *
 * @VenomousWyvern241 2023/April/18
 */

#include "binding_experiment.hpp"
#include "gtest/gtest.h"

using namespace std;

class CppBindingExpTest : public ::testing::Test
{
};

const int e_seq_n = 9;
const int e_seq[e_seq_n] = {0, 1, 1, 2, 3, 5, 8, 13, 21};

TEST_F(CppBindingExpTest, returnInt)
{
  Fibonacci fib;

  int test_int = 6;
  int return_int = fib.test_return_int(test_int);

  EXPECT_EQ(test_int, return_int);
}

TEST_F(CppBindingExpTest, returnString)
{
  Fibonacci fib;

  string test_str = "testing123";
  string return_str = fib.test_return_string(test_str);

  EXPECT_EQ(test_str, return_str);
} 

TEST_F(CppBindingExpTest, calcSeqRet)
{
  Fibonacci fib;

  int *seq = fib.calculate_seq(e_seq_n);

  for (int i = 0; i < e_seq_n; i++)
    EXPECT_EQ(e_seq[i], seq[i]);
}

TEST_F(CppBindingExpTest, calcSeqRef)
{
  Fibonacci fib;

  int seq[e_seq_n] = {};
  fib.calculate_seq_void(e_seq_n, seq);

  for (int i = 0; i < e_seq_n; i++)
    EXPECT_EQ(e_seq[i], seq[i]);
}

TEST_F(CppBindingExpTest, getDigit)
{
  Fibonacci fib;

  int digit = fib.get_nth_digit(e_seq_n);
  EXPECT_EQ(e_seq[(e_seq_n-1)], digit);
}

TEST_F(CppBindingExpTest, modeFetchRef)
{
  Fibonacci fib;
  Mode m = Mode::UNKNOWN;

  fib.get_mode(m);
  EXPECT_EQ(Mode::NONE, m);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
