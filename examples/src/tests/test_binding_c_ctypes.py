#!/usr/bin/python3

"""test_binding_c_ctypes.py

An example implementation of python C binding using ctypes.

Compile: `$ cd notes/build && cmake --build . --`
  * Library path found with find_libpath and attached at bottom.
Run: `$ python3 test_binding_c_ctypes.py [-i <int> -s <str> -f]`
  * Specifically: anywhere within the 'notes' dir `notes$ </full/path/to/>test_binding_c_ctypes.py`
Helpful Links:
  * https://realpython.com/python-bindings-overview/#python-bindings-overview

@VenomousWyvern241 2022/November/30
"""

import argparse
import ctypes
import ctypes.util
import pathlib
import sys


class Error(Exception):
    """Base error class."""


def find_libpath(library_name):
    """Find the librarys full path.
    This will allow the library to be accessed regardless of where this script is called,
      so long as it is called from within the "notes" directory.

    Args:
        library_name: str - the name of the library to find.

    Raises:
        Error if path does not contain "notes".

    Returns:
        The full path to the library.
    """
    curr_path = pathlib.Path().absolute()

    # First find the top directory "notes".
    if "notes" not in str(curr_path):
        raise Error("Must be called from somewhere within the notes directory!")
    while curr_path.name != "notes":
        curr_path = curr_path.parent

    library_path = ""
    # Walk the dirs until libname is found.
    for path in curr_path.rglob(library_name):
        library_path = path

    return library_path


def execute(opts):
    """Execute some library commands.

    Args:
        opts: result of argparse.parse()
    """
    print("==== Int & String Return Tests ====")

    answer = c_lib.test_return_int(opts.test_int)
    print(f"Test integer is: {answer}.")

    # Need to set the return type - python sees it as a byte strings.
    c_lib.test_return_string.restype = ctypes.c_char_p
    answer = c_lib.test_return_string(opts.test_str.encode("utf-8"))
    print(f"Test string is: {answer.decode('utf-8')}.")

    print("==== Printing Test ====")

    c_lib.test_print()

    print("==== Complicated Function Test ====")

    answer = c_lib.get_nth_digit(opts.test_int)
    print(f"Digit {opts.test_int} of the Fibonacci Sequence is: {answer}")

    c_lib.print_n_digits(opts.test_int)

    print("==== Int Array Test 1 (Return Array) ====")

    # Need to set the return type to an int pointer.
    c_lib.calculate_seq.restype = ctypes.POINTER(ctypes.c_int)
    # Create the return variable as an int pointer.
    fib_arr = ctypes.POINTER(ctypes.c_int)
    fib_arr = c_lib.calculate_seq(opts.test_int)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {fib_arr[i]}")

    print("==== Int Array Test 2 (Pass Array) ====")

    # Need to set the argument type(s) to an int and an int pointer.
    c_lib.calculate_seq.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_int)]
    # Create the return variable as a zeroed int array.
    arr = (ctypes.c_int * opts.test_int)()
    c_lib.calculate_seq_void(opts.test_int, arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {arr[i]}")

    # print("==== Enum Class Test ====")

    # fib.print_mode()
    # print("Setting to normal...")
    # fib.set_mode(fib.mode_normal())
    # fib.print_mode()
    # print("Specifying test mode to print.")
    # fib.print_mode(fib.mode_test())


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-i",
        "--test_int",
        type=int,
        default=9,
        help="Integer input used in execution.",
    )
    parser.add_argument(
        "-s",
        "--test_str",
        type=str,
        default="testing 123",
        help="Example",
    )
    parser.add_argument(
        "-d",
        "--example_flag",
        action="store_true",
        help="Example.",
    )

    opts = parser.parse_args()

    try:
        execute(opts)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    # Load the shared library into ctypes
    LIBNAME = "libbinding_experiment_lib_c.so"
    libpath = find_libpath(LIBNAME)

    try:
        c_lib = ctypes.CDLL(libpath)
    except OSError:
        print("Unable to load the system C library")
        sys.exit()

    print(f'Succesfully loaded the system C library from "{libpath}"')

    main()
