// Example gtk+rust program that creates a window with a button in it, that when clicked exits the program.
// Part of a comparison implementation, see: gtk_3-24_helloworld_test.c gtkmm_3-24_helloworld.cpp gtk_3-24_helloworld.rs gtk_3-24_helloworld_test.rs
//
// Influence from: gtk_3-24_helloworld.rs
//
// Compile & Run: `$ cargo test gtk_3-24_helloworld_test`
//
// @Venomouswyvern241 2022/April/18

extern crate gtk;
#[macro_use]
extern crate gtk_test;

use gtk::{
   prelude::ButtonExt, prelude::ContainerExt, prelude::GtkWindowExt, prelude::WidgetExt, 
   Button, Window, WindowType,
};

fn init_ui() -> (Window, Button) {
    gtk::init().unwrap();

    let window = Window::new(WindowType::Toplevel);

    window.set_title("Test Window");
    window.set_border_width(10);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(200, 200);

    let window_clone = window.clone();
    let button = gtk::Button::with_label("Hello World!");

    // https://users.rust-lang.org/t/gtk-rs-bc-wont-let-me-close-window/32060
    button.connect_clicked(move |_|  window_clone.close());

    window.add(&button);

    window.show_all();

    (window, button)
}

fn main() {
    let (w, b) = init_ui();

    // Check the title is correct.
    assert_title!(w, "Test Window");
    
    // Focus on the window.
    w.activate_focus();

    // Check that the lable is correct.
    assert_label!(b, "Hello World!");

    // Close application.
    gtk_test::click(&b);
    gtk_test::wait(1000);

    // If there is still an active window, panic.
    if w.activate_focus() { 
        panic!("Window still running!");
    }
}
