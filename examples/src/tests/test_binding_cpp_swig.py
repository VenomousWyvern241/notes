#!/usr/bin/python3

"""test_binding_cpp_swig.py

An example implementation of python CPP binding using swig.

Compile: `$ cd notes/build && cmake --build . --`
  * Path to module is appended below.
Run: `$ python3 test_binding_cpp_swig.py [-i <int> -s <str> -f]`
  * Specifically: `build$ pushd examples/src/swig/ &&
         python3 /home/haleigh/Documents/notes/examples/src/tests/test_binding_cpp_swig.py`
Helpful Links:
  * https://realpython.com/python-bindings-overview/#python-bindings-overview

@VenomousWyvern241 2022/November/30
"""

import argparse
import os
import sys

# Location of the <import>.py and _<import>.so need to be on PYTHONPATH.
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../../../build/examples/src/swig/python",
    )
)
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "../../../build/examples/lib"
    )
)
import binding_experiment_cpp as cpp_lib


class Error(Exception):
    """Base error class."""


def execute(opts):
    """Execute some library commands.

    Args:
        opts: result of argparse.parse()
    """
    fib = cpp_lib.Fibonacci()

    print("==== Int & String Return Tests ====")

    answer = fib.test_return_int(opts.test_int)
    print(f"Test integer is: {answer}.")

    answer = fib.test_return_string(opts.test_str)
    print(f"Test string is: {answer}.")

    print("==== Printing Test ====")

    fib.test_print()

    print("==== Complicated Function Test ====")

    answer = fib.get_nth_digit(opts.test_int)
    print(f"Digit {opts.test_int} of the Fibonacci Sequence is: {answer}")

    fib.print_n_digits(opts.test_int)

    print("==== Int Array Test (Return Array) ====")

    fib_arr = fib.calculate_seq(opts.test_int)
    # Create wrapper from existing pointer.
    #  - https://www.swig.org/Doc1.3/Library.html#Library_carrays
    array = cpp_lib.intArray.frompointer(fib_arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {array[i]}")

    print("==== Int Array Test 2 (Pass Array) ====")

    # Create new array containing zeros of length opts.test_int.
    #  - https://www.swig.org/Doc1.3/Library.html#Library_carrays
    arr = cpp_lib.intArray(opts.test_int)
    fib.calculate_seq_void(opts.test_int, arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {arr[i]}")

    print("==== Enum Class Test ====")

    save_mode = cpp_lib.Mode_NONE
    fib.print_mode(save_mode)
    print("Setting to normal...")
    fib.set_mode(cpp_lib.Mode_NORMAL)
    print("Attempting to save mode")
    save_mode = fib.get_mode()
    fib.print_mode(save_mode)
    print("Specifying test mode to print.")
    fib.print_mode(cpp_lib.Mode_TEST)
    print("Setting to test...")
    fib.set_mode(cpp_lib.Mode_TEST)
    # Special case here - overwriting the retrun by reference.
    save_mode = fib.get_mode()
    fib.print_mode(save_mode)

    print("==== shared_ptr Test ====")
    sptr_ex = fib.start_something_useless()
    fib.print_something_useless(sptr_ex)
    fib.do_something_useless(sptr_ex)
    fib.print_something_useless(sptr_ex)

    print("==== struct Test ====")
    struct = fib.initial_structure()
    fib.print_structure(struct)

    struct_two = cpp_lib.structure()
    struct_two.integer_value = 1
    struct_two.text_value = "secondary - showing nonsense"
    struct_two.double_value = 2.5
    fib.print_structure(struct_two)


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-i",
        "--test_int",
        type=int,
        default=9,
        help="Integer input used in execution.",
    )
    parser.add_argument(
        "-s",
        "--test_str",
        type=str,
        default="testing 123",
        help="Example",
    )
    parser.add_argument(
        "-d",
        "--example_flag",
        action="store_true",
        help="Example.",
    )

    opts = parser.parse_args()

    try:
        execute(opts)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    main()
