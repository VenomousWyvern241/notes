/*
 * GoogleTest example; testing register access.
 *
 * Compile:
 *   `build$ cmake --build . -t unit_register_example_test`
 * Execute:
 *   `build$ ./examples/src/tests/unit_register_example_test` (individually)
 *   - OR -
 *   build$ ctest --test-dir examples/src/tests/` (all tests in directory)
 *
 * @VenomousWyvern241 2023/April/28
 */

#include "gtest/gtest.h"
#include "imitation-register_example.hpp"
#include "imitation-register_example_constants.hpp"

using namespace std;

class RegisterExTest : public ::testing::Test
{
};

static const uint32_t ID_VERSION = 0x03071205;

class MockRegisterExample : public RegisterExample
{
 public:
  MockRegisterExample() : RegisterExample()
  {
    option_a_ = false;
    option_b_ = false;
    needs_setup_ = false;
  }

  void readFromRegister(uint32_t addr, uint32_t &data)
  {
    data = 0;

    // Push (addr, data) onto the vector for verification.
    commands_.push_back(make_pair(addr, 0));

    // Set data returned for specific addresses.
    if (addr == REG_CONTROL)
    {
      if (option_a_)
        data = REG_CONTROL_OPT_A_Msk;
      if (option_b_)
        data = REG_CONTROL_OPT_B_Msk;
      if (needs_setup_)
        data = REG_CONTROL_UNCFGD_Msk;
    }
    else if (addr == REG_ID)
    {
      data = ID_VERSION;
    }
  }

  void writeToRegister(uint32_t addr, uint32_t data)
  {
    // Push the (addr, data) onto the vector for verification.
    commands_.push_back(make_pair(addr, data));
  }

  vector<pair<uint32_t, uint32_t>> commands_;
  bool option_a_;
  bool option_b_;
  bool needs_setup_;
};

TEST_F(RegisterExTest, testSetup)
{
  MockRegisterExample mre;

  // Already completed.
  mre.needs_setup_ = false;
  mre.setup();
  EXPECT_EQ(1, mre.commands_.size());
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(0).first);
  mre.commands_.clear();

  // Needs to be done.
  mre.needs_setup_ = true;
  mre.setup();
  EXPECT_EQ(3, mre.commands_.size());
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(0).first);
  EXPECT_EQ(REG_RESET, mre.commands_.at(1).first);
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(2).first);
  mre.commands_.clear();
}

TEST_F(RegisterExTest, testPerformAction_opta)
{
  MockRegisterExample mre;

  // Option A set.
  mre.option_a_ = true;
  mre.performAction();
  EXPECT_EQ(2, mre.commands_.size());
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(0).first);
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(1).first);
  EXPECT_EQ((REG_CONTROL_OPT_A_Msk | REG_CONTROL_FLAG_1_Msk), mre.commands_.at(1).second);
  mre.commands_.clear();

  // Option A unset.
  mre.option_a_ = false;
  mre.performAction();
  EXPECT_EQ(2, mre.commands_.size());
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(0).first);
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(1).first);
  EXPECT_EQ(REG_CONTROL_FLAG_2_Msk, mre.commands_.at(1).second);
  mre.commands_.clear();
}

TEST_F(RegisterExTest, testPerformAction_optb)
{
  MockRegisterExample mre;

  // Option B set.
  mre.option_b_ = true;
  mre.performAction();
  EXPECT_EQ(4, mre.commands_.size());
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(0).first);
  EXPECT_EQ(REG_CONTROL, mre.commands_.at(1).first);
  EXPECT_EQ(REG_ID, mre.commands_.at(2).first);
  EXPECT_EQ(REG_ID, mre.commands_.at(3).first);
  EXPECT_EQ((ID_VERSION + (1 << REG_ID_PAT_Sft)), mre.commands_.at(3).second);
  mre.commands_.clear();
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
