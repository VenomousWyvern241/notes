# /usr/bin/python3

"""test_binding_cpp_cython.py

An example implementation of python CPP binding using cython.

Compile: `$ cd notes/build && cmake --build . --`
Run: `$ python3 test_binding_cpp_cython.py [-i <int> -s <str> -f]`
  * Specifically: `build$ pushd examples/lib/ &&
         python3 /home/haleigh/Documents/notes/examples/src/tests/test_binding_cpp_cython.py`

Helpful Links:
  * https://realpython.com/python-bindings-overview/#python-bindings-overview

@VenomousWyvern241 2022/December/22
"""

# distutils: language=c++

import argparse
import sys

# Cython will internally handle this ambiguity.
import numpy as np  # NumPy-Python functions (np.ndarray)

sys.path.append(".")

from binding_experiment_cpp_cy import pyFibonacci


class Error(Exception):
    """Base error class."""


def execute(opts):
    """Execute some library commands.

    Args:
        opts: result of argparse.parse()
    """
    fib = pyFibonacci()

    print("==== Int & String Return Tests ====")

    answer = fib.test_return_int(opts.test_int)
    print(f"Test integer is: {answer}.")

    answer = fib.test_return_string(opts.test_str).decode("utf-8")
    print(f"Test string is: {answer}.")

    print("==== Printing Test ====")

    fib.test_print()

    print("==== Complicated Function Test ====")

    answer = fib.get_nth_digit(opts.test_int)
    print(f"Digit {opts.test_int} of the Fibonacci Sequence is: {answer}")

    fib.print_n_digits(opts.test_int)

    print("==== Int Array Test 1 (Return Array) ====")

    fib_arr = fib.calculate_seq(opts.test_int)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {fib_arr[i]}")

    print("==== Int Array Test 2 (Pass Array) ====")

    # Create zeroed numpy integer array of length opts.test_int.
    arr = np.zeros(opts.test_int, dtype=np.intc)
    fib.calculate_seq_void(opts.test_int, arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {arr[i]}")

    print("==== Enum Class Test ====")

    fib.print_mode()
    print("Setting to normal...")
    fib.set_mode(fib.mode_normal())
    fib.print_mode()
    print("Specifying test mode to print.")
    fib.print_mode(fib.mode_test())


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-i",
        "--test_int",
        type=int,
        default=9,
        help="Integer input used in execution.",
    )
    parser.add_argument(
        "-s",
        "--test_str",
        type=str,
        default="testing 123",
        help="Example",
    )
    parser.add_argument(
        "-d",
        "--example_flag",
        action="store_true",
        help="Example.",
    )

    opts = parser.parse_args()

    try:
        execute(opts)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    main()
