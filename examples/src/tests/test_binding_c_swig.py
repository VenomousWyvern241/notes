#!/usr/bin/python3

"""test_binding_c_swig.py

An example implementation of python C binding using swig.

Compile: `$ cd notes/build && cmake --build . --`
  * Path to module is appended below.
Run: `$ python3 test_binding_c_swig.py [-i <int> -s <str> -f]`
  * Specifically: `build$ pushd examples/src/swig/ &&
           python3 /home/haleigh/Documents/notes/examples/src/tests/test_binding_c_swig.py`

Helpful Links:
  * https://realpython.com/python-bindings-overview/#python-bindings-overview

@VenomousWyvern241 2022/December/01
"""

import argparse
import os
import sys

# Location of the <import>.py and _<import>.so need to be on PYTHONPATH.
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../../../build/examples/src/swig/python",
    )
)
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "../../../build/examples/lib"
    )
)
import binding_experiment_c as c_lib


class Error(Exception):
    """Base error class."""


def execute(opts):
    """Execute some library commands.

    Args:
        opts: result of argparse.parse()
    """

    print("==== Basic Function Test ====")

    answer = c_lib.test_return_int(opts.test_int)
    print(f"Test integer is: {answer}.")

    c_lib.test_print()

    print("==== Complicated Function Test ====")

    answer = c_lib.get_nth_digit(opts.test_int)
    print(f"Digit {opts.test_int} of the Fibonacci Sequence is: {answer}")

    c_lib.print_n_digits(opts.test_int)

    print("==== String Return Test ====")

    answer = c_lib.test_return_string(opts.test_str)
    print(f"Test string is: {answer}.")

    print("==== Int Pointer Return Test ====")

    fib_arr = c_lib.calculate_seq(opts.test_int)
    array = c_lib.intArray.frompointer(fib_arr)
    for i in range(0, opts.test_int):
        print(f"Fibonacci[{i}]: {array[i]}")


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-i",
        "--test_int",
        type=int,
        default=9,
        help="Integer input used in execution.",
    )
    parser.add_argument(
        "-s",
        "--test_str",
        type=str,
        default="testing 123",
        help="Example",
    )
    parser.add_argument(
        "-d",
        "--example_flag",
        action="store_true",
        help="Example.",
    )

    opts = parser.parse_args()

    try:
        execute(opts)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    main()
