/**
 * @file
 *
 * @brief Part of a set of example programs to investigate python bindings for C vs CPP.
 *
 * Programs have the same use case and function names for comparisons sake.
 *
 * @VenomousWyvern241 2022/November/18
 */

#ifndef BINDING_EXPERIMENT_H
#define BINDING_EXPERIMENT_H

/**
 * @brief Basic test to return the value provided with @p n.
 *
 * @param[in] n
 *   A random integer.
 *
 * @return the provided parameter.
 */
int test_return_int(int n);

/**
 * @brief Basic test to return the value provided with @p str.
 *
 * @param[in] str
 *   A random string.
 *
 * @return the provided parameter.
 */
char* test_return_string(char* str);

/**
 * @brief Basic test to print a string.
 */
void test_print();

/**
 * @brief Calculate the fibonacci sequence up to digit @p n.
 *
 * @param[in] n
 *   The number of digits to calculate.
 *
 * @see @ref calculate_seq_void() for another implementation.
 *
 * @return Integer array containing the sequence.
 */
int* calculate_seq(int n);

/**
 * @brief Calculate the fibonacci sequence up to digit @p n.
 *
 * @param[in] n
 *   The number of digits to calculate.
 * @param[out] arr
 *   The integer array to contain the sequence.
 *
 * @see @ref calculate_seq() for another implementation.
 */
void calculate_seq_void(int n, int* arr);

/**
 * @brief Retrieve the Nth digit of the fibonacci sequence.
 *
 * @param[in] n
 *   The Nth digit.
 *
 * @return value of Nth digit of the fibonacci sequence.
 */
int get_nth_digit(int n);

/**
 * Print @p n digits of the fibonacci sequence.
 *
 * @param[in] n
 *   The number of digits to print.
 */
void print_n_digits(int n);

#endif /* BINDING_EXPERIMENT_H */
