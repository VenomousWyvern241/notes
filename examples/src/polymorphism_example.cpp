/*
 * Example program to show use of polymorphism with a pure virtual base.
 *
 * Compile:
 *   `$ g++ <file_name>.<ext> -o <file_name>`
 *   `$ cmake --build . -t <file_name>`
 * Run: `$ ./polymorphism_example`
 * Online: go to http://cpp.sh, select C++11
 *
 * @VenomousWyvern241 2023/March/22
 */

#include <iostream>

#define UNUSED(expr) do { (void)(expr); } while (0)

/* Pure virtual base interface. */
class Base
{
 public:
  /*
   * Sum of two numbers; print the result.
   *
   * @param l - addend one.
   * @param r - addend two.
   */
  virtual void add(uint8_t l, uint8_t r) = 0;

  /*
   * Difference of two numbers; print the result.
   *
   * @param l - minuend.
   * @param r - subtrahend.
   */
  virtual void subtract(uint8_t l, uint8_t r) = 0;
};

/* uint32_t implementation of base. */
class Derived_u32 : public Base
{
 public:
  Derived_u32();
  ~Derived_u32();

  /** @copydoc Base::add */
  void add(uint8_t l, uint8_t r) override;
  void add(uint32_t l, uint32_t r);
  /** @copydoc Base::subtract */
  void subtract(uint8_t l, uint8_t r) override;
  void subtract(uint32_t l, uint32_t r);
};

/* double implementation of base. */
class Derived_double : public Base
{
 public:
  Derived_double();
  ~Derived_double();

  /** @copydoc Base::add */
  void add(uint8_t l, uint8_t r) override;
  void add(double l, double r);
  /** @copydoc Base::subtract */
  void subtract(uint8_t l, uint8_t r) override;
  void subtract(double l, double r);
};

/* Virtual function implementations. */
// Need to have the uint8_t implemented (can simply return or in this case print an error) in the
// derived class(es) otherwise the derived class becomes abstract as well.
void Derived_u32::add(uint8_t l, uint8_t r)
{
  UNUSED(l);
  UNUSED(r);

  printf("Error: uint8_t add is not implemented here.\n");
}

void Derived_u32::subtract(uint8_t l, uint8_t r)
{
  UNUSED(l);
  UNUSED(r);

  printf("Error: uint8_t subtract is not implemented here.\n");
}

void Derived_double::add(uint8_t l, uint8_t r)
{
  UNUSED(l);
  UNUSED(r);

  printf("Error: uint8_t add is not implemented here.\n");
}

void Derived_double::subtract(uint8_t l, uint8_t r)
{
  UNUSED(l);
  UNUSED(r);

  printf("Error: uint8_t subtract is not implemented here.\n");
}

/* Derived_u32 implementations. */
Derived_u32::Derived_u32()
{
}

Derived_u32::~Derived_u32()
{
}

void Derived_u32::add(uint32_t l, uint32_t r)
{
  printf("%u + %u = %u\n", l, r, l + r);
}

void Derived_u32::subtract(uint32_t l, uint32_t r)
{
  printf("%u - %u = %u\n", l, r, l - r);
}

/* Derived_double implementations. */
Derived_double::Derived_double()
{
}

Derived_double::~Derived_double()
{
}

void Derived_double::add(double l, double r)
{
  printf("%.2f + %.2f = %.2f\n", l, r, l + r);
}

void Derived_double::subtract(double l, double r)
{
  printf("%.2f - %.2f = %.2f\n", l, r, l - r);
}

/**
 * Showcase polymorphism with a pure virtual base.
 */
int main()
{
  Derived_u32 var_u32;
  Derived_double var_double;

  uint8_t u8_val_a = 8;
  uint8_t u8_val_b = 4;
  uint32_t u32_val_a = 32;
  uint32_t u32_val_b = 16;
  double d_val_a = 5.5;
  double d_val_b = 2.75;

  printf(" --- --- --- --- --- --- \n");
  // Correct execution.
  var_u32.add(u32_val_a, u32_val_b);
  var_double.add(d_val_a, d_val_b);
  var_u32.subtract(u32_val_a, u32_val_b);
  var_double.subtract(d_val_a, d_val_b);

  printf(" --- --- --- --- --- --- \n");
  // Incorrect execution.
  var_u32.add(u8_val_a, u8_val_b);
  var_double.add(u8_val_a, u8_val_b);
  var_u32.subtract(u8_val_a, u8_val_b);
  var_double.subtract(u8_val_a, u8_val_b);

  // Ambiguous execution - causes compilation error.
  // var_u32.add(15,15);
  // var_double.add(15,15);

  return 0;
}
