#ifndef IMITATION_REGISTER_EXAMPLE_H_
#define IMITATION_REGISTER_EXAMPLE_H_

#include <stdint.h>

#define UNUSED(expr) \
  do                 \
  {                  \
    (void)(expr);    \
  } while (0)

/**
 * Example interface for interacting with registers - specifically to allow showcasing of testing.
 */
class RegisterExample
{
 public:
  RegisterExample();
  ~RegisterExample();

  /**
   * Setup the registers.
   */
  void setup();

  /**
   * Perform an action using multiple register access calls.
   */
  void performAction();

 private:
  /**
   * Read from a register (DUMMY).
   *
   * @param addr The (complete) register address to read from.
   * @param data Storage for the data within the specified register.
   */
  virtual void readFromRegister(uint32_t addr, uint32_t &data);

  /**
   * Write to a register (DUMMY).
   *
   * @param addr The (complete) register address to write to.
   * @param data The data to write to the specified register.
   */
  virtual void writeToRegister(uint32_t addr, uint32_t data);
};

#endif
