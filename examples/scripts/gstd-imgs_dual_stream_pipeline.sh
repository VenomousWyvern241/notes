#!/bin/bash

#
# Start two basic streams with videotestsrc (one ball, one smtpe) and appy a text overlay. 
#
# Setup: 
#  1. Build docker image: `gstd_image$ docker image build - < Dockerfile`
#  2. Get image ID: `$ docker images`
#  3. Run Docker Image: `$ docker run -it --mount type=bind,source=/home/haleigh/Documents/notes/examples,target=/mnt --env DISPLAY=unix:0 --privileged --volume /tmp/.X11-unix:/tmp/.X11-unix <ID>`
#
# Run: `$ cd /mnt/scripts && ./gstd-dual_live_stream_pipeline.sh <start|play|stop|restart>`
#
# @VenomousWyvern241 2022/June/29
#

## Variables
CLIENT="gst-client"
MFS="multifile"

# Source: https://developer.ridgerun.com/wiki/index.php/GstInterpipe_-_GstInterpipes_Elements_Detailed_Description
ISINK_ARGS=(
  "async=0"            # Do not go asynchroniusly to PAUSED.
  "forward-eos=0"      # Disable forwarding of EOS events.
  "forward-events=0"   # Disable forwarding of any idownstream events.
  "sync=0"             # Do not synchronize on the clock
)
ISRC_ARGS=(
  "block=1"            # Block push-buffer when max bytes are queued.
  "format=3"           # Set the format of the segment events/seek to "time".
)

IP_SINK="interpipesink ${ISINK_ARGS[@]}"
IP_SRC="interpipesrc ${ISRC_ARGS[@]}"

FILEPATH="/media/sample_imgs"
FILENAME="frame%05d"
FILEEXT="jpeg"
LOCATION="$FILEPATH/$FILENAME.$FILEEXT"

## Mini Pipelines
##  - These will eache effectively be elements in the finished pipeline.

mk_pn(){
  STREAM_NUM=${1:-0}
  IDX=$(( STREAM_NUM + 150 ))
  BLOCK_NAME="p${STREAM_NUM}"
  SRC_NAME="bbb${STREAM_NUM}"

  $CLIENT pipeline_create $BLOCK_NAME \
  multifilesrc name=${SRC_NAME} location=${LOCATION} start-index=${IDX} stop-index=300 loop=true caps="image/$FILEEXT,framerate=\(fraction\)30/1" ! \
  jpegdec ! videoconvert ! \
  $IP_SINK name=pn${STREAM_NUM}_out
}

mk_mfsink(){
  BLOCK_NAME=$MFS

  $CLIENT pipeline_create $BLOCK_NAME \
  $IP_SRC listen-to=comp_out ! \
  jpegenc ! \
  multifilesink name=mfs max-files=10 location="$(pwd)/gstd-videotestsrc_dual_%05d.jpeg"
}

# Compositor - combine 2+ images/videos into a single frame, acts on the combination removing the need for duplication in individual pipelines.
mk_compositor()
{
  BLOCK_NAME=compositor 

  $CLIENT pipeline_create $BLOCK_NAME \
  $IP_SRC listen-to=pn0_out ! comp.sink_0 \
  $IP_SRC listen-to=pn1_out ! comp.sink_1 \
  compositor name=comp \
  sink_0::xpos=0 sink_0::ypos=0 \
  sink_1::xpos=960 sink_1::ypos=0 ! \
  queue ! 'video/x-raw, width=1920, height=540' ! videoconvert ! \
  $IP_SINK name=comp_out
}

## Control Functions
##  - These functions will control the finished pipeline, via calls to the mini-pipelines.

# Create all pipeline components, in the proper order as some rely on eachother.
initiate(){
  mk_pn 0
  mk_pn 1
  mk_compositor
  mk_mfsink
}

# Play the pipeline components in correct order.
play(){
  $CLIENT pipeline_play compositor
  sleep 1
  $CLIENT pipeline_play p0
  $CLIENT pipeline_play p1
  $CLIENT pipeline_play $MFS
}

# Stop the pipeline nicely and remove components; cleanup and shut down.
#  - pipeline_delete will stop any running pipelines before removal 
#    (see: https://developer.ridgerun.com/wiki/index.php/GStreamer_Daemon_-_Interacting_with_Pipelines#Delete_Pipelines)
shutdown(){
  $CLIENT pipeline_delete $MFS
  $CLIENT pipeline_delete compositor
  sleep 1
  $CLIENT pipeline_delete p0
  $CLIENT pipeline_delete p1
}

## Main

case $1 in
  start)
    initiate
    ;;
  play)
    play
    ;;
  stop)
    shutdown
    ;;
  restart)
    shutdown
    initiate
    ;;
esac

