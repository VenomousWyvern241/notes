#!/bin/python3

"""day_of_the_programmer_test.py

A test suite for the day_of_the_programmer.py file.

Run: `$ pytest -v day_of_the_programmer_test.py`

@VenomousWyvern241 on 2022/April/20
"""

import sys

sys.path.append("..")
from day_of_the_programmer import day_of_programmer


def test_1800():
    """Test for the year 1800."""
    input_year = 1800
    expected_date = "12.09.1800"

    assert day_of_programmer(input_year) == expected_date


def test_1984():
    """Test for the year 1984."""
    input_year = 1984
    expected_date = "12.09.1984"

    assert day_of_programmer(input_year) == expected_date


def test_2016():
    """Test for the year 2016."""
    input_year = 2016
    expected_date = "12.09.2016"

    assert day_of_programmer(input_year) == expected_date


def test_2022():
    """Test for the year 2022."""
    input_year = 2022
    expected_date = "13.09.2022"

    assert day_of_programmer(input_year) == expected_date
