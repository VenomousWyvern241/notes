#!/bin/bash

#
# List the available Operating systems on a multiboot system.
#  
# Run: `$ ./list_os.sh`
# Automate: Can also be added to .bashrc.
#
# Note: Tested with Ubuntu 22.04, CentOs Stream 9, AlmaLinux 9.2
#
# @VenomousWyvern241 2023/December/20
#

list_os(){
  # Command to list the available operating systems on a multiboot system.
  awk -F\' '$1=="menuentry " || $1=="submenu " {print i++ " : " $2}; /\tmenuentry / {print "\t" i-1">"j++ " : " $2};' /boot/grub/grub.cfg
}

list_os

