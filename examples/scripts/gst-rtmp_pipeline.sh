#!/bin/bash

#
# Start a basic RTMP stream of videotestsrc.
#
# Run: `$ ./gst-rtmp_pipeline.sh <HOST_IP>` // HOST_IP = WWW.XXX.YYY.ZZZ
# View: 
#  With GST: `$ gst-launch-1.0 rtmpsrc location='rtmp://<HOST_IP>:1935/live/test live=1' ! decodebin ! autovideosink`
#  With Chrome: copy "rtmp://<HOST_IP>:1935/live/test" into the url bar and use the bottom option.
#
# @VenomousWyvern241 2022/April/28
#
# Requires: setup_local_rtmp_server.sh
#

HOST_IP=$1

gst-launch-1.0 videotestsrc is-live=true! \
x264enc insert-vui=1 ! \
flvmux name=muxer ! \
rtmpsink location="rtmp://$HOST_IP:1935/live/test live=1"

### Explanation

# gst-launch-1.0 videotestsrc is-live=true! \               ## Start outputting 'videotestsrc'.
# x264enc insert-vui=1 ! \                                  ## Encode the raw video as H264 data. [https://gstreamer.freedesktop.org/documentation/x264/index.html]
# flvmux name=muxer ! \                                     ## rtmpsink only accepts "video/x-flv" [https://gstreamer.freedesktop.org/documentation/rtmp/rtmpsink.html#sink]
# rtmpsink location="rtmp://$HOST_IP:1935/live/test live=1" ## Set the sink ans start pushing data - use double quotes for variable expansion.
