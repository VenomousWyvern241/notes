#!/bin/bash

#
# Start a basic stream with videotestsrc, saving the frames as jpeg images. 
#
# Run: `$ ./gst-basic_pipeline.sh`
# Test: `$ compare -metric NCC videotestsrc.jpeg videotestsrc_golden.jpeg difference.jpeg`
#   - Requires: `imagemagick`
#   - Manual result evaluation; successful if result is above 99% similar (i.e."RESULT > 0.990").
#
# @VenomousWyvern241 2022/April/27
#

gst-launch-1.0 videotestsrc ! \
'video/x-raw,width=512,height=512'! jpegenc ! \
filesink location="gst-videotestsrc.jpeg"
#ximagesink

### Explanation

# gst-launch-1.0 videotestsrc ! \                   ## Start outputting 'videotestsrc'.
# 'video/x-raw,width=512,height=512'! jpegenc ! \   ## Set the size of the video frames to be 512x512 and encode as a jpeg image.
# filesink location="gst-videotestsrc.jpeg"         ## Save frames at location - in this case overwriting the file with each new frame.
