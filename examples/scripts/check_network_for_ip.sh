#!/bin/bash

#
# Diagnostic tool for searching for a specific IP on a network.
#
# Run: `$ ./check_network_for_ip.sh <ip>`
#
# @VenomousWyvern241 2023/June/02
#

IP=$1
if [ -z "$IP" ]; then
  echo "ERROR: query ip is required."
  exit 1
fi

BASE=`echo $IP | cut -d '.' -f1-3`

# Enable command printing.
set -v 

# Display route.
ip route show

# Verbosely scan network for TCP connections.
sudo nmap -v -sS ${BASE}.0/24

# Intense scan of IP.
nmap -T4 -A -v -Pn ${IP}

# Trace route; from me to 9.9.9.9 (a DNS server run by IBM).
# Check unknowns at: https://whatismyipaddress.com/ip-lookup
traceroute 9.9.9.9

# Additionally can check UPnP at: https://www.grc.com/shieldsup
