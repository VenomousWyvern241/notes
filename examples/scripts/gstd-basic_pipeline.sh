#!/bin/bash

#
# Start a basic stream with videotestsrc, saving the frames as jpeg images. 
#
# Setup: 
#  1. Build docker image: `gstd_image$ docker image build - < Dockerfile`
#  2. Get image ID: `$ docker images`
#  3. Run Docker Image: `$ docker run -it --env DISPLAY=unix:0 --privileged  --volume /tmp/.X11-unix:/tmp/.X11-unix <ID>`
# Run: `$ ./gstd-basic_pipeline.sh`
#
# @VenomousWyvern241 2022/June/24
#
# Based on a RidgeRun demo pipeline: https://developer.ridgerun.com/wiki/index.php/GStreamer_Daemon_-_Quick_Start_Guide#Starting_GStreamer_Client
#

CLIENT="gst-client"
PN="testpipe"

# Create the pipeline; because of the small size, the whole pipeline is on this line.
$CLIENT pipeline_create $PN videotestsrc name=vts pattern=ball ! 'video/x-raw,width=512,height=512'! jpegenc ! multifilesink max-files=10 location="$(pwd)/gstd-videotestsrc_%05d.jpeg" 

# Play the created pipeline, by name.
$CLIENT pipeline_play $PN 

# Change the videotestsrc pattern.
$CLIENT element_set $PN vts pattern smpte

# Stop the pipeline.
$CLIENT pipeline_stop $PN

# Remove the pipeline.
$CLIENT pipeline_delete $PN 
