# !/bin/bash

#
# Setup an RTMP server on the current machine.
#
# Run: `$ ./setup_local_rtmp_server.sh`
# 
# Access server as "rtmp://<MACHINE_IP>:1935/<live|vod>/<file>"
#   - use "live" if the desire is to strem to the server
#   - use "vod" if the desired is to play the servers existing example video
#   gstreamer: `gst-launch-1.0 rtmpsrc location='rtmp://<HOST_IP>:1935/vod/Big_Buck_Bunny_720_10s_10MB.mp4' ! decodebin ! autovideosink`
#   browser: Copy the following into the browser "rtmp://<HOST_IP>:1935/vod/Big_Buck_Bunny_720_10s_10MB.mp4" and allow use of xdg-open.
#
# @VenomousWyvern241 2022/April/29
#
# References:
# - https://docs.peer5.com/guides/setting-up-hls-live-streaming-server-using-nginx/
# - https://www.maximumbuilders.my/news/setting-up-rtmp-server-using-nginx
#

# This script requires Debain based os.
if [ `apropos "package manager" | grep dpkg | wc -l` == 0 ]; then 
   echo "Script only works on Debain based systems."
   exit 1;   
fi

# Ensure dependencies are installed.
sudo apt-get install build-essential libpcre3 libpcre3-dev libssl-dev

# Create a dir for containment.
mkdir ~/rtmp_experiment

# Clone relevant github repos.
cd ~/rtmp_experiment/
git clone https://github.com/arut/nginx-rtmp-module.git
git clone https://github.com/nginx/nginx.git

# Configure `nginx` with the module.
cd nginx
./auto/configure --add-module=../nginx-rtmp-module

# Make and install `nginx`.
make
sudo make install

# Fetch a test_mp4.
mkdir ~/rtmp_experiment/test_mp4s
pushd ~/rtmp_experiment/test_mp4s
wget https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/720/Big_Buck_Bunny_720_10s_10MB.mp4
popd

# Copy in the nginx configuration.
sudo mv /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.orig
sudo cat<< EOF >>/usr/local/nginx/conf/nginx.conf
worker_processes  auto;
events {
    worker_connections  1024;
}

# RTMP configuration
rtmp {
    server {
        listen 1935;            # Listen to standardized port for RTMP
        chunk_size 4096;

        application live {
            live on;
            record off;
            allow publish all;
            allow play all; 
        }

	# video on demand 
        application vod {
            # Play files from a specific location - with existing media.
            play ~/rtmp_experiment/test_mp4s;

    }
}
EOF

# Start the service running in the foreground; [ctrl]+[c] to interrupt.
sudo /usr/local/nginx/sbin/nginx -g 'daemon off;'

