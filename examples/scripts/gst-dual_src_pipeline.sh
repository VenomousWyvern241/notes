#!/bin/bash

#
# Start two different videotestsrc streams and view. 
#
# Run: `$ ./gst-dual_src_pipeline.sh`
#
# @VenomousWyvern241 2022/June/07
#

gst-launch-1.0 \
videotestsrc ! video/x-raw, width=512, height=512 ! xvimagesink \
videotestsrc pattern=pinwheel ! video/x-raw, width=256, height=256 ! xvimagesink

### Explanation

# gst-launch-1.0 \
# videotestsrc ! video/x-raw, width=512, height=512 ! xvimagesink \                       ## Output basic 'videotestsrc' (0) with size set to 512x512 and display.
# videotestsrc pattern=pinwheel ! video/x-raw, width=256, height=256 ! xvimagesink        ## Output 'pinwheel' pattern 'videotestsrc' (1) with size set to 256x256 and display.

