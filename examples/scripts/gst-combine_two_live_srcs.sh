#!/bin/bash

#
# Start a two different sources, combine them into one display.
#
# Run: `$ ./gst-combine_two_srcs.sh`
#  NOTE: must be run on an Jetson (Xavier AGX) platform
#
# Output: see `tests/gst-combine_two_srcs-output.jpg` for results. 
#
# @VenomousWyvern241 2022/June/15
#

gst-launch-1.0 \
videotestsrc ! nvvideoconvert ! "video/x-raw(memory:NVMM), width=512, height=512" ! m.sink_0  \
videotestsrc pattern=pinwheel ! nvvideoconvert ! "video/x-raw(memory:NVMM), width=512, height=512" ! m.sink_1  \
nvstreammux name=m batch-size=2 width=512 height=512 ! \
nvmultistreamtiler width=1024 height=512 rows=1 columns=2 ! \
nvdsosd ! nvvideoconvert ! \
jpegenc ! multifilesink max-files=60 location="monitor/%05d.jpeg"

### Explanation

# gst-launch-1.0 \                                                                                                      ## Start the application to build/run the pipeline.
# videotestsrc ! nvvideoconvert ! "video/x-raw(memory:NVMM), width=512, height=512" ! m.sink_0  \                       ## Start the first source, going to the first nvstreammux sink pad.
# videotestsrc pattern=pinwheel ! nvvideoconvert ! "video/x-raw(memory:NVMM), width=512, height=512" ! m.sink_1  \      ## Start the second source, going to a second nvstreammux sink pad.
# nvstreammux name=m batch-size=2 width=512 height=512 ! \                                                              ## Create batched (combined) output frames. 
# nvmultistreamtiler width=1024 height=512 rows=1 columns=2 ! \                                                         ## Create tiled output from batch: default image first, pinwheel immediately to the right. 
# nvdsosd ! nvvideoconvert ! \                                                                                          ## Draw bounding boxes and convert into usable format.
# jpegenc ! multifilesink max-files=60 location="monitor/%05d.jpeg"                                                     ## Encode the frames a jpg and save at location, keeping the latest 60 frames (overwriting oldest).
