#/bin/bash

#
# Replace files that are links with original files.
#
# Run: `$ ./helper-replace_softlinks.sh <DIR_PATH>`
#
# @VenomousWyvern241 2022/November/11
#


DIR_PATH=${1:"."}

cd $DIRPATH

find . -type l \
  ! -path "./*.py" \
  ! -path "./*/*.py" \
  -print0 \
  | xargs \
  -0 \
  -n1 \
  -i \ 
  sh -c \
  'cp -L "{}" "{}".orig; rm "{}"; mv "{}".orig "{}"; ls -la "{}"'


### Explanation

# Enter the appropriate directory.

# Recursively find all softlinks
#  except python files in the current dir
#  except python giles in the current dir's subdir(s)
#  print the full file name on the standard output followed by a null character
#  pipe output to xargs
#  input items are terminated by a null character instead of by whitespace
#  input is one argumet per line
#  allow use of find results as variable
#  execute the following command set in bash
#  replace softlinks with actual files

