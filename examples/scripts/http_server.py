#!/usr/bin/env python

"""http_server.py

An example implementation of a local http server.

Run: `$ python3 http_server.py [-i <ip>|-p <port|-d <path/to/dir/to/serve>] `

Can have client via browser or via command line:
 * Browser just open a new tab and go to <address>.
 * Command Line:
    1. Open a new terminal.
    2. Start python3 command line.
    3. Import the requests module: `import requests`
    4. Call address: `r = requests.get(<address>)`
    5. Read response with `print("{} {}".format(r.status_code, r.text))`

Addresses:
 * Get list of files: `http://<ip>:<port>`
 * Get specific file: `http://<ip>:<port>?fn=<file_name>`

@Venomouswyvern241 2022/October/12
"""

import argparse
import http.server
import json
import os
import socketserver
from urllib.parse import urlparse
from urllib.parse import parse_qs


class Error(Exception):
    """Base error class for http server operations."""


class RequestHandler(http.server.BaseHTTPRequestHandler):
    """HTTP Request Handler."""

    def do_GET(self):
        """Handle GET request."""
        # Extract queries from url.
        components = parse_qs(urlparse(self.path).query)

        if not components:
            # If there are no queries; send 'Ok' response and list of available files.
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()

            dir_content = os.listdir(".")
            self.wfile.write(json.dumps(dir_content).encode("utf8"))

        elif "fn" in components:
            # If there are queries; check for file name.
            filename = components["fn"][0]
            if os.path.isfile(filename):
                # If file exists: send 'Ok' response, then file.
                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.end_headers()

                with open(filename, "rb") as fileobj:
                    self.wfile.write(fileobj.read())

        else:
            # In any other case send a 404 error because the requested file doesn't exist.
            self.send_response(404, message="Not Found")
            self.end_headers()


class Server(socketserver.TCPServer):
    """TCP Server."""

    allow_reuse_address = True


def check_directory(path_to_dir):
    """Verify that a given directory exists and contains files.
       If verified, switch to specified directory.

    Args:
        path_to_dir: the directory (including path) to verify

    Raises:
        Error if directory does not exist.
        Error if directory is empty.
    """
    if not os.path.exists(path_to_dir):
        raise Error(
            '"{path_do_dir}" does not exist; please specify directory with appropriate path.'
        )

    contents = os.listdir(path_to_dir)
    if len(contents) == 0:
        raise Error('"{path_to_dir}" does not contain any files.')

    os.chdir(path_to_dir)


def serve(args, handler):
    """Instantiate and start HTTP server.

    Args:
        args: result of argparse.parse()
        handler: RequestHandler class;
    """
    check_directory(args.dirpath)

    with Server((args.host_ip, args.host_port), handler) as httpd:
        print('Serving files in "{args.dirpath}" at port {args.host_port}.')
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass
        httpd.server_close()


def main():
    """Main."""
    parser = argparse.ArgumentParser(description="Simple http file server example.")
    parser.add_argument(
        "-i",
        "--host_ip",
        default="127.0.0.1",
        help="IP Address to use.",
    )
    parser.add_argument(
        "-p",
        "--host_port",
        type=int,
        default=65432,
        help="Port to use.",
    )
    parser.add_argument(
        "-d",
        "--dirpath",
        type=str,
        required=True,
        help="The directory (including absolute / local path) holding the files to be transfered.",
    )

    args = parser.parse_args()

    try:
        serve(args, RequestHandler)
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))


if __name__ == "__main__":
    main()
