#!/bin/bash

#
# Set NVIDIA Graphics Card Fan speed.
#
# Requirements: nvidia driver installed
#  0. Download the correct driver from https://www.nvidia.com/download/index.aspx
#  1. Allow execution on the downloaded driver (will be a run file).
#  2. Setup the system as root:
#    * RHEL Based
#      a. Install required packages: `epel epel-latest pkg-config libglvnd-devel`
#      b. Blacklist `nouveau`.
#        i. `/etc/defult/grub` add the following to the end of the "GRUB_CMDLINE_LINUX" line: `rd.driver.blacklist=nouveau`
#        ii. `/etc/modprobe.d/nouveau-blacklist.conf` add "blacklist nouveau" on one line and "options nouveau modeset=0" on the next.
#        -------
#        i. `$ echo -e "blacklist nouveau\noptions nouveau modeset=0" | sudo tee /etc/modprobe.d/blacklist-nouveau.conf` (Fedora
#      c. [If single boot machine] update grub: `$ grub[2]-mkconfig -o </path/to/>grub.cfg`
#      d. Update `initramfs`.
#        i. Make a backup of the old: `$ mv /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r)-nouveau.img`
#        ii. Create the new: `$ dracut /boot/initramfs-$(uname -r).img $(uname -r)`
#      e. Set the runlevel to 3: `$ systemctl set-default multi-user.target` 
#        * This sets it with a terminal interface; allowing removal / disuse of nouveau (the default GUI interface) module.
#        * May need to make `rc.local` executable.
#      f. [If multiboot machine] boot into the main os and update grub.
#      g. Reboot.
#      h. Run the executable: `$ ./NVIDIA-Linux-*.run`
#        i. Allow registering with dkms.
#        ii. Allow installing 32-bit compatibility.
#        iii. Allow reconfiguraiton of X configuration.
#      i. Reset the runlevel to graphical: `$ systemctl set-default graphical.target`
#      j. Reboot.        
#  
# Run: `$ ./set_gpu_fan.sh <speed>`
# Automate: Can also be added to .bashrc with DESIRED_SPEED replaced with a desired default
#  this will cause the fan to be set appropriately when a new terminal is opened.
#
# Note: Tested with Ubuntu 22.04, Debain 12, CentOs Stream 9, AlmaLinux 9.2
#
# Update NVidia Driver: 
#   (Debian) `$ sudo apt install nvidia-driver nvidia-smi nvidia-settings && sudo reboot`, update main os Grub, reboot into Debian.
#
# @VenomousWyvern241 2022/November/16
#
# References:
# - https://forums.developer.nvidia.com/t/how-to-set-fanspeed-in-linux-from-terminal/72705/24?u=haleigh
# - https://www.if-not-true-then-false.com/2021/install-nvidia-drivers-on-centos-rhel-rocky-linux/
# - https://www.tecmint.com/install-nvidia-drivers-in-linux/
#

DESIRED_SPEED=$1

fans() {
  # First get the current fan setting; if non-zero then return without resetting.
  str_val=`nvidia-settings --display :1.0 -q "[fan:0]/GPUTargetFanSpeed" | head -2 | tail -1 | rev | cut -d " " -f 1 | rev`
  int_val=${str_val%.*}

  # If current value is greater the desired value then return without resetting.
  if [[ $(("$int_val")) -ge $(("$1")) ]]; then
    return
  fi

  # Otherwise attempt to set the fan speed as specified.
  nvidia-settings --display :1.0 -a "[gpu:0]/GPUFanControlState=1" -a "[fan:0]/GPUTargetFanSpeed=$1" &> /dev/null
  if [ "$?" == "0" ]; then
    echo "Successfully set GPU fan speeds set to $1 percent"
  else
    echo "Failed to set GPU fan speed."
  fi
}

fans $DESIRED_SPEED

