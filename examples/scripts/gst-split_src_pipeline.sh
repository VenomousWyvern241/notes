#!/bin/bash

#
# Start a basic stream with videotestsrc, then split the src in two: one RGB, one BGR. 
#
# Run: `$ ./gst-split_src_pipeline.sh`
#
# @VenomousWyvern241 2022/June/07
#

gst-launch-1.0 videotestsrc ! \
video/x-raw, width=512, height=512 ! \
tee name=t t. ! queue ! \
videoconvert ! video/x-raw, format=RGBx ! capssetter join=false caps="video/x-raw, format=(string)BGRx" ! \
videoconvert ! ximagesink t. ! queue ! \
videoconvert ! ximagesink


### Explanation

# gst-launch-1.0 videotestsrc ! \                                                                                      ## Start outputting 'videotestsrc'.
# video/x-raw, width=512, height=512 ! \                                                                               ## Set the size of the video frames to be 512x512.
# tee name=t t. ! queue ! \                                                                                            ## Split the input into multiple pads. [https://gstreamer.freedesktop.org/documentation/coreelements/tee.html?gi-language=c#tee-page]
# videoconvert ! video/x-raw, format=RGBx ! capssetter join=false caps="video/x-raw, format=(string)BGRx" ! \          ## Convert the color : RGB -> BGR.
# videoconvert ! ximagesink t. ! queue !\                                                                              ## Display the converted color stream.
# videoconvert ! ximagesink                                                                                            ## Display the un-modified stream.

