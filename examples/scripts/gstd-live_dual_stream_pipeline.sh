#!/bin/bash

#
# Start two basic streams with videotestsrc (one ball, one smtpe) and appy a text overlay. 
#
# Setup: 
#  1. Build docker image: `gstd_image$ docker image build - < Dockerfile`
#  2. Get image ID: `$ docker images`
#  3. Run Docker Image: `$ docker run -it --mount type=bind,source=/home/haleigh/Documents/notes/examples,target=/mnt --env DISPLAY=unix:0 --privileged --volume /tmp/.X11-unix:/tmp/.X11-unix <ID>`
#
# Run: `$ cd /mnt/scripts && ./gstd-dual_live_stream_pipeline.sh <start|play|stop|restart>`
#
# @VenomousWyvern241 2022/June/29
#

## Variables
CLIENT="gst-client"
PN1="tp_smpte"
PN2="tp_pinwheel"
MFS="multifile"

# Source: https://developer.ridgerun.com/wiki/index.php/GstInterpipe_-_GstInterpipes_Elements_Detailed_Description
ISINK_ARGS=(
  "async=0"            # Do not go asynchroniusly to PAUSED.
  "forward-eos=0"      # Disable forwarding of EOS events.
  "forward-events=0"   # Disable forwarding of any idownstream events.
  "sync=0"             # Do not synchronize on the clock
)
ISRC_ARGS=(
  "block=1"            # Block push-buffer when max bytes are queued.
  "format=3"           # Set the format of the segment events/seek to "time".
)

IP_SINK="interpipesink ${ISINK_ARGS[@]}"
IP_SRC="interpipesrc ${ISRC_ARGS[@]}"

## Mini Pipelines
##  - These will eache effectively be elements in the finished pipeline.

# Original Script [OG] (gstd-basic_pipeline.sh) translated to gstd-client.
mk_pn1_orig(){
  BLOCK_NAME=${PN1}_orig

  $CLIENT pipeline_create $BLOCK_NAME \
  videotestsrc name=vts1o pattern=smpte ! \
  'video/x-raw,width=512,height=512'! jpegenc ! \
  multifilesink max-files=10 location="$(pwd)/gstd-videotestsrc_dual_%05d.jpeg"
}

# Split OG into pieces - pt 1 source generation and encoding.
mk_pn1(){
  BLOCK_NAME=$PN1

  $CLIENT pipeline_create $BLOCK_NAME \
  videotestsrc name=vts1 pattern=smpte ! \
  'video/x-raw,width=512,height=512' ! \
  $IP_SINK name=pn1_out
}

# Split OG into pieces - pt 2 sink use.
mk_mfsink(){
  BLOCK_NAME=$MFS

  $CLIENT pipeline_create $BLOCK_NAME \
  $IP_SRC listen-to=comp_out ! \
  jpegenc ! \
  multifilesink name=mfs max-files=10 location="$(pwd)/gstd-videotestsrc_dual_%05d.jpeg"
}

# Adding a secondary source.
mk_pn2(){
  BLOCK_NAME=$PN2

  $CLIENT pipeline_create $BLOCK_NAME \
  videotestsrc name=vts2 pattern=pinwheel ! \
  'video/x-raw,width=512,height=512' ! \
  $IP_SINK name=pn2_out
}

# Compositor - combine 2+ images/videos into a single frame, acts on the combination removing the need for duplication in individual pipelines.
mk_compositor()
{
  BLOCK_NAME=compositor 

  $CLIENT pipeline_create $BLOCK_NAME \
  $IP_SRC listen-to=pn1_out ! comp.sink_0 \
  $IP_SRC listen-to=pn2_out ! comp.sink_1 \
  compositor name=comp \
  sink_0::xpos=0 sink_0::ypos=0 \
  sink_1::xpos=512 sink_1::ypos=0 ! \
  queue ! 'video/x-raw, width=1024, height=512' ! videoconvert ! \
  $IP_SINK name=comp_out
}

## Control Functions
##  - These functions will control the finished pipeline, via calls to the mini-pipelines.

# Create all pipeline components, in the proper order as some rely on eachother.
initiate(){
  #mk_pn1_orig
  mk_pn1
  mk_pn2
  mk_compositor
  mk_mfsink
}

# Play the pipeline components in correct order.
play(){
  #$CLIENT pipeline_play ${PN1}_orig
  $CLIENT pipeline_play compositor
  sleep 1
  $CLIENT pipeline_play $PN1
  $CLIENT pipeline_play $PN2
  $CLIENT pipeline_play $MFS
}

# Stop the pipeline nicely and remove components; cleanup and shut down.
#  - pipeline_delete will stop any running pipelines before removal 
#    (see: https://developer.ridgerun.com/wiki/index.php/GStreamer_Daemon_-_Interacting_with_Pipelines#Delete_Pipelines)
shutdown(){
  $CLIENT pipeline_delete $MFS
  $CLIENT pipeline_delete compositor
  $CLIENT pipeline_delete $PN1
  $CLIENT pipeline_delete $PN2
  $CLIENT pipeline_delete ${PN1}_orig
}

## Main

case $1 in
  start)
    initiate
    ;;
  play)
    play
    ;;
  stop)
    shutdown
    ;;
  restart)
    shutdown
    initiate
    ;;
esac

