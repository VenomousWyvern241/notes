#!/usr/bin/python3

"""socket_udp_server.py

An example implementation of a UDP python socket server.

Requires:
 * Ethernet connection from one PC to the other.
   $ sudo ifconfig <interface> 192.168.3.<network_ip> netmask 255.255.255.0 broadcast 192.168.3.255
 * Allowing the port through firewall.
   Temporarily: $ sudo firewall-cmd --zone=public --add-port=<port>/udp
   Permanently: $ sudo firewall-cmd --zone=public --permanent --add-port=<port>/udp

Run: `$ python3 socket_udp_server.py [i <ip>|-p <port>]`

@VenomousWyvern241 2022/October/18
"""

import argparse
import socket
import traceback


def start_server(args):
    """Create instance of server and bind to parameters.

    Args:
        args: result of argparse.parse()

    Returns:
        Instance of server socket.
    """
    # Create server socket instance; and bind to host and port.
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind((args.server_ip, args.port))

    if args.broadcast:
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        return server_socket

    # Configure how many client the server can listen simultaneously.
    server_socket.listen(1)

    # Accept new connection.
    conn, _ = server_socket.accept()

    return conn


def communicate(args, connection):
    """Communicate over a provided connection.

    Args:
        args: result of argparse.parse()
        connection: server socket instance.
    """
    looping = False
    loop_max = 100

    if args.broadcast:
        # Broadcast a message indefinitely.
        while True:
            # Set the IP to be a broadcast address related to server_ip.
            broadcast_address = args.server_ip[: args.server_ip.rfind(".")] + ".255"

            message = "broadcasting stuff and things for everyone"
            connection.sendto(message.encode(), (broadcast_address.args.port))
    else:
        # Enter a dialogue.
        while True:
            # Receive data stream; data packet needs to be less than 1024 bytes.
            data = connection.recv(1024).decode()
            if not data:
                break

            if "hello" in data.lower() or "hi" in data.lower():
                # Automated greeting response.
                response = "Hello!"
                connection.send(response.encode())

            elif "thank" in data.lower():
                # Automated thanks response.
                response = "welcome"
                connection.send(response.encode())

            elif "loop" in data.lower():
                # Automated looping response.
                looping = True
                print("Instructed to loop {loop_max} times.")
                loop_ct = 0

                while looping:
                    loop_ct += 1
                    response = "looping {loop_ct}/{loop_max} ...... "
                    connection.send(response.encode())
                    if loop_ct >= loop_max:
                        response = "done... "
                        connection.send(response.encode())
                        looping = False

            else:
                # Have conversation.
                print("from connected user: " + str(data))
                response = input(" -> ")
                # Send response to the client.
                connection.send(response.encode())

        connection.close()


def main():
    """Main."""
    parser = argparse.ArgumentParser(description="Simple UDP socket server example.")
    parser.add_argument(
        "-s",
        "--server_ip",
        default="192.168.3.42",
        help="IP Address of server.",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=5000,
        help="Port to use.",
    )
    parser.add_argument(
        "-b",
        "--broadcast",
        action="store_true",
        help="Only broadcast data.",
    )
    args = parser.parse_args()

    try:
        conn = start_server(args)
        communicate(args, conn)
    except KeyboardInterrupt:
        print("\nCaught a keyboard interrupt, closing UDP connection now.")
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))
        print(traceback.format_exc())
    finally:
        if conn:
            conn.close()


if __name__ == "__main__":
    main()
