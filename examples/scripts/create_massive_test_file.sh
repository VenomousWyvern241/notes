#!/bin/bash

#
# Create a massive repetitive file for testing from a smaller 'base' file.
#
# Run: `$ ./create_massive_test_file.sh <pattern> <base_file_name> <min_size_bytes> <binary>`
#
# Check File Contains Pattern: `$ head -c <character_count> <file> [| xxd]`
#  * <character_count> is the number of characters from head to display
#  * <base_file> is the file to examine
#  * [| xxd] is optional - specifically for if the file is a binary blob
#
# @VenomousWyvern241 2023/May/02
#
# References:
# - https://skorks.com/2010/03/how-to-quickly-generate-a-large-file-on-the-command-line-with-linux/#you-want-readable-contents-but-don-t-care-if-it-is-duplicated
# - https://stackoverflow.com/a/8521308
#

PATTERN="${1:-123456789}"
FILE_NAME="${2:-file.txt}"
MIN_SIZE_B="${3:-1000000}"     # 100M
BINARY_BLOB=$4
TEMP_FILE="/tmp/temp_file.txt"

# If the file name does not already exist create the base file.
if [ ! -f $FILE_NAME ]; then
  if [ -z "$BINARY_BLOB" ]; then
    # Create Textual Base: `$ echo "<pattern>" > <base_file>`
    #    - <pattern> is the pattern to place in an empty file
    #    - <base_file> is the path and name of the base file to create
    `echo $PATTERN > $FILE_NAME`
  else
    #  Create Binary Base: `$ echo "<padded_location>: <pattern>" | xxd -r - <base_file>`
    #    - <padded_location> is the address within the new file to place <pattern>
    #    - <pattern> is the pattern to place in an empty file
    #    - <base_file> is the path and name of the base file to create
    `echo "000010: $PATTERN" | xxd -r - $FILE_NAME`
  fi
fi

# While the base size is less than the minimum size, double the file.
BASE_SIZE=`wc -c < $FILE_NAME`
while [ $BASE_SIZE -le $MIN_SIZE_B ]; do
  cat $FILE_NAME $FILE_NAME > $TEMP_FILE
  mv $TEMP_FILE $FILE_NAME
  BASE_SIZE=`wc -c < $FILE_NAME`
done

ls -la $FILE_NAME
