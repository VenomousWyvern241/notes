#!/bin/python3

"""day_of_the_programmer_test.py

A test suite for the day_of_the_programmer.py file.

Completed by VenomousWyvern241 on 2021/May/18 (updated 2022/April/20)

Courtesy: Hacker Rank - Day Of The Programmer
"""

import argparse


def day_of_programmer(year):
    """Calculate the day of the programmer for a given year."""
    days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    julian_leap = lambda n: True if n < 1918 and n % 4 == 0 else False
    gregorian_leap = (
        lambda n: True
        if n > 1918 and (n % 400 == 0 or (n % 4 == 0 and n % 100 != 0))
        else False
    )

    # Adjust the number of days in February.
    if year == 1918:
        days_in_month[1] = 15
    elif julian_leap(year) or gregorian_leap(year):
        days_in_month[1] = 29

    # Calculate the first 8 months worth of days.
    days_sum = 0
    for i in range(0, 8):
        days_sum += days_in_month[i]

    # Calculate the days required from September.
    day = 256 - days_sum

    return f"{day}.09.{year}"


def main():
    """Main."""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-y",
        "--year",
        required=True,
        type=int,
        help="Four digit year find the date in.",
    )

    opts = parser.parse_args()

    print(day_of_programmer(opts.year))


if __name__ == "__main__":
    main()
