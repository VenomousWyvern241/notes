#!/bin/bash

#
# Start a test stream with videotestsrc, and display the fps on the viewable output.
#
# Run: `$ ./gst-fps_display_pipeline.sh`
#
# @VenomousWyvern241 2022/June/08
#

gst-launch-1.0 videotestsrc pattern=ball ! \
'video/x-raw,width=600,height=400'! \
fpsdisplaysink text-overlay=true video-sink=ximagesink

### Explanation

# gst-launch-1.0 videotestsrc pattern=ball! \                  ## Start outputting 'videotestsrc' - specifcally the bouncing ball pattern.
# 'video/x-raw,width=600,height=400'! \                        ## Set the size of the video frames to be 600x400.
# fpsdisplaysink text-overlay=true video-sink=ximagesink       ## Add the fps as an overlay and display the result with ximagesink display.
                                                               ##  see: https://gstreamer.freedesktop.org/documentation/debugutilsbad/fpsdisplaysink.html
