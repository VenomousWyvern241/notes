#!/bin/bash

#
# Start a basic RTSP stream of videotestsrc with "test-launch", viewable on another PC on the same LAN. 
#
# Run: `$ ./gst-rtsp_pipeline.sh`
# View: 
#  (where <HOST_IP> is 127.0.0.1 for local use, or the IP of the Host machine (on same LAN) for remote use)
#  With GST: `$ gst-launch-1.0 -e rtspsrc location=rtsp://<HOST_IP>:8554/test ! queue ! decodebin ! nvvidconv ! xvimagesink`
#  With VLC: `$ vlc rtsp://<HOST_IP>:8554/test
#
# @VenomousWyvern241 2022/February/11
#
# Requires: `notes/examples/third_party/test-launch.c` 
#

./test-launch "videotestsrc is-live=true ! \
x264enc insert-vui=1 ! \
h264parse ! \
rtph264pay name=pay0 pt=96"

### Explanation

# ./test-launch "videotestsrc is-live=true ! \      ## Start outputting 'videotestsrc' via the pre-existing "test-launch" application.
# x264enc insert-vui=1 ! \                          ## Encode the raw video as H264 data. [https://gstreamer.freedesktop.org/documentation/x264/index.html]
# h264parse ! \                                     ## Parses the H264 data in for payload prepping.
# rtph264pay name=pay0 pt=96"                       ## Payload-encode H264 video into RTP packets [https://gstreamer.freedesktop.org/documentation/rtp/rtph264pay.html]

