#!/usr/bin/python3

"""socket_tcp_client.py

An example implementation of a TCP python socket client.

Requires:
 * Ethernet connection from one PC to the other.
   $ sudo ifconfig <interface> 192.168.3.<network_ip> netmask 255.255.255.0 broadcast 192.168.3.255
 * Allowing the port through firewall.
   Temporarily: $ sudo firewall-cmd --zone=public --add-port=<port>/tcp
   Permanently: $ sudo firewall-cmd --zone=public --permanent --add-port=<port>/tcp

Run: `$ python3 socket_tcp_client.py [i <ip>|-p <port>]`

Modified @VenomousWyvern241 2022/October/14
Courtesy: https://www.digitalocean.com/community/tutorials/python-socket-programming-server-client
"""

import argparse
import socket


def connect_client(args):
    """Create client socket instance and bind to server host and port.

    Args:
        args: result of argparse.parse()

    Returns:
        Instance of client socket connection.
    """
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((args.server_ip, args.port))

    return client_socket


def communicate(connection):
    """Communicate over a provided connection.

    Args:
        connection: client socket instance, connected ot server.
    """

    # Collect input to send to server.
    message = input(" -> ")

    while message.lower().strip() != "bye":
        # Send input.
        connection.send(message.encode())

        # Display conversation, if looping show all in one.
        data = connection.recv(1024).decode()
        print("Received from server: " + data)
        while "looping" in data.lower():
            print(data)
            data = connection.recv(1024).decode()
            if "done" in data:
                break

        # Collect new input to send to server.
        message = input(" -> ")

    connection.close()


def main():
    """Main."""
    parser = argparse.ArgumentParser(description="Simple TCP socket client example.")
    parser.add_argument(
        "-s",
        "--server_ip",
        default="192.168.3.42",
        help="IP Address of server.",
    )
    parser.add_argument(
        "-c",
        "--client_ip",
        default="192.168.3.41",
        help="IP Address of client. (FTR; must be the same subnet (first 3 ip digits) as server.)",
    )

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=5000,
        help="Port to use.",
    )

    args = parser.parse_args()

    try:
        conn = connect_client(args)
        communicate(conn)
    except KeyboardInterrupt:
        print("\nCaught a keyboard interrupt, closing TCP connection now.")
        conn.close()
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))
    finally:
        if conn:
            conn.close()


if __name__ == "__main__":
    main()
