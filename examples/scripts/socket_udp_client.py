#!/usr/bin/python3

"""socket_udp_client.py

An example implementation of a UDP python socket client.

Requires:
 * Ethernet connection from one PC to the other.
   $ sudo ifconfig <interface> 192.168.3.<network_ip> netmask 255.255.255.0 broadcast 192.168.3.255
 * Allowing the port through firewall.
   Temporarily: $ sudo firewall-cmd --zone=public --add-port=<port>/udp
   Permanently: $ sudo firewall-cmd --zone=public --permanent --add-port=<port>/udp
 * Verify broadcast: $ sudo tcpdump -i <interface_name> port <port>-XX

Run: `$ python3 socket_udp_client.py [i <ip>|-p <port>]`

@VenomousWyvern241 2022/October/18
"""

import argparse
import socket
import traceback


def connect_client(args):
    """Create client socket instance and bind to server host and port.

    Args:
        args: result of argparse.parse()

    Returns:
        Instance of client socket connection.
    """
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client_socket.bind((args.client_ip, args.port))

    return client_socket


def communicate(args, connection):
    """Communicate over a provided connection.

    Args:
        args: result of argparse.parse()
        connection: client socket instance, connected or listening to server.
    """
    if args.listen:
        # Listen indefinitely.
        raw_data, _ = connection.recvfrom(1024)
        while raw_data:
            data = raw_data.decode()
            print("Received from server: " + data)
            raw_data, _ = connection.recvfrom(1024)
    else:
        # Send information.
        server_details = (args.server_ip, args.port)

        # Collect input to send to server.
        message = input(" -> ")

        while message.lower().strip() != "bye":
            # Send input.
            connection.sendto(message.encode(), server_details)

            # Display conversation, if looping show all in one.
            raw_data, _ = connection.recvfrom(1024)
            data = raw_data.decode()
            print("Received from server: " + data)
            while "looping" in data.lower():
                print(data)
                raw_data, _ = connection.recvfrom(1024)
                data = raw_data.decode()
                if "done" in data:
                    break

            # Collect new input to send to server.
            message = input(" -> ")

    connection.close()


def main():
    """Main."""
    parser = argparse.ArgumentParser(description="Simple UDP socket client example.")
    parser.add_argument(
        "-s",
        "--server_ip",
        default="192.168.3.42",
        help="IP Address of server.",
    )
    parser.add_argument(
        "-c",
        "--client_ip",
        default="192.168.3.41",
        help="IP Address of client. (FTR; must be the same subnet (first 3 ip digits) as server.)",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=5000,
        help="Port to use.",
    )
    parser.add_argument(
        "-l",
        "--listen",
        action="store_true",
        help="Only listen broadcasted data.",
    )
    args = parser.parse_args()

    if args.listen:
        # Update the Client IP to be a broadcast address related to server_ip.
        args.client_ip = args.server_ip[: args.server_ip.rfind(".")] + ".255"

    try:
        conn = connect_client(args)
        communicate(args, conn)
    except KeyboardInterrupt:
        print("\nCaught a keyboard interrupt, closing UDP connection now.")
        conn.close()
    except Exception as anomaly:
        print("Encountered an error: " + str(anomaly))
        print(traceback.format_exc())
    finally:
        if conn:
            conn.close()


if __name__ == "__main__":
    main()
