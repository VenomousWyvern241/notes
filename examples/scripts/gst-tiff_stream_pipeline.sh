#!/bin/bash

#
# Start stream using a set of images. 
#
# Setup: `cd ~/Pictures/sample_imgs && ffmpeg -i ~/Videos/Big_Buck_Bunny_720_10s_5MB.mp4 frame%05d.<ext>`
#
# Run: `$ ./gst-img_stream_pipeline.sh`
#
# @VenomousWyvern241 2022/July/05
#

FILEPATH="$HOME/Pictures/sample_imgs"
FILENAME="frame%05d"
FILEEXT="tiff"
LOCATION="$FILEPATH/$FILENAME.$FILEEXT"

gst-launch-1.0 \
multifilesrc location=${LOCATION} start-index=1 stop-index=300 loop=true caps="image/$FILEEXT,framerate=\(fraction\)30/1" ! \
avdec_tiff ! \
videoconvert ! \
ximagesink

### Explanation

# gst-launch-1.0 \                                                                                                               # Start the application.
# multifilesrc location=${LOCAITON} start-index=1 stop-index=300 loop=true caps="image/$FILEEXT,framerate=\(fraction\)30/1" ! \  # Setup the multifile source; 
                                                                                                                                 #  specify location of files
                                                                                                                                 #  start and stop indexes being the first an last id number of image
                                                                                                                                 #  continuously loop through specified images
                                                                                                                                 #  specify the image type and the output framerate
# avdec_tiff ! \                                                                                                                 # Decode the images; jpg/jpeg -> jpegdec, tiff -> avdec_tiff, etc
# videoconvert ! \                                                                                                               # Convert the stream to video output for display.
# ximagesink                                                                                                                     # Display the video stream.
