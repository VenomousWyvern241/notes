= <PRODUCT> Design Document
:xrefstyle: short
:listing-caption: Listing
:toc:
:toclevels: 4
:icons: font
:imagesdir: images

NOTE: _italicized_ entries are descriptions of what is to go in the relevant field.

NOTE: <inequality surrounded> entries are replacements.

== Revision History

|===
| Date | Version | Description | Author

| 2022/February/03
| 1.0
| Initial Version Of Document
| VenomousWyvern241

| 2023/February/22
| 1.1
| Minor: adding better/more obvious specification of language/compiler and explanation of intended implementation.
| VenomousWyvern241

| 2023/July/10
| 2
| Major: adding test planning / detail documentation and technical specification documentation.
| VenomousWyvern241

|===

== Overview

_A brief objective overview as to why the <PRODUCT> is being done; typically contains (a) a description of the planned creation or of the current solution, (b) technical reasoning as to why the creation or upgrade is being done, and (c) the end goal._

== Objectives

_Listing or briefly describing the goals of the <PRODUCT>; some samples listed below._

**Automation**: keep automation in mind - the more processes we can automate (in a simple manner) the more streamlined this repository will be.

* build the necessary executables and put them into packages
* publish the necessary packages internally
** if possible to outside/public mirror
* SEMI automate package versioning
** introduce link:https://semver.org/[semantic versioning] (Major.Minor.Patch-rc#)
** release version (Major.Minor.Patch) will remain human decided for consistency
** development version (-rc#) will be auto incremented
* testing: unit, build, installation

**Code**: bring the coding standard up to date and modernize the framework

* clean - remove unnecessary / obstructive / unused code
* compartmentalize - simplify the code hierarchy to allow finer granularity based on component, but allow generalization from device point of view
* minimize - avoid duplication in functionality and linkage
* add testing - code quality, unit, changelog addition

**Platforms**: updating support commitment to rule out EOL operating systems as they add unnecessary complication

* Linux: RHEL and Debain variants
* Windows: 10+

**Shipping / Support**: only shipping pre-compiled binaries, this is not an example application it is a demo / validation / "normal" application so we will never need to ship source files.

== Design / Plan

[IMPORTANT]
========
_If this <PRODUCT> contains cross department work (software and hardware), note dependencies here._
========

=== MileStones

|===
| Number | Name | Description | End Goal | EST. TimeFrame

| 1 // this is a link to the milestone in gitlab
| Discovery
| Researching methodology and best solution.
| Completed Design Document
|

| 2 // this is a link to the milestone in gitlab
| MVP (Minimum Viable Product)
| Creation of basic <PRODUCT>, testing framework, autobuild framework, etc.
| Functional minimalist application with basic testing and automated build process.
|

| 3 // this is a link to the milestone in gitlab
| Specification
| Adding complexity to the <PRODUCT> (features, important data display).
| Application with high functionality and accompanying testing.
|

| ...
| ...
| ...
| ...
| ...

|===

== Product Lifetime

_Detail in roughly two to three paragraphs the expected lifetime of the <PRODUCT>, if there is (or is not) a 'next generation' already under development/consideration, any influences that will further or hinder development and continued support (parts availability, basic system requirements, etc)._

[cols="1,2,2,3,3"]
|===
| Release | Time Frame | OS Support | Marketing | Other Notes

| Initial
| <~X months>
a|
* <Minimum Requirements>
| <Summary>
|

|===

// Asciidoctor inclusions are zero indexed, they must be included on level 0 (i.e not under a sub heading).
//  To include under a specific header, that header must be a level 0 header of the included document; include both the line containing the header and the lines originally desired, separated by a semi-colon (";").
include::Template-Tech_Specs.asciidoc[leveloffset=1,lines=1;24..72]

== Development

=== Basic Details

Application Language: _______________

Library Language: _______________

Test Language: _______________

Compiler: _______________

Applications/Libraries Required For Testing : _______________

Repository: link:<URL_of_repo>[<repo_name>]

=== Classes

_Bulleted description of classes to be created for <PRODUCT>; with attention given to organize hierarchically._

* _class_
** (xN) _subclass component_
** _optional required services_
* _subclass_
** (xN) _component_
** _optional required services_

=== Implementation

[IMPORTANT]
.existing_code_disclaimer
========
The statements made below are not intended to degrade the existing work or codebase in any way; implementation previously completed met or exceeded the requirements of the time - requirements coming from the companies standpoint, customers standpoint, and coding standards. Implementation methods have evolved due to both internal and external factors, which necessitates the re-evaluation of methodology for this <Program/Project Name>.
========

[IMPORTANT]
========
_If applicable include important note about any non-software based reasoning for implementation decisions (ex: hardware/firmware change)._
========

_Paragraph containing implementation details for <PRODUCT>, verbose enough for someone not currently on the project to understand the technical reasoning and intent._

.example_sub-class
----
...
int sub-class::sc_func(...){...}
...
----

.example_top_class
----
...

void c_func(...)
{
  ...
  // Declaration and use of sub-class.
  SC sub_class(...);
  sub_class.sc_function(...);
  ...
}

...
----

=== Relationship Diagram

[mermaid]
.sample of class relationship diagram from https://mermaid.live 
----
classDiagram
    TopClass <|-- SubClass0
    TopClass <|-- SubClass1
    TopClass <|-- SubClass2
    TopClass : +int var_i
    TopClass : +String var_s
    TopClass : +top_f1()
    TopClass : +top_f2()

    class SubClass0{
      +String var
      +sc0_f1()
      +sc0_f2()
    }

    class SubClass1{
      -int var
      -sc1_f1()
    }

    class SubClass2{
      +bool var
      +sc2_f1()
    }
----

=== User Interface [Optional]

_Description of proposed UI, with mocked images - note that this is only included if UI creation is included._

=== Use Cases

==== User Completed Task / User Interaction

Goal:

Success Measurement:

Typical Event Flow:

. <step 1>
. <step 2>

Assumptions:

// Asciidoctor inclusions are zero indexed, they must be included on level 0 (i.e not under a sub heading).
//  To include under a specific header, that header must be a level 0 header of the included document; include both the line containing the header and the lines origianlly desired, seperated by a semi-colon (";").
include::Template-Test_Plan.asciidoc[leveloffset=1,lines=1;26..408]

== Current Solution

[IMPORTANT]
=========
The word "current" (and its derivatives) as used in the following paragraphs / points is to be defined as **<repo_name> v#**.

.Detailed Reasoning
[%collapsible]
========

_Detailed reasoning if applicable (i.e. when more than one version is needed either for cross repo reference or other)._

========

=========

_Briefly note (as applicable) (a) one sentence overview of current solution mechanics, (b) any supplemental packages / dependencies and (c) distribution locations. If there are multiple distributions note via a table for clarity, example below._

|===
| Operating System | Architecture | Dependency Application Version(s) | Package/Archive Contents

| Linux | x86_64 | <app> v#, <app2> v# | ex: source files for local compilation

| Windows | 64 Bit | <app> v#, <app2> v# | pre-compiled binaries and *.dll files

| Windows | 32 Bit | <app> v#, <app2> v# | pre-compiled binaries and *.dll files
|===

Directions for installation and use were provided within the README.

_List format of relevant directions, making note of subtle OS specific differences - include images of UI components if applicable._

In essence however the procedure is as follows:_

== Glossary

[glossary]
Semantic Version:: a standardized method of versioning a package: `<Major>.<Minor>.<Patch>-rc<Release Candidate Num>` <<ref-SemVer>>

[bibliography]
== References

[NOTE]
=========
Using IEEE citations generated here: https://www.citationmachine.net/ieee

For online forum, blog, newsgroup (https://libguides.library.cityu.edu.hk/citing/ieee)

[#] <A. A. Author> (<year>, <month> <day>). <Title of Post> [Description of form]. Available: <URL> [Accessed: <Month> <Day>, <Year>]

For Github/Gitlab (https://www.wikihow.com/Cite-a-GitHub-Repository)

[#] <Author A.> (<created month day, year>). <repo_name>: <path/to/source_file.extension> (Version <version>) [Source Code]. <URL>
=========

- [[[ref-SemVer, 1]]] T. Preston-Werner, P. Hack, isaacs, J. Handley, T. S. Schreijer, J. Liss, O. Bilgic , O. Masanori , and S. Douglas , “Semantic versioning 2.0.0,” Semantic Versioning, 17-Jun-2013. [Online].
  Available: https://semver.org/. [Accessed: 03-Feb-2022].
             Un-Rendered: https://github.com/semver/semver/tree/v2.0.0
