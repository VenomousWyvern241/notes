= <PRODUCT> Master Test Plan
:xrefstyle: short
:listing-caption: Listing
:toc:
:toclevels: 4
:icons: font
:imagesdir: images
:sectnums:

NOTE: _italicized_ entries are descriptions of what is to go in the relevant field.

NOTE: <inequality surrounded> entries are replacements.

== Revision History

|===
| Date | Version | Description | Author

| 2023/July/10
| 1.0
| Initial Version Of Document
| VenomousWyvern241

|===

== Introduction

_Identify the type of testing plan this is and summarize it (i.e set the objective, scope, goals, expectations); sample below._

The intent of this master test plan is to clarify and record all facets necessary for <PRODUCT> testing.

There are four levels of testing required:

* Hardware Validation; testing the physical product - bring up, check for manufacturing defects, verification that individual components have the ability to communicate, etc.
** Testing is completely manual; pass/fail result is determined purely manually (human interaction and analysis is required).
* Software Unit; tests to validate software functionality - appropriate register access, accurate calculations, etc.
** Testing is completely automated; pass/fail result is determined purely programatically (no human interaction or analysis of test results is needed).
* Software Integration; library and application testing.
** Testing is completely automated; pass/fail result is determined purely programatically (no human interaction or analysis of test results is needed).
* Product Conformance; assurance that the product requirements are met or exceeded - generate a report per product for archival.
** Testing is completed at a dedicated test station via manually initiated suite; pass/fail result is determined programatically, however some human interaction is required during the testing process.

Each of these levels has specific testing methodology and pass/fail criteria, all of which is detailed below.

== Test Items

_List the artifacts (physical and/or virtual) intended to tested within the scope of this plan._

* Hardware
** _Brief explanation of hardware device; just what it is._
* Software
** Software API; precompiled CPP library and example programs for use.

== Features

_From a users perspective of the system._

* Tested
** _List all of the features and functionalities that *will* be included in the testing; must also provide references to the requirements specifications documents (Technical Specifications), which describe the features to be tested in-depth._
** Device is mechanically sound.
** CPP API base is sound in calculation and procedure.
* UnTested
** _List all of the features and functions that *will not* be included in the testing; must include justifications for why certain features will not be tested._
** Device dimensions and weight; unnecessary to check on every single board.

== Approach

_Definition of the approach for testing, contains: details of how testing will be performed, information of the sources of test data, inputs and outputs, testing techniques and priorities. This will define the guidelines for requirements analysis, develop scenarios, derive acceptance criteria, construct and execute test cases._

Due to this being a master plan, the testing procedure for the given tests have been listed below per testing level.

The following will detail the test procedure with minor references to:

* Requirements - see <<section-env>> for details.
* Results - see <<section-result_criteria>> for determination.
** <<section-suspension_resumption>> will assist in a decision to halt and resume testing.

=== Hardware Validation

.Requirements
* _List the required testing materials, example below._
* Test fixture kit.
** Take a fresh board, align it to the bottom set of pogo pins (aka: bed-of-nails), clamp down the top bed-of-nails, and now all test points and power are hooked up to the DUT.
** EX: https://www.youtube.com/watch?v=XfxvxwSIjrU&ab_channel=AdafruitIndustries, https://www.cortektestsolutions.com/pcb-test-fixture-kits-2/

.Assessments
[upperalpha]
. _What will be assessed in this test level._

.Procedure
. _Describe in detail the testing procedure._

.Results
* *Metrics*
** Contents: <brief description of test output>
** Storage Location: <where results are stored>
** Storage Format: <how results are stored>
* *Failure Case*
** Transition: blocked, <what the next step is>
* *Success Case*
** Transition: <what the next step is>

=== Software Unit

.Requirements
* _List the required testing materials._

.Assessments
[upperalpha]
. _What will be assessed in this test level._

.Procedure
. _Describe in detail the testing procedure._

.Results
* *Metrics*   
** Contents: <brief description of test output>
** Storage Location: <where results are stored>
** Storage Format: <how results are stored>
* *Failure Case*
** Transition: blocked, <what the next step is>
* *Success Case*
** Transition: <what the next step is>

=== Software Integration

.Requirements
* _List the required testing materials._

.Assessments
[upperalpha]
. _What will be assessed in this test level._

.Procedure
. _Describe in detail the testing procedure._

.Results
* *Metrics*   
** Contents: <brief description of test output>
** Storage Location: <where results are stored>
** Storage Format: <how results are stored>
* *Failure Case*
** Transition: blocked, <what the next step is>
* *Success Case*
** Transition: <what the next step is>

=== Product Conformance

NOTE: the following is an example.

.Requirements
* Capable host system, see <<section-env>> for details.
* Device Under Test (DUT).

.Assessments
[upperalpha]
. DUT is properly recognized by the host system.
. DUT has the latest applicable firmware loaded.
. The CPP API is functional.

.Procedure
. Setup host system.
. Install DUT.
.. Power off the host system.
.. Insert the DUT (must pass the "Hardware Validation" stage) into an available PCIe slot.
.. Power on the host system.
. Execute the test script.
+
[NOTE]
=========
Collection of all necessary tests.
=========

.Results
* *Metrics*
+
[NOTE]
========
Reports will be only the latest 'run', placed in the specified location regardless of pass or failure.

Reports must be both machine parseable and human readable - suggest yaml.
========
** Contents (at a minimum)
*** Date Tested
*** Device Serial Number
*** All Versioning Information (Driver, Firmware, Software, etc)
*** Singular "pass" or "fail" Grade
*** Verbose Test Results
** Storage Location: <location>
** Storage Format: text file of name "<serial_number>_<YYYYMMDD>_[pass|fail].txt"
* *Failure Case*
** Transition: blocked, requires investigation/verification for path; if failure is determined to be.
*** hardware related (short, malfunction, breakage, etc), return to "Hardware Validation".
*** software related (inaccurate register set, inaccurate speed, etc), return to "Software Integration" and perform tests manually to diagnose.
* *Success Case*
** Transition: testing complete

== Criteria (Pass/Fail) [[section-result_criteria]]

_Describe detailed success criteria for evaluating each of the test results._

Due to this being a master plan, the criteria for determining success or failure for the given tests have been listed below per testing level.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in link:https://datatracker.ietf.org/doc/html/rfc2119[RFC 2119].

=== Hardware Validation

|===
| Test Detail | Success | Failure

| ...
| ...
| ...

|===

=== Software Unit

|===
| Test Detail | Success | Failure

| ...
| ...
| ...

|===

=== Software Integration

|===
| Test Detail | Success | Failure

| ...
| ...
| ...

|===

=== Product Conformance

|===
| Test Detail | Success | Failure

| ...
| ...
| ...

|===

== Suspension Criteria and Resumption Requirements [[section-suspension_resumption]]

_Detail any criteria that may lead to the suspension of testing activities, as well as the conditions to resume testing._

[cols="2,2,4"]
|===
| Suspension Reason | Effected Test Levels | Resumption Requirement

| <Problem>; <resulting blockage>.
a|
* Software Integration
* Product conformance
a | 
* <Requirement 1 for solution.>
* ...
* <Requirement N for solution.>

| ...
| ...
| ...
| ...

|===

== Test Deliverables

[cols="4,5,4,6"]
|===
| Test Level | Deliverables | Basis / Frequency of Test | Location Of Archive

| Hardware Validation
| Functional device.
| Per product basis - must occur for every single product.
| N/A

| Software Unit
| Successful execution of the test suite.
| Per software package release.
| _(Typically)_ Recorded within an automated test.

| Software Integration
| Successful execution of any application or GUI.
| Per software package release.
| Attach verification images to the release 'bump' ticket.

| Product Conformance
| Single detailed report file.
| Per product basis - must occur for every single product.
| `<location>` 

|===

== Task Dependencies

Tests should be completed in the following order:

. Hardware Validation
. Software Unit / Software Integration
+
[NOTE]
========
Software Unit: tasks do not require hardware and are completed automatically any time a change is made to the repository.

Software Integration: tasks do require hardware and are currently manually run - however in the future tasks *will be* completed automatically any time a change is made to the devices software source(s).

However it should be mentioned that software releases must not be created unless all of the unit and integration tests are passing.
========
. Product Conformance

Software Unit tasks do not require hardware and are completed automatically any time a change is made to the repository; thus unit testing should not effect the product flow. However it should be mentioned that software releases must not be created unless all of the unit tests are passing.

== Environmental Needs [[section-env]]

_Describe the requirements for test environment; hardware, software, test equipment, or any other environmental requirement for testing._

.Relevant Repositories
[[ref-repos]]
|===
| Department | GitLab Project | New Issue

| ...
| ...
| ...

|===

Due to this being a master plan, the environmental needs have been listed below per testing level.

However some of the values will remain constant across testing levels:

.Constants
|===
| <Descriptor> | Best Option | Minimum Option

| Python3
| latest
| 3.8

| ...
| ...
| ...

|===

=== Hardware Validation

* Hardware
** <External testing equipment.>
** One of the device under test.

=== Software Unit

_Typically the environment necessary for unit testing should exist as it ought to be executed as part of the merge script; if this is the case make not of it here (linking any relevant repositories). If on the other hand the unit tests are executed in some other fashion - or if they optionally have the ability to be executed outside the automated testing, describe that as well - linking or directing to any relevant setup information._

=== Software Integration

* Software
** <Device driver to allow product recognition by the system.>
** <Software application package to allow product use.>
** <Requirements of application use.>
* Hardware
** Capable host system with:
*** Software packages installed: <list of above packages>
*** <Other host requirements.>
** One of the device under test.
** <External testing/probing equipment.>

=== Product Conformance

* Software
** <Device driver to allow product recognition by the system.>
** <Software application package to allow product use.>
** <Requirements of application use.>
* Hardware
** Capable host system with: 
*** Software packages installed: <list of above packages>
*** <Other host requirements.>
** One of the device under test.
** <External testing/probing equipment.>

== Responsibilities

Issues; if at any point an issue is found, it is the responsibility of finder to gather all relevant information and report it to a member of the relevant team so that a ticket in the appropriate repository (see <<ref-repos>>) can be created to remedy the issue.

* Minimum information for issue creation/recording:
** [Host] System Detail
*** Operating system (Linux / Windows) and kernel / build version.
** Serial number and part number of device under test.
** Any relevant versioning information (device driver, software package, etc).
** As applicable, a detailed description / screenshot / picture of the error or physical issue.

*Hardware Validation*; primarily the Manufacturing Team, with questions directed to the Hardware Team.

*Software Unit*; primarily completed within the existing automated test suite within the software repository (managed by the Software Team).

*Software Integration*; primarily the Software Team, initially manual execution - will be automated for future use.

*Product Conformance*; both the Manufacturing Team and the Software Team need to be able to perform these tests, initially manual execution - will be automated for future use.

[bibliography]
== References

[NOTE]
=========
Using IEEE citations generated here: https://www.citationmachine.net/ieee

For online forum, blog, newsgroup (https://libguides.library.cityu.edu.hk/citing/ieee)

[#] <A. A. Author> (<year>, <month> <day>). <Title of Post> [Description of form]. Available: <URL> [Accessed: <Month> <Day>, <Year>]

For Github/Gitlab (https://www.wikihow.com/Cite-a-GitHub-Repository)

[#] <Author A.> (<created month day, year>). <repo_name>: <path/to/source_file.extension> (Version <version>) [Source Code]. <URL>
=========

- [[[ref-UoI_template, 1]]] Clint Jeffery, Computer Science Department Web Resources, http://www2.cs.uidaho.edu/~jeffery/courses/383/TP_Template.pdf (accessed Jul. 10, 2023). 

- [[[ref-reqtest, 2]]] “How to write a test plan with the IEEE 829 standard,” How to Write a Test Plan with the IEEE 829 Standard, https://reqtest.com/en/knowledgebase/how-to-write-a-test-plan-2/ (accessed Jul. 10, 2023).

- [[[refguru99, 3]]] T. Hamilton, “Test plan: What is, how to create (with example),” TEST PLAN: What is, How to Create (with Example), https://www.guru99.com/what-everybody-ought-to-know-about-test-planing.html (accessed Jul. 10, 2023). 
