[[_TOC_]]

# Errors

# How To

## alias 

> for alias with parameters see [functions](#functions)

## arrays

* add elements to array
  * associative array
    * set the indexes (different parts separated by special character ":")
        ```
        #ID             Name           Description              Node     Version
        #---------      -------------  -------------------      ------   ----------
        [0x0000]="      TEST:          TEST:                    test:    1"
        ...
        ```
    *  `<array_name>[<key>]=<value>`
    * remove element (by key):
      * with spaces `unset <array_name>[<key>]`
      * without spaces: `unset <array_name>["<key X>"]` | `unset <array_name>['<key X>']`

  * normal array
    * auto indexed (different parts separated by special character ":")
        ```
        #ID             Name            Description              Node     Version
        #---------      -------------   -------------------      ------   ----------
        "0x0000:        TEST:           TEST:                    test:    1" 
        ...
        ```
    * `<array_name>[<key>]=<value>` | `<array_name>+=<value>`
   
* add grep output by line to array (one line = one element)
    ```
    // Set IFS (input field separator) to be a newline so that the line itself is added as one element rather than having each word be an array element
    IFS=$'\n'
    // Add lines containing search string; 
    //  if search strings are not found `|| true` will allow script to continue - if missing then the script will end with greps return of 1 (no search entries found)
    ERRORS+=(`grep -Eir "<something|somethingelse|other>" relative/location/of/file || true`)
    ```
* count elements
    ```
    ${#list[@]}
    ```
* declare an array
  * associative array - I set the indexes
    ```
    declare -A <array_name>
    ```
  * normal array - auto indexed
    ```
    declare -a <array_name>
    ```
    ```
    array=()
    ```
* destruct tuple element (separated by ",")
	```
    for $tuple in "$list[@]}"; do
	  pt1=`echo $tuple | sed 's/,.*//'`
	  pt2=`echo $tuple | sed 's/^.*,//'`
	done
	```
* get keys (associative array)
    ```
    echo "${!array[@]}"     // loop and print all keys/indexes
    ```
* get value (associative array)
    ```
    echo ${array[$index]}   // individual value
    ```
* increment (numerical) element
    ```
    ((list[index++]))
    ```
* iterate over
    ```
    for i in <>; do
        -> "${list[@]}"                 # string list
        -> $(eval echo "{1..$var}")     # from to $var_number
    ```
* join elements in array
    ```
    function join {
      local IFS="$1"
      shift
      echo "$*"
    }
    list=$(join ',' ${old_list[@]})
    ```
* print array
    ```
    echo "${array[*]}"
    ```

## case statements
* string
    ```
    case "${STRING_VAR}" in
    "test_string1")
      # do something
      ;;
    "test_string2")
      # do something else
      ;;
    *)
      echo "Error: unrecognized command!"
      exit 1
      ;;
    esac
    ```

## check command stdout print out (and exit if contains keyword)
```
rc='$<command> 2>&1`	# save command return AND [2>&1] redirect stderr and stdout of command to rc
if [[ "$rc" == *"<stdout_key_word(s)>"* ]] ; then
  echo $rc	# print rc
  exit 1
  fi
```

## conversions
* string to lowercase
    ```
    VAR=${var,,}
    ```
* string to uppercase
    ```
    VAR=${var..}
    ```

## counters/variables
* count occurrences of
    ```
    <command> | wc -l
    ```
    * example: 
        ```
        num_nvmes=`lspci | grep ­i nvme | wc -l`
        ```
* decrement
    ```
    ((var--))
    ```
    ```
    ((var-=#))
    ```
* increment
    ```
    ((var++))
    ```
    ```
    ((var+=#))
    ```
    ```
    new_list=("${old_list}")
    ```

## comment chunk of code in script
```
: <<'END'
... //commented
END
... //uncommented
```

## enable verbose bash script debugging

```
#!/bin/bash
#
# <script_name>.sh
#
# <script description>

# Create DEBUG_BASH environment variable to enable debugging: `export DEBUG_BASH1`
#  - to disable debugging: `unset DEBUG_BASH`
if [[ ! -z "${DEBUG_BASH}" ]]; then
  export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
  set -x
fi

```


## file owned by another user
* add to file
    ```
    echo "whatever you want added" | sudo tee -a <file>
    ```
* remove from file
    ```
    sudo sed -i "/<beginning_of_line_to_remove>/d" "<file>"
    ```
* remove multiple lines from file (1 N for each line including and after the beginning line)
    ```
    sudo sed -i "/<beginning_of_line_to_remove>/{N;N;d;}" "<file>"
    ```

## functions

* non-parameterized function
    ```
    non_parameter_function_name(){
      # do something
    }

    non_parameter_function_name
    ```
* purameterised function
    ```
    function(){
      ./some_other_script.sh "$1"

      ./some_other_script.py \
          --param_a "$1"
          --param_b "$2"
    }

    function \ 
        $variable_param \
        '{"json":true,"param":"APPLICABLE"}`
    ```

## get my ip
```
ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v "<first_3_points_of_network>"
```

## get occurrence
ex:    
```
var = 
	time is 1:20 pm
	time is 1:21 pm
	time is 1:22 pm
```
* first
    ```
    echo $var | 'sed 's|pm .*|pm|'
    ```
    * gives "time is 1:20 pm"
* last
	  ```
    last=`echo $var | seid 's|.*pm ||'`
    ```
	* gives "time is 1:22 pm" 

## get part of a string
  *  `${string_var:-X}`
    * `X` is the number of characters from the end
  *  `${string_var:X:Y}`
    * `X` is the start 
    * `Y` is the end
    * ex: `string`="I am pie"; `${string:2:3}` = "am"
  *  unknown character specification (in search for example)
    * `[A-Z]|[a-z]|[0-9]|[0-ff]`
    * ex: `full_dirs=($(find . -type d | grep nvme[0-ff]n[0-ff]p[0-ff]))`

## get subscript PID
Save PID of subscript for later use.

```
./some_script \
    with \
    3 \
    parameters &

PID=$!
```

Example Use: kill process
  * `kill -INT $PID`
    * sends SIGINT to script
    * in my experience does not always work (maybe needs delay after call)
  * `kill -9 $PID`
    * works
    * also prints out which process was killed
  * `kill $PID`
    * works

## if
  * `-a`|`-e`
    * does it exist
  * `-d`
    * is it a directory
  * `-z`      
    * is it ""|NULL
      * `if [[ ! -z "$thing" ]]`
      * `if [ -z "$thing" ]`
  * conjunction
    * and &&
      * `if [[ <test1> && <test2> ]]`
    * or ||
      * `if [[ <test1> || <test2> ]]`
      * `if [ <test1> ] || [ <test2> ]`
      * `if [ <test_numerical_ne> -o <test_numerical_eq> ]`
  * is a number
    * `if [ -z "${a##*[!0-9]*}" ]`
  * numerical comparison
    * `-eq`
      * equal
    * `-gt`
      * greater than
    * `-ne`
      * not equal
    * `-lt`
      * less than
    * ex: if *a* is equal to *b*
      * ``[ `echo "${thinga[0]}==${thingb[$i]}" | bc` -eq 1 ]``
      * ``[ `echo "$sizea==$sizeb"|bc` -eq 0 ]``
        * `bc` is a program to complete the mathematical expression provided
          * returns 0 if false, 1 if true in this case
    * ex: if *a* is NOT equal to *b*
      * ``if [ "`mount | grep -E "ramdisk|localdev" | wc -l`" -ne 2 ]; then // do something; fi``
        * "" to make the value comparable (via -ne) to 2
        * `` to complete the command to get the value to compare to 2
## string comparison
  * contains: thing contains thing2
    * `[[ "$thing" =~ .*<thing2>.* ]]`
    * `[[ $thing[$n] == *"<thing2>"* ]]`
    * `[[ ! $thing =~ $things ]]`
    * `[ ! -z "${thing1##*$thing2*" ]`
  * equality
    * `[ "$thing1" == "$thing2" ]`
  * matches regex
    * `re = "<regex>"`; `if [[ $thing =~ $re ]]; then echo "Found"; fi`
## output
	* display on console and send to file
	  * `<command> | tee -a <file>`
	* redirection
   	* both std redirection:
      * `<command> 2>&1`
      * `<command> 1>&2`
      * `<command> &> redirection_file`
    * ignore:
	    * `<command> > /dev/null`
    	* `<command> 1> /dev/null`
      * `<command> 2> /dev/null`
      * `<command> &> /dev/null`
    * std error redirection:
	    * `<command> 2> redirection_file`
    * std output redirection:
      * `<command>  > redirection_file rewrite`
      * `<command> >> redirection_file apprehension`
      * `<command> 1> redirection_file`

## parameters
```
$ ./script p1 p2 p3 p4 ...
  $0      = ./script
  $1      = p1                    // parameter n
  $2      = p2
  $*      = p1 p2 p3 p4...        // all parameters
  ${*:3}  = p3 p4 ...             // parameter 3 and any following parameters
```

## partitions
* delete all partitions on a given device
  ```
  for rm_part in $(sudo parted -s /dev/<device> print|awk '/^ / {print $1}'); do
    sudo parted -s /dev/<device> rm ${rm_part}
  done
  ```

## run parallel (and save output)
```
cmd1='<command> <param1> <param2> ... <paramN>'
cmd2='<command> <param1> <param2> ... <paramN>'
# run commands
#   > is the redirection of output
#   /tmp/rc# is the save file (all output will go here)
#   & is to run the process in the background
ssh <user>@<ip> $cmd1 > /tmp/rc1 & 
                $cmd2 > /tmp/rc2 & 
# wait for all commands to finish
wait
# take output files and save in a variable
rc1=$(cat /tmp/rc1)
rc2=$(cat /tmp/rc2)
# print output
echo "rc1: $rc1"
echo "rc2: $rc2" 
```

## send response to command
```
yes "Yes" | sudo parted $dev_listing mklabel gpt
```

## ssh onto another machine, perform commands, and logout
* here documents
  ```
  // First key (EOF) must be in single quotes in order to have everything within the set be completed by
	// the sshed machine - necessary for creation and use of (ssh machine) local variables
	<ssh <user>@<ip> | vagrant ssh> << 'EOF'
  cmd 1
  cmd2
  ...
  EOF
  ```
* ssh direct
  ```
  ssh <user>@<ip> <command>
  ```

## strings
* add to end of string: `string+="addition"`

## view exit code of function
* 0 is success, 1+ is failure
```
<function>
echo $?
```

ex:
```
$(find . -print | grep <filename>)
if [[ $? -ne 0 ]]; do ...
```

## writing a script that takes multiple input, in the main function save the different inputs in separate variables
* allows use of argv[n] in different scopes via the save variable
```
$ for NUM in `seq <start_num> <increment num> <end num>`        //`seq 1 1 5` ~= for i = 0; i < 5; i ++)      
  > do
  > <insert stuff to do>
  > done >> <date>-<save_file_name>.txt
```

## undefined symbol 
* make human readable: `$ c++filt <undefined_symbol>`
* list all undefined symbols: `$ ldd -d -r <executable_name>`

# Pre-Canned Functions

## Capture Error And Notify Of Origin
```
# Setup a trap to catch any errors; include the line number and the error.
set -eE -o functrace
failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at line $lineno, with command: \"$msg\""
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR
```

## Convert Hex UDP Packet To Readable Output (script)
```
#!/bin/bash

hex_to_ascii () {
  echo -ne "$1" | xxd -r -p
}

hex_to_ip () {
  printf '%d.%d.%d.%d\n' $(echo "$1" | sed 's/../0x& /g')
}

if [[ "$1" =~ .*help.* || "$1" == "-h" ]]; then
  echo "Description:"
  echo "  Process received packets; searching file (containing captured data) for a specified ip address."
  echo "Usage:"
  echo "  process: $ process_data_from_passthrough_o3.sh IP DATA TEXTONLY"
  echo "    IP: ip address of machine data was sent to"
  echo "    DATA: data to process; local path to file containing passthrough_o3 output"
  echo "    TEXTONLY: true (default) only show data with no packet information or false show packet information"
  exit 0
fi

# Save and verify parameters
IP=$1
DATA=$2
TEXTONLY=${3:-"true"}   # default value is true

if [[ ! $IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "Error: not a valid ip, must be in the format #.#.#.#"
  exit 1
fi

if [[ ! -e $DATA ]]; then
  echo "Error: file \"$DATA\" does not exist!"
  exit 1
fi

TEXTONLY=${TEXTONLY,,}
if [[ ! $TEXTONLY =~ ^(true$|false$) ]]; then
  echo "Error: TEXTONLY needs to be \"true\" or \"false\"."
  exit 1
fi

HEX_IP=`printf '%02x' ${IP//./ }`
HEX_PORT=`printf '%x' ${PORT}`

# Remove lines that do not have IP in Destination IP and Port in Destination Port.
sed -i "/^Packet/ {/^.*$HEX_IP....$HEX_PORT.*/n; N; {//!d}}" "$DATA"


# Print source and destination ip addresses and the data.
awk '!/^Packet/n;n; \
{ip_src_str = substr($1,53,8); if (ip_src_str) printf " Source IP: %s (%s)\n", ip_src_str, ip_src_str} \
{ip_dst_str = substr($1,61,8); if (ip_dst_str) printf " Destination IP: %s (%s)\n", ip_dst_str, ip_dst_str} \
{data_str = substr($1,85); if (data_str) printf " Data: %s\n  string: %s\n\n", data_str, data_str}' \
$DATA  > processing_temp
mv processing_temp $DATA

# Translate the ip portion of each packet
while IFS="" read -r line || [ -n "$line" ]
do
  if [[ "$line" =~ .*IP\:.* ]]; then
    hex=`echo $line | tr -s ' ' | cut -d ' ' -f3`
    str_of_hex=$(hex_to_ip "$hex")
    sed -i "s/($hex)/($str_of_hex)/" "$DATA"
  fi
done < $DATA

# Translate the data portion of each packet
while IFS="" read -r line || [ -n "$line" ]
do
  if [[ "$line" =~ .*string\:.* ]]; then
    hex=`echo $line | tr -s ' ' | cut -d ' ' -f2`
    str_of_hex=$(hex_to_ascii "$hex")
    sed -i "s/string: $hex/string\: $str_of_hex/" "$DATA"
  fi
done < $DATA

if [[ $TEXTONLY == "true" ]]; then
  sed -i '/string/!d' $DATA
  sed -i 's/^.*string: //g' $DATA
fi

```

* input (file):
```
Packet # 
5254008fe9e6000129a7161108004500003a31db40004011dfa90a320a970a320a34e5f615b30026bdb848616c65696768202d2074686973206973206a757374206120746573740a
```

* output (file): `$ process_udp_data 10.50.10.52 <DATA> false`
```
Packet # 
5254008fe9e6000129a7161108004500003a31db40004011dfa90a320a970a320a34e5f615b30026bdb848616c65696768202d2074686973206973206a757374206120746573740a
 Source IP: 0a320a97 (10.50.10.151)
 Destination IP: 0a320a34 (10.50.10.52)
 Data: 48616c65696768202d2074686973206973206a757374206120746573740a
  string: Haleigh - this is just a test

```
* output (file): `$ process_udp_data 10.50.10.52 <DATA>`
```
Haleigh - this is just a test

```

# Symbols

* `$?`
  * return code of last command completed
* `$!`
  * PID of the most recently executed background task
* `#!/bin/bash <args>`
  * `<args>`
    * `-x`
      * debug mode
      * expand each command (simple, for, case, select, arithmetic) and its expanded arguments or word list
    * `-v`
      * print shell input ines as they are read
    * `-/+<option>`	
      * `-<option>` turns options on
      * `+<option>` turns options off 
* parenthesis
  * `$(...)`
    * execute command(s) within parens in a subshell and return stdout
  * `(...)`
    * execute command(s) within parens in a subshell
  * `$((...))`
    * perform arithmetic and return the result of the calculation
      * `$((<hex_var>))` convert hexadecimal to decimal
  * `((...))`
    * perform arithmetic (possibly changing shell variable values)
