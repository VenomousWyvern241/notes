[[_TOC_]]

# Reverse 64 Bit Hexadecimal Number

AKA: 'htonl()' for uin64_t  

```
    uint64_t tmp = 0x112233445566, t1, t2;
      
    t1 = ntohl(tmp >> 32);
    std::cout << t1 << "\n";
    t2 = ntohl(tmp & 0xffffffff);
    std::cout << t2 << "\n";
     
    std::cout << ( ( t2 << 32 ) | t1 ) / 0x10000 << "\n";
    // prints 0x665544332211
```

# Sort Hexadecimal Numbers

```
    $ cat hexanumbers.txt
    $ while read hexnum; do array[16#$hexnum]="$hexnum"; done < hexanumbers.txt
    $ printf '%s\n' "${array[@]}" > ordered_hexnumbers.txt
```

# Turn C Into Assembly

```
    $ vi tea.c
            #include <stdio.h>
            int main (){
                    puts("hello world");
            }
    $ make tea // creates tea
    $ cc -S tea.c //-> creates tea.s
    $ vi tea.s
                    .file   "tea.c"
                    .section        .rodata
            .LC0:
                    .string "hello world"
                    .text
                    .globl  main
                    .type   main, @function
            main:
            .LFB0:
                    .cfi_startproc
                    pushq   %rbp
                    .cfi_def_cfa_offset 16
                    .cfi_offset 6, -16
                    movq    %rsp, %rbp
                    .cfi_def_cfa_register 6
                    movl    $.LC0, %edi
                    call    puts
                    movl    $0, %eax
                    popq    %rbp
                    .cfi_def_cfa 7, 8
                    ret
                    .cfi_endproc
            .LFE0:
                    .size   main, .-main
                    .ident  "GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
                    .section        .note.GNU-stack,"",@progbits
    $ cc tea.s      //-> creates a.out
    $ a.out
            -> hello world
```


