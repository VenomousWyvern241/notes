## Formatting
set shiftwidth=2
set tabstop=2

## Spell Checking
# Add capability to icheck dts files.
autocmd FileType dts syntax spell toplevel
# Set language.
set spelllang=en_us
