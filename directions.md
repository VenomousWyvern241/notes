# Directions 

## How to build/run cmake in this repo.

0. If first time, need to pull submodules (without updating them).
   ```
   $ cd .../notes
   $ git pull --recurse-submodules
   $ git submodule update --init
   ```
1. Create and populate the build directory.
   ```
   $ cmake -G Ninja -B build -S .
   ```
2. Enter the build directory.
   ```
   $ cd build
   ```
3. Build everything.
   * end in `--` for normal execution
   * end in `--verbose` to print commands executed
   ```
   $ cmake --build . --[verbose]
   ``` 

## How to enter poetry venv.

0. If first time, need to install pipx and poetry.
   ```
   $ sudo apt-get install pipx
   $ pipx install poetry
   ```
1. Enter the projects root dir.
   ```
   $ cd .../notes
   ```
2. Install poetry managed dependencies.
   ```
   $ poetry install
   ```
3. Activate the virtual environment.
   ```
   $ source .venv/bin/activate
   ```
